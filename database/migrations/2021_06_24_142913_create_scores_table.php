<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scores', function (Blueprint $table) {
            $table->id();

$table->unsignedBigInteger('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->string('school_name')->nullable();
            $table->unsignedBigInteger('skill_id')->references('id')->on('skills')->onDelete('cascade');
            $table->string('skill_name')->nullable();


            $table->unsignedBigInteger('exam_id')->references('id')->on('exams')->onDelete('cascade');
            $table->string('exam_name')->nullable();

            // ឆមាសទី
            $table->unsignedBigInteger('section_id')->references('id')->on('sections')->onDelete('cascade');
            $table->string('section_name')->nullable();

            $table->unsignedBigInteger('level_id')->references('id')->on('levels')->onDelete('cascade');
            $table->string('level_name')->nullable();
            $table->unsignedBigInteger('class_id')->references('id')->on('classes')->onDelete('cascade');
            $table->string('class_name')->nullable();

            $table->string('study_year')->nullable();
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->date('date')->nullable();

            $table->unsignedBigInteger('student_id')->references('id')->on('students')->onDelete('cascade');
            $table->string('student_name')->nullable();

            $table->unsignedBigInteger('subject_id')->references('id')->on('subjects')->onDelete('cascade');
            $table->string('subject_name')->nullable();
            $table->unsignedDecimal('score')->default(0.00);
            $table->unsignedDecimal('total_score')->default(0.00);

            // $table->integer('rank')->nullable();
            $table->string('credit')->nullable();
            $table->unsignedDecimal('average')->nullable();
            $table->string('color')->nullable();
            $table->string('final')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scores');
    }
}
