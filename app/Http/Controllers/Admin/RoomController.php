<?php

namespace App\Http\Controllers\Admin;

use App\Models\Room;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoomController extends Controller
{
    public function index()
    {
        $data['rooms']=Room::get();
        return view('backend.rooms.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if( request()->ajax()){
		    return view('backend.rooms.modal.add');
		}
        return view('backend.rooms.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = [
            'status' => false,
            'data' => [],
        ];
        $add = Room::create([
            // 'business_id'  =>  1,
            'school_id'          => auth()->user()->school_id,
            'code'          => $request->code,
            'name'          => $request->name,
            'description'    => $request->description,
            'status'    => $request->status,
        ]);

        if ($add) {
            $response = [
                'status' => true,
                'data' => $add,
                'dom' => view('backend.rooms.rows',['row' => $add])->render(),
            ];
        }
        if(request()->ajax()){

            return $response;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {

        if( request()->ajax()){
		    return view('backend.rooms.modal.show',compact('room'));
		}

        // return view('backend.schools.show',compact('school','user','teacher'));
    }

    public function edit(Room $room)
    {

        if( request()->ajax()){
		    return view('backend.rooms.modal.edit',compact('room'));
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Room $room)
    {
        $response = [
            'status' => false,
            'data' => [],
            // 'id' => $category->id,
        ];
        $update = $room->update([
            'school_id'          => auth()->user()->school_id,
            'code'          => $request->code,
            'name'          => $request->name,
            'description'    => $request->description,
            'status'    => $request->status,
        ]);

        if ($update) {
            $response = [
                'status' => true,
                'data' =>['id'=> $room->id],
                // 'data' => $roomes,
                'dom' => view('backend.rooms.rows',['row' => $room])->render(),

            ];

        }
        // dd($response);

        if(request()->ajax()){
            return $response;
        }
    }

    public function destroy($id)
    {
        //
        $response = [
            'status' => false,
        ];
        $delete=Room::findOrFail($id);

        // $userCheck=Classes::where('id','=',$id)->count();
        // dd($userCheck);

        // if ($userCheck == 0) {
        //     $delete->delete();
        //     $response = [
        //         'status' => true,
        //     ];
        // }

        $delete->delete();
        $response = [
            'status' => true,
        ];


        if (request()->ajax()) {
            return $response;
        }

        return redirect()->route('rooms.index');
    }
}
