@extends('layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
    <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0">{{trans('controls.teachers.all')}}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/')}}">{{trans('controls.home')}}</a></li>
                    <li class="breadcrumb-item active">{{trans('controls.teachers.all')}}</li>
                </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user-graduate"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">សរុបគ្រូ</span>
              <span class="info-box-number">
                {{$teachers->count()}}
                <small>នាក់</small>
                ស្រី {{$teachers_female->count()}} <small>នាក់</small>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user-edit"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">សិស្សឆ្នាំថ្មី</span>
              <span class="info-box-number">
                100
                <small>នាក់</small>
                ស្រី 30 <small>នាក់</small>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box mb-3">
            <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-chalkboard-teacher"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">សិស្សអវត្តមាន</span>
              <span class="info-box-number">50 <small>នាក់</small>, ស្រី <small>នាក់</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->



      <div class="row">
            <div class="col-md-12">
                <!-- USERS LIST -->
                <div class="card bg-default">
                <div class="card-header">
                    <h3 class="card-title">{{trans('controls.teachers.all')}}</h3>

                    <div class="card-tools">
                    <span class="badge badge-danger">{{$teachers->count()}} នាក់</span>
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                    </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0" style="display: block;">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                    @foreach ($teachers as $teacher)
                    <li class="item">
                        <div class="product-img">
                            @if ($teacher->profile=="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50")
                            <img src="{{asset('images/teachers/default_teacher.jpg')}}" alt="{{$teacher->name_en}}" class="img-size-50">
                            @elseif ($teacher->profile==null)
                                <img src="{{asset('images/teachers/default_teacher.jpg')}}" alt="{{$teacher->name_en}}" class="img-size-50">
                            @else
                            <img src="{{asset('images/teachers/'.$teacher->profile)}}" alt="{{$teacher->name_en}}" class="img-size-50">
                            @endif
                        </div>
                        <div class="product-info">
                        <a href="javascript:void(0)" class="product-title">{{$teacher->name_kh}}
                            <span class="badge badge-warning float-right">{{$teacher->dob}}</span></a>
                        <span class="product-description">
                            {{$teacher->name_en}}
                        </span>
                        </div>
                    </li>
                    <!-- /.item -->
                    @endforeach

                    </ul>
                    <!-- /.users-list -->
                </div>
                <!-- /.card-body -->

                <!-- /.card-footer -->
                </div>
                <!--/.card -->
            </div>



      </div>



    </div><!--/. container-fluid -->
  </section>
  <!-- /.content -->
@endsection
