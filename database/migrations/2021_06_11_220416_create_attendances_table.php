<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendances', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('school_id')->references('id')->on('students')->onDelete('cascade');
            $table->unsignedBigInteger('skill_id')->references('id')->on('students')->onDelete('cascade');
            $table->unsignedBigInteger('student_id')->references('id')->on('students')->onDelete('cascade');
            $table->unsignedBigInteger('class_id')->references('id')->on('classes')->onDelete('cascade');
            $table->unsignedBigInteger('section_id')->references('id')->on('sections')->onDelete('cascade');
            $table->unsignedBigInteger('level_id')->references('id')->on('sections')->onDelete('cascade');

            $table->string('section_name')->nullable();
            $table->string('year')->nullable();
            $table->string('month')->nullable();

            $table->date('date')->nullable();
            $table->integer('attendance')->nullable()->comment('0 is absent, 1 is present, 2 is late,3 is permission');
            $table->string('reason')->nullable();

            $table->unsignedBigInteger('academic_years_id')->references('id')->on('sections')->onDelete('cascade');

            $table->string('study_year')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendances');
    }
}
