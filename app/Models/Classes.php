<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    use HasFactory;

    protected $fillable=([
        'school_id',
        'level_id',
        'skill_id',
        'code',
        'name',
        'description',
        'teacher_id',
        'teacher_name',
        'status',
    ]);
}
