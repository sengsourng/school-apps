@extends('layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">{{trans('controls.contact-us')}}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('/')}}">{{trans('controls.home')}}</a></li>
            <li class="breadcrumb-item active">{{trans('controls.contact-us')}}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-4">
            <p>Director : </p>
            <p>Phone :</p>
            <address>Address:</address>
        </div>

        <div class="col-md-8">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d970.4842772620702!2d103.91939656627044!3d13.35418669080239!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31103d59829ef3ad%3A0x3f60e3e786737631!2sHigh%20School%20and%20Primary%20School%20Svay%20Thom!5e0!3m2!1sen!2skh!4v1627898911081!5m2!1sen!2skh" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>

      </div>
      <!-- /.row -->




    </div><!--/. container-fluid -->
  </section>
  <!-- /.content -->
@endsection
