<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    use HasFactory;

    protected $fillable=[
        'user_id',
        'name','phone','email',
        'city','city_slug','address','director_name','is_active',
        'created_at','updated_at'
    ];
}
