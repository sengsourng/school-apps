<div class="container pt-1">
    <form action="{{ route('academic_years.update',$AcademicYear) }}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('PUT') }}

        <div class="row">
            <div class="col-md-12 bg-info pt-3">

                  <div class="form-group row">
                        <label for="lable_s_name" class="col-sm-3 col-form-label">{{trans('controls.acadamic_years.name')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="name"   class="form-control" id="lable_s_name"  value="{{$AcademicYear->name}}">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="lable_s_phone" class="col-sm-3 col-form-label">{{trans('controls.acadamic_years.year')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="year"  class="form-control" id="lable_s_phone" value="{{$AcademicYear->year}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lable_s_email" class="col-sm-3 col-form-label">{{trans('controls.acadamic_years.decription')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="description"  class="form-control" id="lable_s_email" value="{{$AcademicYear->description}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lable_s_city" class="col-sm-3 col-form-label">{{trans('controls.acadamic_years.active')}}</label>
                        <div class="col-sm-9">
                           <select name="is_active" id="active"  class="form-control">
                            <option {{$AcademicYear->is_active==1?'selected':''}} value="1">Active</option>
                            <option {{$AcademicYear->is_active==0?'selected':''}} value="0">Not Active</option>
                           </select>
                        </div>
                    </div>




            </div>



        </div>

    </form>
</div>
