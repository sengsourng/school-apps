<?php

return [

	'menus'	=>[
		'list'		=>	'បញ្ជី',
		'students'	=>	'សិស្ស',
		'teachers'	=>	'គ្រូ',
		'skills'	=>	'ជំនាញ',
		'subjects'	=>	'មុខវិជ្ជា',
		'sections'	=>	'ឆមាស',
		'rooms'		=>	'បន្ទប់',
		'classes'	=>	'ថ្នាក់',
		'credit_score'	=>	'លំដាប់និទ្ទេស',
		'exams'			=>	'ការប្រឡង',
		'exam_details'			=>	'ការប្រឡងលំអិត',
		
		'attendances'			=>	'វត្តមាន',
		'attendance_teachers'			=>	'វត្តមាន គ្រូ',
		'attendance_students'			=>	'វត្តមាន សិស្ស',
		
		'monthly_top_five'	=>	'តារាងកត្តិយសប្រចាំខែ',
		'yearly_top_five'	=>	'លិខិតសរសើរប្រចាំឆ្នាំ',
		'result_by_student'	=>	'តារាងព្រឹត្តបត្រពិន្ទុ',
		
	],


    // Main Menu
    'dashboard'         =>'ផ្ទាំងគ្រប់គ្រង',
    'home'              =>'ទំព័រដើម',
    'contact-us'        =>'ទំនាក់ទំនង',
    'about'             =>'អំពីសាលា',
    'login'             =>'ចូលប្រើ',
    'teacher register'     =>'ចុះឈ្មោះគ្រូ',
    'student register'     =>'ចុះឈ្មោះសិស្ស',
    'top five students'     =>'បញ្ចីសិស្សពូកែ',
    'student absent'        =>'សិស្សអវត្តមាន',
    'result exam'           =>'លទ្ធផលប្រឡង',

    'Are you sure?'         =>'តើអ្នក​ច្បាស់​ជាលុប​ឬ?',
    'Yes, delete it!'         =>'បាទ/ចាស, ត្រូវលុប!',
    'cancel'         =>'បោះបង់',
    'save close'         =>'រក្សាទុក បិទ',
    'save'         =>'រក្សាទុក',
    'You will not be able to delete this!' =>'អ្នកមិនអាចលុប​ទិន្ន​ន័យ​នេះបានទេ!',
    'Your data has been saved or updated!' =>   'ទិន្ន​ន័យ​បាន​រក្សាទុក!',
    'Your file has been deleted.'   =>  'ទិន្ន​ន័យ​មិនត្រូវបានលុប.',
    'delete'   =>  	"លុបទិន្ន​ន័យ !",
    'delete_message_can_not' => 'អ្នក​មិន​អាចលុប វាកំពុងប្រើ​ !',
	'student_report_by_class' => 'បញ្ជីសិស្សតាមថ្នាក់',
	'student_score_by_class' =>'ពិន្ទុសិស្សតាមថ្នាក់',

    'logout'     =>'ចាកចេញ',
	
	
	
//Attendance វត្តមានសិស្ស
	'Present'	=>'វត្តមាន',
	'Absent'		=>'អវត្តមាន',
	'Late'			=>	'មកយឺត',
	'student_atten_report' =>	'របាយការណ៍វត្តមានសិស្ស',
	'att_times'		=>	'ដង',

    // Teacher
    'teachers'     =>[
            'all'       =>'បញ្ជីគ្រូ',
			'code'	=> 'លេខកូដមន្ត្រី',
            'name'      =>'ឈ្មោះគ្រូ',
            'full_name'      =>'ឈ្មោះពេញ',
            'gender'      =>'ភេទ',
            'pob'      =>'កន្លែងកំណើត',
            'dob'      =>'ឆ្នាំកំណើត',
            'address'      =>'អាស័យដ្ឋាន',
            'profile'      =>'រូបថតកាត',
			'is_active' => 'ស្ថានភាពគ្រូ',
        ],

//Student
	'students'		=>[
			'id'	=>'ល.រ',
			'code'	=>'លេខកូដ',
			'all'	=>'បញ្ចីសិស្សទាំងអស់',
			'name'	=>'ឈ្មោះពេញ (ខ្មែរ)',
			'first_name'	=>'នាមត្រកូល',
			'last_name'	=>'នាមខ្លួន',
			'name_en'	=>'ឈ្មោះឡាតាំង​',
			'gender'	=>	'ភេទ',
			'dob'		=>	'ឆ្នាំកំណើត',
			'pob'		=>	'កន្លែងកំណើត',
			'address'	=>	'អាស័យដ្ឋាន',
			'profile'	=>	'រួបភាព',
			
			'father'	=>	'ឪពុក',
			'father_job'	=>	'មុខរបរឪពុក',
			'mother'	=>	'ម្តាយ',
			'mother_job'	=>	'មុខរបរម្តាយ',
			'city'			=>	'ខេត្ត/ក្រុង',
			'city_slug'		=>	'slug ក្រុង',
			'register_year'	=>	'ឆ្នាំចុះឈ្មោះ',
			'status'		=>	'ស្តានភាព',
			'bio'			=>	'គំនិតយោបល់',
			'add'			=>	'បន្ថែមថ្មី',
			'export'		=>	'នាំចេញ',
			'import'		=>	'នាំចូល​',
			'manage_attendance'	=>	'គ្រប់គ្រងវត្តមាន',
			'student_attendance'	=>	'វត្តមានរបស់សិស្ស',
			'save_attendance'	=>	'រក្សាទុក​វត្តមាន',
			'date'			=> 'កាលបរិច្ឆេទ',
			'active'	=>	'នៅរៀន',
			'is_active'	=>	'ស្ថានភាពសិស្ស',
			'disable'	=>	'ឈប់រៀន',
			'filter_student'	=>	'ច្រោះ​ពត៌មាន​សិស្ស',
			'edit'	=> 'កែប្រែពត៌មានសិស្ស',

			],


    'sent' => 'បានផ្ញើរ',
    'users' => "អ្នកប្រើប្រាស់",
    'user' => "អ្នកប្រើប្រាស់",
    'name'  =>'ឈ្មោះ',
    'full_name' => "ឈ្មោះពេញ",


    'users'     =>[
            'email'         => 'អ៊ីម៉ែល',
            'username'      =>'ឈ្មោះប្រើប្រាស់',
            'password'      =>'ពាក្យសំង៉ាត់',
            'forgot password'      =>'ភ្លេចពាក្យសំង់ាត់?',
            ],


    'schools'   =>[
        'list'  =>'បញ្ចីឈ្មោះសាលា',
        'name'  =>'ឈ្មោះសាលា',
        'phone'  =>'ទូរស័ព្ទ',
        'email'  =>'អ៊ីម៉ែល',
        'city'  =>'ខេត្ត/ក្រុង',
        'address'  =>'អាស័យដ្ឋាន',
        'director_name'  =>'ឈ្មោះនាយក',
    ],

    'forms'         =>[
                'Add'   =>  "បន្ថែម",
                'Add New'   =>  "បន្ថែមថ្មី",
                'Add More'   =>  "បន្ថែម",
                'Edit'   =>  "កែប្រែ",
                'Delete'   =>  "លុប",
                'Update'   =>  "ធ្វើបច្ចុប្បន្នភាព",
                'Show'   =>  "បង្ហាញ",
                'Upload'   =>  "បញ្ចូល​",
                'Created By'   =>  "បង្កើតដោយ",
                'Please Select'   =>  "សូម​ជ្រើសរើស",
                'Select All'   =>  "ជ្រើសរើសទាំងអស់",
                'Show All'   =>  "បង្ហាញទាំងអស់",
                'List'   =>  "បញ្ចី",
                'Action'   =>  "សកម្មភាព",
                'ID'   =>  "ល.រ",
                'Code'   =>  "លេខកូដ",
                'Male'   =>  "ប្រុស",
                'Female'   =>  "ស្រី",
				'created_at'	=> 'ថ្ងៃបង្កើត',
				'brow_image'	=> 'ជ្រើសរើស',
				'remove'	=> 'ដកចេញ',
				'print_report' =>'ព្រីនរបាយការណ៍',
				'print' => 'ព្រីន/បោះពុម្ភ',
				'filter'	=> 'ច្រោះទិន្ន​ន័យ',
            ],
	//Acadamic_year
	'acadamic_years' => 	[
			'name'		=>	"ឆ្នាំសិក្សា",
			'year'		=>	"ឆ្នាំ",
			'decription'=>	"ពណ៌នា",
			'active'	=>	"សកម្ម",
			'disactive'	=>	"អសកម្ម",
			'list'		=>	"បញ្ជី ឆ្នាំសិក្សា",
			'month'	=>	'ខែ',
	],
	
	
	//Levels
	'levels'		=>	[
		'id'	=>	'ល.រ',
		'code'	=>	'កូដ',
		'name'	=>	'កំរិតសិក្សា',
		'description'	=>	'អធិប្បាយ',
		'status'	=>	'ស្ថានភាព',
		'select'	=>	'ជ្រើសរើសកំរិតសិក្សា',
		
	],
	
	
	//Classes
	'classes'		=>	[
		'id'	=>	'ល.រ',
		'list'	=>	'បញ្ចីថ្នាក់រៀន',
		'code'	=>	'កូដ',
		'name'	=>	'ថ្នាក់រៀន',
		'description'	=>	'អធិប្បាយ',
		'status'	=>	'ស្ថានភាព',
		'select'	=>	'ជ្រើសរើសថ្នាក់',
		
	],
	
	//Sections
	'sections'		=>	[
		'id'	=>	'ល.រ',
		'code'	=>	'កូដ',
		'list'	=>	'បញ្ចីឆមាស',
		'name'	=>	'ឈ្មោះឆមាស',
		'description'	=>	'អធិប្បាយ',
		'status'	=>	'ស្ថានភាព',
		'score_type'	=>	'គណនាពិន្ទុ',
		'select'	=>	'ជ្រើសរើសឆមាស',
		'average'	=>	'តាមមធ្យម',
		'formula'	=>	'តាមរូបមន្ត',
		
	],
	
	//Skills
	'skills'		=>	[
		'id'	=>	'ល.រ',	
		'list'	=>	'បញ្ចីជំនាញ',
		'name'	=>	'ឈ្មោះជំនាញ',
		'description'	=>	'អធិប្បាយ',
		'status'	=>	'ស្ថានភាព',
		'select'	=>	'ជ្រើសរើសជំនាញ',		
	],
	
	//Subjects
	'subjects'		=>	[
		'id'	=>	'ល.រ',
		'code'	=>	'លេខកូដ',
		'list'	=>	'បញ្ចីមុខវិជ្ជា',		
		'name'	=>	'ឈ្មោះ​មុខវិជ្ជា',
		'description'	=>	'អធិប្បាយ',
		'status'	=>	'ស្ថានភាព',
		'select'	=>	'ជ្រើសរើសមុខវិជ្ជា',
		'active'	=>	'ដាក់ឲ្យប្រើ',
		'disable'	=>	'មិនឲ្យ​ប្រើ',
		'sort'	=>	'លំដាប់',
	],
	
	//Rooms
	'rooms'		=>	[
		'id'	=>	'ល.រ',
		'code'	=>	'លេខកូដ',
		'list'	=>	'បញ្ចីបន្ទប់',		
		'name'	=>	'ឈ្មោះ​បន្ទប់',
		'description'	=>	'អធិប្បាយ',
		'status'	=>	'ស្ថានភាព',
		'select'	=>	'ជ្រើសរើសមបន្ទប់',
		'active'	=>	'ដាក់ឲ្យប្រើ',
		'disable'	=>	'មិនឲ្យ​ប្រើ',
		'sort'	=>	'លំដាប់',
	],
	//Scores
	'scores'		=>	[
		'manage'	=>	'គ្រប់គ្រងពិន្ទុ សិស្ស',
		'save'	=>	'រក្សាទុក​ពិន្ទុ',
		'score'	=>	'ពិន្ទុ',
		'avg'	=>	'មធ្យមភាគ',		
		'total'	=>	'សរុប',
		'rank'	=>	'លំដាប់',
		'credit'	=>	'និទ្ទេស',
		'subject'	=>	'មុខវិជ្ជា',
		'level'	=>	'កំរិតសិក្សា',
		'class'	=>	'ថ្នាក់',
		
	],
	
];
