<?php

namespace Database\Seeders;

use App\Models\Room;
use Illuminate\Database\Seeder;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Room::create([
            'school_id' => 1,
            'code'      =>'R101',
            'name'      =>'Room C1',
            'description'   =>'បន្ទប់ C1',
            'status'        =>'active',
        ]);

        Room::create([
            'school_id' => 1,
            'code'      =>'R102',
            'name'      =>'Room C2',
            'description'   =>'បន្ទប់ C2',
            'status'        =>'active',
        ]);

        Room::create([
            'school_id' => 1,
            'code'      =>'R103',
            'name'      =>'Room C3',
            'description'   =>'បន្ទប់ C3',
            'status'        =>'active',
        ]);


        Room::create([
            'school_id' => 1,
            'code'      =>'R104',
            'name'      =>'Room C4',
            'description'   =>'បន្ទប់ C4',
            'status'        =>'active',
        ]);


        Room::create([
            'school_id' => 1,
            'code'      =>'R105',
            'name'      =>'Room C5',
            'description'   =>'បន្ទប់ C5',
            'status'        =>'active',
        ]);


        Room::create([
            'school_id' => 1,
            'code'      =>'R106',
            'name'      =>'Room C6',
            'description'   =>'បន្ទប់ C6',
            'status'        =>'active',
        ]);


        Room::create([
            'school_id' => 1,
            'code'      =>'R107',
            'name'      =>'Room C7',
            'description'   =>'បន្ទប់ C7',
            'status'        =>'active',
        ]);
    }
}
