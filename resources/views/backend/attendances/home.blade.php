@extends('layouts.admin')

@push('custom_css')

<!-- DataTables -->
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

 <!-- daterange picker -->
 <link rel="stylesheet" href="{{asset('themes')}}/plugins/daterangepicker/daterangepicker.css">

 <style>
     /* Custom checkbox */
th .c-container {
	margin-top: 12px;
    margin-bottom: 11px;
}
.c-container {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 23px;
    cursor: pointer;
    font-size: 14px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}


.c-container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}

/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 20px;
    width: 20px;
    background-color: #bdc3c7;
}

/* On mouse-over, add a grey background color */
.c-container:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.c-container input:checked ~ .checkmark {
    background-color: #007bff;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the checkmark when checked */
.c-container input:checked ~ .checkmark:after {
    display: block;
}

/* Style the checkmark/indicator */
.c-container .checkmark:after {
    left: 8px;
    top: 4px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
 </style>
@endpush


@section('content')


     <section class="content pt-2">
        <div class="card">
          <div class="card-header text-left py-3" style="height: 9vh;">
            <h5 class="mb-0 text-left">
                <strong>វត្តមានសិស្ស</strong>
            </h5>
          </div>
          <div class="card-body">
            <div>
                <div class="row">
                    <div class="col-md-12">
                            <div class="card card-outline card-success">
                              <div class="card-header">
                                <form id="search_form" class="params-panel validate" action="{{ route('student_attendance.create')}}" method="post" autocomplete="off" accept-charset="utf-8">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="control-label">{{trans('controls.levels.name')}}</label>
                                                <select name="level_id" class="form-control select2" onChange="getData(this.value);" required>
                                                    <option value="">{{ trans('controls.levels.select') }}</option>
                                                    {{ create_option('levels','id','name',$level_id) }}
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="control-label">{{trans('controls.classes.name')}}</label>
                                                <select name="class_id" id="class_id" class="form-control select2" required>
                                                    <option value="">{{ trans('controls.classes.select') }}</option>
                                                    {{ create_option('classes','id','name',$class_id) }}
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="control-label">{{trans('controls.sections.name')}}</label>
                                                <select name="section_id" class="form-control select2" required>
                                                    <option value="">{{ trans('controls.forms.Please Select') }}</option>
                                                    {{ create_option('sections','id','name',$section_id) }}
                                                </select>
                                            </div>
                                            {{-- <input type="text" name="my_sections" value="{{$section_id}}"> --}}
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="date" class="control-label">{{trans('controls.students.date')}}</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    <input type="date" class="form-control datepicker" name="date" value="{{ $date }}" required>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-sm-2 pt-4">
                                            <div class="form-group pt-2">
                                                <button type="submit" class="btn btn-primary btn-block rect-btn">{{trans('controls.students.manage_attendance')}}</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                              </div>
                            </div>
                    </div>
                    <div class="col-md-12">
                            @if (!empty($attendance))

                            <div class="text-center">
                                <h3>{{trans('controls.students.manage_attendance')}}
                                </h3>
                                <span class="text-success mb-4">
                                    {{$section}} - {{$class}} {{trans('controls.students.date')}} {{$date}}
                                </span>

                            </div>

                            <form action="{{ route('student_attendance.save') }}" class="appsvan-submit-validate" method="post" accept-charset="utf-8">
                                @csrf
                                <table class="table table-bordered table-responsive">
                                    <thead>
                                        <th style="width:250px;"><label class="c-container">{{trans('controls.students.name')}}</th>
                                        <th><label class="c-container">{{trans('controls.students.code')}}</th>
                                        <th><label class="c-container">{{('Present')}}<input type="checkbox" id="present_all" onclick="present(this)"><span class="checkmark"></span></label></th>
                                        <th><label class="c-container">{{('Absent')}}<input type="checkbox" id="absent_all" onclick="absent(this)"><span class="checkmark"></span></label></th>
                                        <th><label class="c-container">{{('Late')}}<input type="checkbox" id="late_all" onclick="late(this)"><span class="checkmark"></span></label></th>
                                        <th><label class="c-container">{{('Holiday')}}<input type="checkbox" id="holiday_all" onclick="holiday(this)"><span class="checkmark"></span></label></th>
                                    </thead>
                                    <tbody>
                                        @foreach($attendance AS $key => $data)
                                            <tr>
                                                <td>{{ $data->name_kh}}</td>
                                                <input type="hidden" name="student_id[]" value="{{$data->student_id}}">
                                                <input type="hidden" name="section_id" value="{{$section_id}}">
                                                <input type="hidden" name="class_id" value="{{$data->class_id}}">

                                                <input type="hidden" name="date" value="{{$date}}">
                                                <input type="hidden" name="attendance_id[]" value="{{$data->attendance_id}}">
                                                <td>{{ $data->student_id }}</td>
                                                <td><label class="c-container"><input type="checkbox" name="attendance[{{$key}}][]" @if($data->attendance=='1') checked @endif value="1" class="present"><span class="checkmark"></span></label></td>
                                                <td><label class="c-container"><input type="checkbox" name="attendance[{{$key}}][]" @if($data->attendance=='2') checked @endif value="2" class="absent"><span class="checkmark"></span></label></td>
                                                <td><label class="c-container"><input type="checkbox" name="attendance[{{$key}}][]" @if($data->attendance=='3') checked @endif value="3" class="late"><span class="checkmark"></span></label></td>
                                                <td><label class="c-container"><input type="checkbox" name="attendance[{{$key}}][]" @if($data->attendance=='4') checked @endif value="4" class="holiday"><span class="checkmark"></span></label></td>
                                                <td><input type="text" name="reason[]" value="{{$data->reason}}" placeholder="ហេតុផល របស់សិស្ស"></td>
                                            </tr>
                                        @endforeach

                                        <tr>
                                          <td colspan="100"><button type="submit" class="btn btn-primary float-right">{{trans('controls.students.save_attendance')}}</button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>

                            @endif
                    </div>
                </div>

            </div>
          </div>
        </div>



     </section>





@endsection


@push('custom_js')

<!-- DataTables  & Plugins -->
<script src="{{asset('themes')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/jszip/jszip.min.js"></script>
<script src="{{asset('themes')}}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{asset('themes')}}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<!-- date-range-picker -->
<script src="{{asset('themes')}}/plugins/daterangepicker/daterangepicker.js"></script>

<script src="{{asset('dropzone/dist/dropzone.js')}}"></script>



<script type="text/javascript">
    // getData('');

    $('#class_id').attr('disabled', 'disabled');

	function getData(val) {
		var _token=$('input[name=_token]').val();
		var level_id=$('select[name=level_id]').val();
        if(level_id !=""){
            $('#class_id').removeAttr('disabled');
        }else{
            $('#class_id').attr('disabled', 'disabled');
        }
		$.ajax({
			type: "POST",
			url: "{{url('admin/classes/get_classes')}}",
			data:{_token:_token,level_id:level_id},
			beforeSend: function(){
				$("#preloader").css("display","block");
			},success: function(classes){
				$("#preloader").css("display","none");
				$('select[name=class_id]').html(classes);
			}
		});
	}

	$("input:checkbox").on('click', function() {

		var $box = $(this);
		if ($box.is(":checked")) {
			var group = "input:checkbox[name='" + $box.attr("name") + "']";
			$(group).prop("checked", false);
			$box.prop("checked", true);
		} else {
			$box.prop("checked", false);
		}
	});

	function present(source) {
		$(".absent,.late,.present,.holiday,#late_all,#absent_all,#holiday_all").prop("checked",false);
		var checkboxes = document.querySelectorAll('.present');
		for (var i = 0; i < checkboxes.length; i++) {
			if (checkboxes[i] != source)
				checkboxes[i].checked = source.checked;
		}
	}
	function absent(source) {
		$(".absent,.late,.present,.holiday,#present_all,#late_all,#holiday_all").prop("checked",false);
		var checkboxes = document.querySelectorAll('.absent');

		for (var i = 0; i < checkboxes.length; i++) {
			if (checkboxes[i] != source)
				checkboxes[i].checked = source.checked;
		}
	}
	function late(source) {
		$(".absent,.late,.present,.holiday,#present_all,#absent_all,#holiday_all").prop("checked",false);
		var checkboxes = document.querySelectorAll('.late');
		for (var i = 0; i < checkboxes.length; i++) {
			if (checkboxes[i] != source)
				checkboxes[i].checked = source.checked;
		}
	}

	function holiday(source) {
		$(".absent,.late,.present,.holiday,#present_all,#absent_all").prop("checked",false);
		var checkboxes = document.querySelectorAll('.holiday');
		for (var i = 0; i < checkboxes.length; i++) {
			if (checkboxes[i] != source)
				checkboxes[i].checked = source.checked;
		}
	}
</script>

@endpush
