@extends('layouts.admin')

@push('custom_css')
    <style>
        body {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
            background-color: #FAFAFA;
            /* font: 12pt "Tahoma"; */
        }
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }
        .page {
            width: 210mm;
            min-height: 297mm;
            padding: 20mm;
            margin: 10mm auto;
            /* border: 1px #D3D3D3 solid; */
            border-radius: 5px;
            /* background: white; */
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            /* background-image:url({{asset('images/alis_frame.png')}}) */

        }
        .subpage {
            padding: 1cm;
            /* border: 5px rgb(243, 242, 242) solid; */
            height: 257mm;
            /* outline: 1cm #ffffff solid; */
        }

        @page {
            size: A4;
            margin: 0;

        }
        @media print {
            html, body {
                padding-top: 0px;
                width: 290mm !important;
                height: 310mm !important;
                /* background: rgb(255, 255, 255); */
            /* backgournd-image:url({{asset('images/bg_card.jpg')}}); */
                /* background-image:url({{asset('images/bg_card.jpg')}}); */

            }

            .bgSizeCover {
                background-image: url('{{asset("images/bg_card.jpg")}}');
                background-size: cover;
                /* background-size: auto;
                background-repeat: no-repeat !important; */
                /* width: 160px;
                height: 160px; */
                /* width: 210mm; */
                width: 320mm !important;
                /* height: 297mm; */
                height: 448mm !important;

                margin-top: -2cm !important;
                margin-left: -30px !important;

                /* border: 2px solid; */
                /* color: pink; */
                resize: both;
                /* overflow: scroll; */
            }

            .subpage {
                padding-top: 4cm;
                /* border: 5px rgb(243, 242, 242) solid; */
                /* height: 257mm; */
                /* outline: 1cm #ffffff solid; */
            }


            .noPrint{
                    display: none !important;
                }

            .page {
                margin-top: 0px;
                /* margin: 0; */
                border: initial;
                border-radius: initial;
                width: initial;
                min-height: initial;
                box-shadow: initial;
                background: initial;
                /* page-break-after: always; */


            }
            .pageBreak {
                    page-break-after: always !important;
            }
        }
    </style>
    <style>
        .page_break{
            /* page-break-after: always !important; */
            page-break-before: always !important;
        }
    </style>
@endpush

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">{{trans('controls.top five students')}}  </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('/')}}">{{trans('controls.home')}}</a></li>
            <li class="breadcrumb-item active">{{trans('controls.top five students')}}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      {{-- Score List --}}
      <div class="noPrint">
        <div class="row">
                <div class="col-md-12 ">
                    <div class="card card-outline card-info">
                        <div class="card-body">
                            <div class="row">
                                @foreach ($class_score as $item)
                                <div class="col-md-4">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-chalkboard-teacher"></i></span>

                                            <div class="info-box-content">
                                                <a href="{{url('/student-top5/'.$item->class_id)}}" style="text-decoration: none;">
                                                    <span class="info-box-text">{{getName('classes','name',$item->class_id)}}</span>
                                                </a>
                                                <span class="info-box-number text-blue">
                                                    <small>បង្រៀនដោយ:</small> {{getName('teachers','name_kh',getName2('classes','teacher_id',$item->class_id))}}
                                                    <span><a  class="text-white btn btn-primary btn-sm" href="{{url('/student-top5_print/'.$item->class_id)}}"><i class="fas fa-print"></i> ព្រីន</a></span>

                                                </span>
                                            </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                <!-- /.info-box -->


                            </div>

                                @endforeach

                            </div>

                        </div>
                    </div>
                </div>
        </div>
    </div>



    <div>
        <div class="page_break bgSizeCover">
            <div class="row">
                <div class="col-md-12">
                    <div class="page">
                        <div>

                            <div>
                                        @php
                                            $score_rank=array();
                                        @endphp

                                        @foreach ($student_score as $item)
                                            @php
                                                $score_rank[]=$item->total_score;
                                            @endphp
                                        @endforeach
                                        {{-- {{$score_rank}} --}}
                                        @php
                                        rsort($score_rank);

                                            $arrlength = count($score_rank);
                                            $rank = 1;
                                            $prev_rank = $rank;
                                        @endphp
                                    @foreach ($student_score as $key => $item)
                                            @if ($key<5)
                                                @php
                                                $student_top5[$key]=$item->student_name;
                                                $student_top5_img[$key]=getName2('students','profile',$item->student_id);
                                                if ($key==0) {
                                                        // echo $score_rank[$key]."- Rank".($rank);
                                                        $rank2[$key]=$rank;
                                                        // echo $rank;
                                                    }
                                                    elseif ($score_rank[$key] != $score_rank[$key-1]) {
                                                        $rank++;
                                                        $prev_rank = $rank;
                                                        $rank2[$key]=$rank;
                                                        // echo $score_rank[$key]."- Rank".($rank);
                                                        // echo $rank;
                                                    }
                                                    else{
                                                                $rank++;
                                                                // echo $score_rank[$key]."- Rank".($prev_rank);
                                                                $rank2[$key]=$prev_rank;

                                                                // echo $prev_rank;
                                                            }
                                                            // echo "<br>";
                                                    @endphp

                                            @endif
                                    @endforeach


                            </div>


                                        <div class="subpage">

                                            {{-- <table > --}}
                                                {{-- <tbody> --}}
                                                    {{-- <tr> --}}
                                                        {{-- <td > --}}
                                                            <center>
                                                                <table class="printPageMargint">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>
                                                                                <table>
                                                                                    <tbody><tr>
                                                                                        <td rowspan="4" style="width:2480px;"><img style="width:100%;" src="{{asset('images/logo_banner.png')}}"></td>
                                                                                    </tr>

                                                                                </tbody></table>
                                                                            </th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th class="text-center">
                                                                                @php
                                                                                use KhmerDateTime\KhmerDateTime;
                                                                                    $date = Carbon\Carbon::now();// will get you the current date, time
                                                                                    $dateTime = KhmerDateTime::parse($date->format("Y-m-d"));

                                                                                        $dateTime->day(); // ២២
                                                                                        $dateTime->fullDay(); // ពុធ
                                                                                        $dateTime->month(); // ០៥
                                                                                        $dateTime->fullMonth(); // ឧសភា
                                                                                        $dateTime->year(); // ២០១៩
                                                                                        $dateTime->minute(); // ០០
                                                                                        $dateTime->hour(); // ០០
                                                                                        $dateTime->meridiem(); // ព្រឹក
                                                                                        $dateTime->week(); // ៤
                                                                                        $dateTime->fullWeek(); // សប្តាហ៍ទី៤
                                                                                        $dateTime->weekOfYear(); // ២១
                                                                                        $dateTime->fullWeekOfYear(); // សប្តាហ៍ទី២១
                                                                                        $dateTime->quarter(); // ២
                                                                                        $dateTime->fullQuarter(); // ត្រីមាសទី២

                                                                                @endphp


                                                                                <h3 style="color:#0e13b0 !important; font-size:20px; font-family:Khmer OS Muol Light !important ; font-weight: normal !important;">តារាងកិត្តិយស</h3>
                                                                                <p style="display:inline !important; color:#0e13b0 !important; font-size:18px; font-family:Khmer OS Muol Light !important; font-weight: normal !important;">ប្រចាំ {{ $dateTime->fullMonth()}} </p>
                                                                                <p style="display:inline !important; color:#f23838 !important; font-size:18px; font-family:Khmer OS Muol Light !important ; font-weight: normal !important;">{{getName('classes','name',$item->class_id)}}</p><p></p>

                                                                            </th>
                                                                        </tr>
                                                                    </thead>


                                                                        <tbody>
                                                                            <tr>
                                                                            <td>
                                                                                <center>
                                                                                    <div style="width:26%; height:220px;">
                                                                                        <div class="card-body box-profile">
                                                                                            <div class="text-center">
                                                                                                <img style="border-bottom:2px solid #0e13b0;border-radius:5px;" height="140px" width="120px" src="{{asset('images/students/'. $student_top5_img[0])}}">

                                                                                            {{-- <img class="profile-user-img img-fluid img-circle" src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200" alt="User profile picture"> --}}
                                                                                            </div>

                                                                                            <h3 class="profile-username text-center text-red">លេខ {{$rank2[0]}}</h3>
                                                                                            <h5 class="text-center">{{$student_top5[0]}}</h5>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div style="margin-top:-20%;  width:25%; height:220px; float:left !important">
                                                                                        <div class="card-body box-profile">
                                                                                            <div class="text-center">
                                                                                                <img style="border-bottom:2px solid #0e13b0;border-radius:5px;" height="140px" width="120px" src="{{asset('images/students/'. $student_top5_img[1])}}">

                                                                                            {{-- <img class="profile-user-img img-fluid img-circle" src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200" alt="User profile picture"> --}}
                                                                                            </div>

                                                                                            <h3 class="profile-username text-center text-red">លេខ {{$rank2[1]}}</h3>
                                                                                            <h5 class="text-center">{{$student_top5[1]}}</h5>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div style="margin-top:-20%; width:25%; height:220px; float:right !important">
                                                                                        <div class="card-body box-profile">
                                                                                            <div class="text-center">
                                                                                                <img style="border-bottom:2px solid #0e13b0;border-radius:5px;" height="140px" width="120px" src="{{asset('images/students/'. $student_top5_img[2])}}">

                                                                                            {{-- <img class="profile-user-img img-fluid img-circle" src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200" alt="User profile picture"> --}}
                                                                                            </div>

                                                                                            <h3 class="profile-username text-center text-red">លេខ {{$rank2[2]}}</h3>
                                                                                            <h5 class="text-center">{{$student_top5[2]}}</h5>

                                                                                        </div>
                                                                                    </div>
                                                                                    <div style="clear:both !important"></div>
                                                                                    <div style="margin-top:3%;width:25%; height:220px; float:left !important">
                                                                                        <div class="card-body box-profile">
                                                                                            <div class="text-center">
                                                                                                <img style="border-bottom:2px solid #0e13b0;border-radius:5px;" height="140px" width="120px" src="{{asset('images/students/'. $student_top5_img[3])}}">

                                                                                            {{-- <img class="profile-user-img img-fluid img-circle" src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200" alt="User profile picture"> --}}
                                                                                            </div>

                                                                                            <h3 class="profile-username text-center text-red">លេខ {{$rank2[3]}}</h3>
                                                                                            <h5 class="text-center">{{$student_top5[3]}}</h5>

                                                                                        </div>
                                                                                    </div>

                                                                                    <div style="margin-top:3%;width:25%; height:220px; float:right !important">

                                                                                        <div class="card-body box-profile">
                                                                                            <div class="text-center">
                                                                                                <img style="border-bottom:2px solid #0e13b0;border-radius:5px;" height="140px" width="120px" src="{{asset('images/students/'. $student_top5_img[4])}}">

                                                                                            {{-- <img class="profile-user-img img-fluid img-circle" src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200" alt="User profile picture"> --}}
                                                                                            </div>

                                                                                            <h3 class="profile-username text-center text-red">លេខ {{$rank2[4]}}</h3>
                                                                                            <h5 class="text-center">{{$student_top5[4]}}</h5>

                                                                                        </div>
                                                                                    </div>
                                                                                </center>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>


                                                                    <tfoot>
                                                                        <tr>
                                                                            <th>
                                                                                <table style="width:100%; margin-top:3%">
                                                                                    <tbody><tr>
                                                                                        <th style="width:50%; font-weight: normal !important; vertical-align : top !important; text-align:center !important" class="text-center;">
                                                                                            បានឃើញ និងឯកភាព <br>
                                                                                        <!--
                                                                                        <div style="font-family:Khmer OS Muol Light">ជ.នាយិកាសាលា </div>
                                                                                            <div style="font-family:Khmer OS Muol Light">នាយិការង </div>
                                                                                            -->

                                                                                        </th>
                                                                                        <td style="width:50%" class="text-center">
                                                                                            <div style="font-size:12px; font-weight: normal !important;">ថ្ងៃ​ ព្រហស្បតិ៍ ១០ កើត ខែ​ មាឃ ឆ្នាំច សំរិទ្ធិស័ក ព.ស ២៥៦២</div>
                                                                                            <!-- <div style="font-size:12px; font-weight: normal !important;">រាជធានីភ្នំពេញ ថ្ងៃទី............ខែ................ឆ្នាំ............</div> -->
                                                                                            <div style="font-family:Khmer OS Muol Light; font-weight: normal !important;">គ្រូប្រចាំថ្នាក់</div>
                                                                                            <div style="margin-top:20%">

                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody></table>
                                                                            </th>
                                                                        </tr>
                                                                    </tfoot>

                                                                </table>
                                                            </center>
                                                        {{-- </td> --}}
                                                    {{-- </tr>
                                                </tbody>
                                            </table> --}}
                                        </div>



                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="page_break">
        <div class="row page">
            <div class="col-md-12">
                <div class="card card-outline card-info">
                    <div class="card-body">
                        <h4 class="card-title">{{trans('controls.top five students')}}</h4>
                        <div class="table-responsive">
                            <div style='break-after:always'></div>
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col" style="width:300xp;">{{trans('controls.students.name')}}</th>
                                        <th scope="col">{{trans('controls.students.gender')}}</th>
                                        @php
                                                $score_subject = App\Models\Score::select('scores.*','student_id')
                                                ->join("students","students.id","=","scores.student_id")
                                                // ->with('attendance')
                                                // ->where('student_id','=',$item->student_id)
                                                // ->where('month','=',$date->format("m"))
                                                // ->where('year','=',$date->format("Y"))
                                                //    ->where('attendance','=',2)
                                                ->where('class_id','=',1)
                                                ->groupBy("scores.subject_id")
                                                ->get();
                                        @endphp
                                            @foreach ($score_subject as $item)
                                                <th>{{$item->subject_name}}</th>
                                            @endforeach
                                        <th scope="col">ពិន្ទុសរុប</th>
                                        <th scope="col">ម.ភាគ</th>
                                        <th scope="col">ចំ.ថ្នាក់</th>
                                        <th scope="col">និទ្ទេស</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $score_rank=array();
                                        @endphp

                                        @foreach ($student_score as $item)
                                            @php
                                                $score_rank[]=$item->total_score;
                                            @endphp
                                        @endforeach
                                        {{-- {{$score_rank}} --}}
                                        @php
                                        rsort($score_rank);

                                            $arrlength = count($score_rank);
                                            $rank = 1;
                                            $prev_rank = $rank;


                                        @endphp

                                        {{-- @php
                                            $rank=1;
                                            $prev_rank = $rank;
                                        @endphp --}}

                                    @foreach ($student_score as $key => $item)
                                    <tr>
                                        <th scope="row">{{$item->student_id}}</th>
                                        <td>{{$item->student_name}}</td>
                                        <td>{{getName('students','gender',$item->student_id)}}</td>
                                        @php
                                                $score_subject = App\Models\Score::select('scores.*','student_id')
                                                ->join("students","students.id","=","scores.student_id")
                                                // ->with('attendance')
                                                ->where('student_id','=',$item->student_id)
                                                // ->where('month','=',$date->format("m"))
                                                // ->where('year','=',$date->format("Y"))
                                                //    ->where('attendance','=',2)
                                                //    ->where('gender','=','ស')
                                                // ->groupBy("scores.class_id")
                                                ->get();
                                        @endphp
                                            @foreach ($score_subject as $item)
                                                <td>{{$item->score}}</td>
                                            @endforeach
                                            <td class="text-blue">{{number_format(($item->total_score), 2, '.', '')}}</td>
                                            <td class="text-blue">{{number_format(($item->total_score/$score_subject->count()), 2, '.', '')}}</td>
                                        {{-- <td class="{{ number_format(($item->total_score/$score_subject->count()), 2, '.', '')<4.99?'text-red':'text-blue'}}">{{number_format($item->total_score, 2, '.', '')}} </td> --}}
                                        {{-- <td class="{{ number_format(($item->total_score/$score_subject->count()), 2, '.', '')<4.99?'text-red':'text-blue'}}">{{number_format(($item->total_score/$score_subject->count()), 2, '.', '')}}</td> --}}
                                        <td class="text-red">
                                            @php

                                                    if ($key==0) {
                                                        // echo $score_rank[$key]."- Rank".($rank);
                                                        echo $rank;
                                                    }
                                                    elseif ($score_rank[$key] != $score_rank[$key-1]) {
                                                        $rank++;
                                                        $prev_rank = $rank;
                                                        // echo $score_rank[$key]."- Rank".($rank);
                                                        echo $rank;
                                                    }
                                                    else{
                                                        $rank++;
                                                        // echo $score_rank[$key]."- Rank".($prev_rank);
                                                        echo $prev_rank;
                                                    }
                                                    echo "<br>";
                                            @endphp

                                        </td>
                                        <td>
                                            {{checkCredit(number_format(($item->total_score/$score_subject->count()), 2, '.', ''))}}
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






    </div><!--/. container-fluid -->
  </section>
  <!-- /.content -->
@endsection


