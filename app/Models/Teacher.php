<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    use HasFactory;

    protected $fillable=[
        'user_id','school_id',
        'name_kh','name_en','gender','dob','pob','address',
        'city','city_slug','is_active','status','profile',
        'bio','created_at','updated_at'
    ];
}
