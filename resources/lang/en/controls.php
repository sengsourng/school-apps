<?php

return [
'menus'	=>[
		'list'		=>	'List',
		'students'	=>	'Students',
		'teachers'	=>	'Teachers',
		'skills'	=>	'Skills',
		'subjects'	=>	'Subjects',
		'sections'	=>	'Sections',
		'rooms'		=>	'Rooms',
		'classes'	=>	'Classes',		
		'credit_score'	=>	'Credit Scores',
		'exams'			=>	'Examinations',
		'exam_details'			=>	'Examination Details',
		
		'attendances'			=>	'Attendances',
		'attendance_teachers'			=>	'Teacher Attendances',
		'attendance_students'			=>	'Attendance Teachers',
		
		'monthly_top_five'	=>	'Monthly Top Five',
		'yearly_top_five'	=>	'Yearly Top Five',
		'result_by_student'	=>	'Result by Students',
		
	],
	
    // Main Menu
    'dashboard'         =>'Dashboard',
    'home'              =>'Home',
    'contact-us'        =>'Contact',
    'about'             =>'About us',
    'login'             =>'Login',
    'teacher register'     =>'Teacher Register',
    'student register'     =>'Student Register',
    'top five students'     =>'Top 5 Students',
    'student absent'        =>'Student Absent',
    'result exam'            =>'Result Exam',

    'Are you sure?'         =>'Are you sure?',
    'Yes, delete it!'         =>'Yes, delete it!',
    'cancel'         =>'Cancel',
    'save close'         =>'Save Close',
    'save'         =>'Save',
    'You will not be able to delete this!' =>   'You will not be able to delete this!',
    'Your data has been saved or updated!' =>   'Your data has been saved!',
    'Your file has been deleted.'   =>  'Your file has been deleted.',
    'delete'   =>  	"Delete !",
    'delete_message_can_not' => 'You can not delete !',
	
	'student_report_by_class' =>'Student Class Report',
	'student_score_by_class' =>'Student Score Class',



    'logout'     =>'Logout',
	
	
	
	//Attendance វត្តមានសិស្ស
	'Present'	=>'Present',
	'Absent'		=>'Absent',
	'Late'			=>	'Late',
	'student_atten_report' =>	'ReportStudent Attendance',
	'att_times'		=>	'Times',

    // Teacher
    'teachers'     =>[
            'all'       =>'List Teachers',
			'code'	=> 'Code',
            'name'      =>'Teacher Name',
            'full_name'      =>'Full Name',
            'gender'      =>'Gender',
            'pob'      =>'Place Of Birth',
            'dob'      =>'Date Of Birth',
            'address'      =>'Address',
            'profile'      =>'Profile',

        ],

	//Student
	'students'		=>[
			'id'	=>'ID',
			'code'	=>'Code',
			'all'	=>'List Students',
			'name'	=>'Full Name (ខ្មែរ)',
			'first_name'	=>'First Name',
			'last_name'	=>'Last Name',
			'name_en'	=>'Latin Name',
			'gender'	=>	'Gender',
			'dob'		=>	'Birth Date',
			'pob'		=>	'Place of Birth',
			'address'	=>	'Address',
			'profile'	=>	'Profile',
			
			'father'	=>	'Father Name',
			'father_job'	=>	'Father Job',
			'mother'	=>	'Mother Name',
			'mother_job'	=>	'Mother Job',
			'city'			=>	'City',
			'city_slug'		=>	'City Slug',
			'register_year'	=>	'Register Year',
			'status'		=>	'Status',
			'bio'			=>	'Bio',
			'add'			=>	'Add New',
			'export'		=>	'Export',
			'import'		=>	'Import',
			'manage_attendance'	=>	'Manage Attendance',
			'student_attendance'	=>	'Student Attendance',
			'save_attendance'	=>	'Save Attendance',
			'date'			=> 'Date',
			'active'	=>	'Active',
			'is_active'	=>	'Is Active',
			'disable'	=>	'Disable',
			'filter_student'	=>	'Filter Student',
			'edit'	=> 'Edit Student',
			],



    'sent' => 'Sent',
    'users' => "All Users",
    'user' => "User",
    'name'  =>'Name',
    'full_name' => "Full Name",

    'users'     =>[
        'email'         => 'Email',
        'email_place_holder'         => 'yourname@gmail.com',
        'username'      =>'Username',
        'password'      =>'Password',
        'password_place_holder'      =>'Enter Password',
        'forgot password'      =>'Forgot your password?',
        ],

        'schools'   =>[
            'list'  =>'School List',
            'name'  =>'School Name',
            'phone'  =>'Phone',
            'email'  =>'Email',
            'city'  =>'Province/City',
            'address'  =>'Address',
            'director_name'  =>'Director',
        ],


    'forms'         =>[
        'Add'   =>  "Add",
        'Add New'   =>  "Add New",
        'Add More'   =>  "Add More",
        'Edit'   =>  "Edit",
        'Delete'   =>  "Delete",
        'Update'   =>  "Update",
        'Show'   =>  "Show",
        'Upload'   =>  "Upload",
        'Created By'   =>  "Created By",
        'Please Select'   =>  "Please Select",
        'Select All'   =>  "Select All",
        'Show All'   =>  "Show All",
        'List'   =>  "List",
        'Action'   =>  "Action",
        'ID'   =>  "ID",
        'Code'   =>  "Code",
        'Male'   =>  "Male",
        'Female'   =>  "Female",
		'created_at'	=> 'Created At',
		'brow_image'	=> 'Choose',
		'remove'	=> 'Remove',
		'is_active' => 'Status',
		'print_report' =>'Print Report',
		'print' => 'Print',
		'filter'	=> 'Filter',
    ],
	//Acadamic_year
	'acadamic_years' => 	[
			'name'		=>	"name",
			'year'		=>	"year",
			'decription'=>	"decription",
			'active'	=>	"active",
			'disactive' =>	"not active",
			'list'		=>	"Years list",
			'month'	=>	"Monthly",
	],
	
	//Levels
	'levels'		=>	[
		'id'	=>	'ID',
		'code'	=>	'Code',
		'name'	=>	'Levels',
		'description'	=>	'Description',
		'status'	=>	'Status',
		'select'	=>	'Select Level',
		
	],
	
	
	//Classes
	'classes'		=>	[
		'id'	=>	'ID',
		'list'	=>	'List Classes',
		'code'	=>	'Code',
		'name'	=>	'Class Name',
		'description'	=>	'Description',
		'status'	=>	'Status',
		'select'	=>	'Select Class',		
	],
	//Sections
	'sections'		=>	[
		'id'	=>	'ID',
		'code'	=>	'Code',
		'list'	=>	'Section List',
		'name'	=>	'Section Name',
		'description'	=>	'Description',
		'status'	=>	'Status',
		'score_type'	=>	'Score Type',
		'select'	=>	'Select Section',
		'average'	=>	'Average',
		'formula'	=>	'Formula',
		
	],
	//Skills
	'skills'		=>	[
		'id'	=>	'ID',	
		'list'	=>	'List Skills',
		'name'	=>	'Skill Name',
		'description'	=>	'Description',
		'status'	=>	'Status',
		'select'	=>	'Select Skill',		
	],
	
	
	//Subjects
	'subjects'		=>	[
		'id'	=>	'ID',
		'code'	=>	'Code',
		'list'	=>	'List Subjects',		
		'name'	=>	'Subject Name',
		'description'	=>	'Description',
		'status'	=>	'Status',
		'select'	=>	'Select Subject',
		'active'	=>	'Active',
		'disable'	=>	'Disable',
		'sort'	=>	'sort',
	],
	
	//Rooms
	'rooms'		=>	[
		'id'	=>	'ID',
		'code'	=>	'Code',
		'list'	=>	'List Rooms',		
		'name'	=>	'Room Name',
		'description'	=>	'Description',
		'status'	=>	'Status',
		'select'	=>	'Select Room',
		'active'	=>	'Active',
		'disable'	=>	'Disable',
		'sort'	=>	'Sort',
	],
	
	//Scores
	'scores'		=>	[
		'manage'	=>	'Manage Score',
		'save'	=>	'Save Score',
		'score'	=>	'Score',
		'avg'	=>	'Avg',		
		'total'	=>	'Total',
		'rank'	=>	'Rank',
		'subject'	=>	'Subject',
		'level'	=>	'Level',
		'class'	=>	'Class',
		'credit'	=>	'credit',
		
	],
	
];
