<?php

namespace App\Http\Controllers;

use App\Models\School;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['schools']=School::get();
        return view('backend.schools.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if( request()->ajax()){
		    return view('backend.schools.modal.add');
		}
        return view('backend.schools.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = [
            'status' => false,
            'data' => [],
        ];
        $add = School::create([
            // 'business_id'  =>  1,
            'name'          => $request->name,
            'description'    => $request->description,
            'created_by'    => auth()->user()->id
        ]);

        if ($add) {
            $response = [
                'status' => true,
                'data' => $add,
                'dom' => view('backend.schools.rows',['row' => $add])->render(),
            ];
        }
        if(request()->ajax()){

            return $response;
        }
        // dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(School $school)
    {
        $user=User::findOrFail($school->user_id);
        $teacher=Teacher::findOrFail($school->user_id);
        // dd($user);
        if( request()->ajax()){
		    return view('backend.schools.modal.show',compact('school','user','teacher'));
		}

        // return view('backend.schools.show',compact('school','user','teacher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(School $school)
    {

        $user=User::findOrFail($school->user_id);
        $teacher=Teacher::findOrFail($school->user_id);
        // dd($user);
        if( request()->ajax()){
		    return view('backend.schools.modal.edit',compact('school','user','teacher'));
		}

        return view('backend.schools.edit',compact('school','user','teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,School $school)
    {
        $response = [
            'status' => false,
            'data' => [],
        ];

        $teacher_name=explode("-",$request->teacher_name);
        $update = $school->update([
            'name'          => $request->school_name,
            'email'          => $request->school_email,
            'phone'    => $request->school_phone,
            'city'    => $request->school_city,
            'city_slug' => $this->makeSlug($request->school_city),
            'address'    => $request->school_address,
            'director_name'    => $teacher_name[0],


            // 'user_id'    => auth()->user()->id
        ]);

        $teacher=Teacher::findOrFail(auth()->user()->id);
        $teacher_name=explode("-",$request->teacher_name);
        $data_teacher=[
            'name_kh'   =>$teacher_name[0],
            'name_en'   =>$teacher_name[1],
            'gender'    => $request->gender,
            'dob'    => $request->teacher_dob,
            'pob'    => $request->teacher_pob,
            'address'    => $request->teacher_address,
            'city'    => $request->teacher_city,
            'city_slug' => $this->makeSlug($request->teacher_city),

        ];
        $teacher->update($data_teacher); //Update Teacher Table


        $user=User::findOrFail(auth()->user()->id);
        $full_name=explode(" ",$teacher_name[0]);
        $username=explode("@",$request->teacher_email);

        $data_user=[

            'name'    => $teacher_name[0],
            'fname'   =>$full_name[0],
            'lname'   =>$full_name[1],
            'username'    => $username[0],
            'email'    => $request->teacher_email,
            // 'username'    => $request->gender,

        ];
        $user->update($data_user); //Update Teacher Table

        if ($update) {
            $response = [
                'status' => true,
                'data' => $school,
                'dom' => view('backend.schools.rows',['row' => $school])->render(),
            ];
        }
        if(request()->ajax()){
            return $response;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = [
            'status' => false,
        ];
        $school=School::findOrFail($id);


        $userCheck=User::where('id','=',$id)->count();
        // dd($userCheck);

        if ($userCheck == 0) {
            $school->delete();
            $response = [
                'status' => true,
            ];
        }


        if (request()->ajax()) {
            return $response;
        }

        return redirect()->route('schools.index');
    }
}
