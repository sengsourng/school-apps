@extends('layouts.admin')

@push('custom_css')

<!-- DataTables -->
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

 <!-- daterange picker -->
 <link rel="stylesheet" href="{{asset('themes')}}/plugins/daterangepicker/daterangepicker.css">
@endpush

{{-- @push('title','Manage Brands')

@section('content-header',"Manage Brands") --}}


@section('content')

     <!--Section: Sales Performance KPIs-->
     <section class="content pt-2">
        <div class="card">
          <div class="card-header text-left py-3" style="height: 9vh;">
            <h5 class="mb-0 text-left">

                <strong>{{trans('controls.students.import')}}</strong>

                <span >
                    <a href="{{route('students.create')}}"
                    class="btn btn-primary btn-sm float-right"

                    data-toggle="modal-ajax"
                    {{-- data-toggle="modal" --}}
                    {{-- data-modal-size="modal-lg" --}}
                    data-modal-size="modal-xl"
                    {{-- modal-xl --}}
                    {{-- data-mdb-toggle="modal" --}}
                    {{-- data-target="#exampleModal" --}}
                    data-target="#openModal"
                    >
                    <i class="fas fa-plus-circle"></i> {{trans('controls.students.add')}}</a>
                </span>



            </h5>
          </div>


          <div class="card-body">

            @if (session('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>
            @endif

            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @endif

            <div>
                <form action="{{ route('students.import') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="skill_id">{{trans('controls.skills.name')}}</label>
                                <select name="skill_id" id="skill_id" class="form-control">
                                    <option value="1">កម្មវិធីភាសាខ្មែរ</option>
                                    <option value="2">កម្មវិធីភាសាអង់គ្លេស</option>
                                    <option value="3">កម្មវិធីភាសាចិន</option>
                            </select>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">{{trans('controls.levels.name')}}</label>
                                <select name="level_id" class="form-control select2" onChange="getData(this.value);" required>
                                    <option value="">{{ trans('controls.levels.select') }}</option>
                                    {{ create_option('levels','id','name') }}
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">{{trans('controls.classes.name')}}</label>
                                <select name="class_id" id="class_id" class="form-control select2" required>
                                    <option value="">{{ trans('controls.classes.select') }}</option>
                                    {{ create_option('classes','id','name') }}
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="date">{{trans('controls.students.date')}}</label>
                                <input type="date" id="date" class="form-control" name="date">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="study_year">{{trans('controls.students.register_year')}}</label>
                                <select name="study_year" id="study_year" class="form-control">
                                        {{ create_option('academic_years','year','name') }}
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            {{-- <div class="form-group">
                                <label for="level_id">Upload Student File</label>
                                <input type="file" name="file" class="form-control">
                            </div> --}}

                            <div class="form-group mt-4">
                                <button type="button" class="btn btn-primary btn-block">
                                    <label>
                                        <i class="fas fa-paperclip"></i> {{trans('controls.forms.brow_image')}}
                                        <input class="btn-block" type="file" id="uploadImage" onchange="Preview_Image_Before_Upload('uploadImage', 'uploadPreview');" name="file"/>
                                    </label>
                                </button>
                            </div>

                        </div>

                        <div class="col-md-6">
                            <div class="form-group mt-4">
                                <button class="btn btn-success">Import Student Data</button>
                                 <a class="btn btn-warning" href="{{ route('students.export') }}">Export Student Data</a>
                            </div>
                        </div>
                    </div>


                </form>
            </div>
          </div>


        </div>
     </section>
      <!--Section: Sales Performance KPIs-->

{{-- @endif --}}


@endsection


@push('custom_js')

<!-- DataTables  & Plugins -->
<script src="{{asset('themes')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/jszip/jszip.min.js"></script>
<script src="{{asset('themes')}}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{asset('themes')}}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<!-- date-range-picker -->
<script src="{{asset('themes')}}/plugins/daterangepicker/daterangepicker.js"></script>

<script src="{{asset('dropzone/dist/dropzone.js')}}"></script>



<script>

    $('#class_id').attr('disabled', 'disabled');

	function getData(val) {
		var _token=$('input[name=_token]').val();
		var level_id=$('select[name=level_id]').val();
        if(level_id !=""){
            $('#class_id').removeAttr('disabled');
        }else{
            $('#class_id').attr('disabled', 'disabled');
        }
		$.ajax({
			type: "POST",
			url: "{{url('admin/classes/get_classes')}}",
			data:{_token:_token,level_id:level_id},
			beforeSend: function(){
				$("#preloader").css("display","block");
			},success: function(classes){
				$("#preloader").css("display","none");
				$('select[name=class_id]').html(classes);
			}
		});
	}


    $(document).ready(function() {
           $('#DataTableList').DataTable({
            ordering:true,
            processing: false,
            // serverSide: true,
            "order": [[ 4, "asc" ]],
            language : $('html').attr('lang') != 'en' ? window.datatableI18n[$('html').attr('lang')] : null,
        });
    } );

        $(document).on('click',`[data-toggle="modal-ajax"]`,function(ev){
                    ev.preventDefault();
                    var url = $(this).attr('href');
                    var modalSize = $(this).data('modal-size');
                    var title = $(this).text();
                    // var target = $(this).data('mdb-target');
                    var target = $(this).data('target');


                    $.get(url).done((res)=>{
                        var $modal = $(target);
                        $modal.find('.modal-title').text(title);
                        $modal.find('.modal-dialog').addClass(modalSize);
                        $modal.find('.modal-body').html(res);

                      //  $.getScript(`${location.origin }/themes/js/mdb.min.js`).done();

                        $modal.modal('show');

                        $modal.find('button#btn-submit').unbind().click(function(ev){
                            ev.preventDefault();

                            var $form = $modal.find('form');
                            var formData = new FormData($form[0]);
                            $.ajax({
                                url : $form.attr('action'),
                                method : 'post',
                                data : formData,
                                processData : false,
                                contentType : false,
                                success : (res)=>{
                                    if(res.status){

                                        Swal.fire({
                                            position: 'top-end',
                                            icon: 'success',
                                            title: '{{__('controls.Your data has been saved or updated!')}}',
                                            showConfirmButton: false,
                                            timer: 1500
                                        });


                                        if($(this).data('allow-close')){
                                            $modal.find(`[data-dismiss="modal"]`).click();
                                        }
                                        if ( url.match('edit')) {
                                            $('#DataTableList').find(`tr[data-id="${res.data.id}"]`).html($(res.dom).html());
                                        }else{
                                            $('#DataTableList').find('tbody').prepend(res.dom);
                                        }


                                        $form[0].reset();
                                    }

                                },
                            })

                        });
                        $modal.find(`[data-dismiss="modal"]`).click(function(ev){
                            ev.preventDefault();
                            $modal.modal('hide');
                            $modal.find('.modal-body').html("");

                        });


                    });

                });

</script>


<script type="text/javascript">
    $(document).on('submit',`[data-toggle="formDelete"]`,function(ev){
        ev.preventDefault();
        $.ajax({
            url : $(this).attr('action'),
            method : 'post',
            data : new FormData(ev.target),
            processData : false,
            contentType : false,
            success : (res)=>{
                if(res.status){
                    Swal.fire(
                        '{{trans('controls.delete')}}!',
                        '{{trans('controls.Your file has been deleted.')}}',
                        'success'
                    );
                    $(this).parents("tr").remove();
                }else{
                    Swal.fire(
                        '{{trans('controls.delete')}}!',
                        '{{trans('controls.delete_message_can_not')}}',
                        'error'
                    )
                }
            }

        })
    });

    function deleteItem(id){
      Swal.fire({
        title: "{{trans('controls.Are you sure?')}}",
        text: "{{trans('controls.You will not be able to delete this!')}}",
        icon: 'warning',
        showCancelButton: true,

        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText :"{{trans('controls.cancel')}}",
        confirmButtonText:  "{{trans('controls.Yes, delete it!')}}"
      }).then((result) => {
        if (result.value) {
          $('#frmDeleteItem-'+id).submit();

        }
      })
    }
</script>

@endpush
