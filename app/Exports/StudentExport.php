<?php

namespace App\Exports;

use App\Models\Student;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class StudentExport implements FromCollection,  WithHeadingRow, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // $students = User::where('name', 'like','%'.$this->request->name)->get();
        // $students = Student::where('id', '>',1)->get();
        $students = Student::all();

        $output = [];

        foreach ($students as $student)
        {
          $output[] = [
            $student->code,
            $student->name_kh,
            $student->name_en,
            $student->gender=='m'?'Male':'Female',
            $student->dob,
            $student->pob,
            $student->address,
          ];
        }
        return collect($output);
    }

    public function headings(): array
    {
        return [
            'ល.រ',
            'ឈ្មោះខ្មែរ',
            'ឡាតាំង',
            'ភេទ',
            'ឆ្នាំកំណើត',
            'កន្លែងកំណើត',
            'អាស័យដ្ឋាន',
        ];
    }


    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {

                //  $event->sheet->getDelegate()->getDefaultStyle()->getFont()->setName('Khmer OS Battambang');
                //  $event->sheet->getDelegate()->getDefaultStyle()->getFont()->setSize(12);

                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setName('Khmer OS Muol Light');
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(11);

                $cellRangeRow = 'B2:B200'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRangeRow)->getFont()->setName('Khmer OS Battambang');
                $event->sheet->getDelegate()->getStyle($cellRangeRow)->getFont()->setSize(11);

            },
        ];
    }
}
