<div class="container pt-1">
    <form  enctype="multipart/form-data">
        @csrf

        <div class="row">
            <div class="col-md-12 bg-info pt-3">

                  <div class="form-group row">
                        <label for="lable_s_name" class="col-sm-3 col-form-label">{{trans('controls.acadamic_years.name')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="name" disabled  class="form-control" id="lable_s_name"  value="{{$year->name}}">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="lable_s_phone" class="col-sm-3 col-form-label">{{trans('controls.acadamic_years.year')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="year" disabled class="form-control" id="lable_s_phone" value="{{$year->year}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lable_s_email" class="col-sm-3 col-form-label">{{trans('controls.acadamic_years.decription')}}</label>
                        <div class="col-sm-9">
                            <input type="email" name="description" disabled class="form-control" id="lable_s_email" value="{{$year->description}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lable_s_city" class="col-sm-3 col-form-label">{{trans('controls.acadamic_years.active')}}</label>
                        <div class="col-sm-9">
                           <select name="active" id="active" disabled class="form-control">
                            <option {{$year->is_active==1?'selected':''}} value="1">Active</option>
                            <option {{$year->is_active==0?'selected':''}} value="0">Not Active</option>
                           </select>
                        </div>
                    </div>




            </div>



        </div>

    </form>
</div>
