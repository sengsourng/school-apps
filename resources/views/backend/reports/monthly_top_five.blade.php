@extends('layouts.admin')

@push('custom_css')

<!-- DataTables -->
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

 <!-- daterange picker -->
 <link rel="stylesheet" href="{{asset('themes')}}/plugins/daterangepicker/daterangepicker.css">
@endpush


@section('content')

     <!--Section: Sales Performance KPIs-->
     <section class="content pt-2">
            <div class="row">
                    <div class="col-md-5">
                        <div class="card card-danger card-outline">
                            <div class="card-header">
                            <h3 class="card-title">Searching Class...</h3>
                            </div>
                            <div class="card-body">
                            {{-- <div class="row">
                                <div class="col-md-2 pb-1">
                                <input type="text" id='search' class="form-control" placeholder="Code class">
                                </div>
                                <div class="col-md-2 pb-1">
                                <input type="text" class="form-control" placeholder=".col-4">
                                </div>
                                <div class="col-md-2 pb-1">
                                <input type="text" class="form-control" placeholder=".col-5">
                                </div>
                                <div class="col-md-2 pb-1">
                                    <input type="text" class="form-control" placeholder=".col-5">
                                </div>
                                <div class="col-md-2 pb-1">
                                    <input type='button' class="btn btn-success mr-2 btn-block" value='Search' id='but_search'>

                                </div>

                                <div class="col-md-2 pb-1">

                                    <input type="submit" class="btn btn-success btn-block" value="Filter Class">
                                </div>

                            </div> --}}

                            <div class="card-body">
                                <div class="table-responsive">
                                <table class="table table-hover text-nowrap" id="DataTableList">
                                    <thead>
                                    <tr>
                                        <th scope="col" style="width: 40px !important;">{{trans('controls.forms.ID')}}</th>
                                        <th scope="col">{{__('controls.levels.name')}}</th>
                                        <th scope="col">{{__('controls.classes.name')}}</th>
                                        <th scope="col">{{__('controls.acadamic_years.month')}}</th>
                                        {{-- <th scope="col">{{__('controls.classes.description')}}</th> --}}
                                        {{-- <th scope="col">Gender</th> --}}
                                        <th scope="col">{{__('controls.acadamic_years.name')}}</th>
                                        <th scope="col">{{__('controls.forms.Action')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($class_score as $row)
                                            <tr data-id="{{ $row->id}}">
                                                <th scope="row" style="width: 40px !important;">{{$row->id}}</th>
                                                <td>
                                                    {{$row->level_name}}
                                                </td>
                                                <td>
                                                    {{$row->class_name}}
                                                </td>
                                                <td>
                                                    {{$row->month}}
                                                    {{-- {{getName('levels','name',$row->level_id)}} --}}
                                                </td>



                                                <td>
                                                    <span class="text-success">
                                                    {{$row->study_year}}

                                                        {{-- <i class="fas fa-user me-1"></i> {{getName('teachers','name_kh',$row->teacher_id)}} --}}
                                                        {{-- <i class="fas fa-clock me-1"></i><span> {{ session()->get('locale')=='kh'?KhmerDateTime\KhmerDateTime::parse($row->created_at)->fromNow():$row->created_at->diffForHumans() }}</span> --}}
                                                    </span>

                                                </td>


                                                <td>
                                                    <a class="btn btn-primary  edit_button btn-sm" href="{{url('student-top5_print/'.$row->class_id)}}"
                                                            {{-- data-toggle="modal-ajax" --}}
                                                            {{-- data-modal-size="modal-lg" --}}
                                                            {{-- data-modal-size="modal-xl" --}}
                                                            {{-- data-mdb-toggle="modal" --}}
                                                            {{-- data-target="#openModalPrint" --}}
                                                    >
                                                    <i class="fa fa-eye left"></i> {{ trans('controls.forms.Show')}}</a>



                                                    {{-- <a href="javascript:deleteItem({{ $row->id }})"​​ class="btn btn-danger delete_button btn-sm"><i class="fa fa-trash-alt left"></i> {{ trans('controls.forms.Delete')}}</a> --}}


                                                    {{-- <form data-toggle="formDelete"  id="frmDeleteItem-{{ $row->id }}" style="display: none" action="{{ route('classes.destroy',$row->id) }}" role="form" method="POST" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                    </form> --}}

                                                </td>
                                            </tr>
                                        @endforeach


                                    </tbody>
                                </table>
                                </div>
                            </div>

                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>



                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-md-5">
                                <input type='text' id='class_id' class="form-control mr-2" name='class_id' value="1">
                            </div>
                            <div class="col-md-5">
                                <input type='text' id='section_id' class="form-control mr-2" name='section_id' value="1">
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-primary btn-block rect-btn" id='but_search'><i class="fas fa-filter"></i> {{trans('controls.forms.filter')}}</button>

                            </div>
                        </div>
                        <div class="row pt-2">
                            <div class="col-md-12 ">
                                <div class="table-responsive">
                                    <h3>តារាងពិន្ទុសិស្សប្រចាំខែ</h3>
                                    <table class="table table-bordered" id='empTable'>
                                        <thead>
                                        <tr>
                                            <th>{{trans('controls.students.id')}}</th>
                                            <th>{{trans('controls.students.code')}}</th>
                                            <th>{{trans('controls.students.name')}}</th>
                                            <th>{{trans('controls.students.gender')}}</th>
                                            <th>{{trans('controls.students.dob')}}</th>
                                            <th>{{trans('controls.scores.total')}}</th>
                                            <th>{{trans('controls.scores.avg')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>

    </section>
      <!--Section: Sales Performance KPIs-->

{{-- @endif --}}


@endsection


@push('custom_js')

<!-- DataTables  & Plugins -->
<script src="{{asset('themes')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/jszip/jszip.min.js"></script>
<script src="{{asset('themes')}}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{asset('themes')}}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<!-- date-range-picker -->
<script src="{{asset('themes')}}/plugins/daterangepicker/daterangepicker.js"></script>

<script src="{{asset('dropzone/dist/dropzone.js')}}"></script>



<script>

    $(document).ready(function() {
           $('#DataTableList').DataTable({
            ordering:true,
            processing: false,
            // serverSide: true,
            "order": [[ 4, "asc" ]],
            language : $('html').attr('lang') != 'en' ? window.datatableI18n[$('html').attr('lang')] : null,
        });
    } );

        $(document).on('click',`[data-toggle="modal-ajax"]`,function(ev){
                    ev.preventDefault();
                    var url = $(this).attr('href');
                    var modalSize = $(this).data('modal-size');
                    var title = $(this).text();
                    // var target = $(this).data('mdb-target');
                    var target = $(this).data('target');


                    $.get(url).done((res)=>{
                        var $modal = $(target);
                        $modal.find('.modal-title').text(title);
                        $modal.find('.modal-dialog').addClass(modalSize);
                        $modal.find('.modal-body').html(res);

                      //  $.getScript(`${location.origin }/themes/js/mdb.min.js`).done();

                        $modal.modal('show');

                        $modal.find('button#btn-submit').unbind().click(function(ev){
                            ev.preventDefault();

                            var $form = $modal.find('form');
                            var formData = new FormData($form[0]);
                            $.ajax({
                                url : $form.attr('action'),
                                method : 'post',
                                data : formData,
                                processData : false,
                                contentType : false,
                                success : (res)=>{
                                    if(res.status){

                                        Swal.fire({
                                            position: 'top-end',
                                            icon: 'success',
                                            title: '{{__('controls.Your data has been saved or updated!')}}',
                                            showConfirmButton: false,
                                            timer: 1500
                                        });


                                        if($(this).data('allow-close')){
                                            $modal.find(`[data-dismiss="modal"]`).click();
                                        }
                                        if ( url.match('edit')) {
                                            $('#DataTableList').find(`tr[data-id="${res.data.id}"]`).html($(res.dom).html());
                                        }else{
                                            $('#DataTableList').find('tbody').prepend(res.dom);
                                        }


                                        $form[0].reset();
                                    }

                                },
                            })

                        });
                        $modal.find(`[data-dismiss="modal"]`).click(function(ev){
                            ev.preventDefault();
                            $modal.modal('hide');
                            $modal.find('.modal-body').html("");

                        });


                    });

                });

</script>


<script type="text/javascript">
    $(document).on('submit',`[data-toggle="formDelete"]`,function(ev){
        ev.preventDefault();
        $.ajax({
            url : $(this).attr('action'),
            method : 'post',
            data : new FormData(ev.target),
            processData : false,
            contentType : false,
            success : (res)=>{
                if(res.status){
                    Swal.fire(
                        '{{trans('controls.delete')}}!',
                        '{{trans('controls.Your file has been deleted.')}}',
                        'success'
                    );
                    $(this).parents("tr").remove();
                }else{
                    Swal.fire(
                        '{{trans('controls.delete')}}!',
                        '{{trans('controls.delete_message_can_not')}}',
                        'error'
                    )
                }
            }

        })
    });

    function deleteItem(id){
      Swal.fire({
        title: "{{trans('controls.Are you sure?')}}",
        text: "{{trans('controls.You will not be able to delete this!')}}",
        icon: 'warning',
        showCancelButton: true,

        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText :"{{trans('controls.cancel')}}",
        confirmButtonText:  "{{trans('controls.Yes, delete it!')}}"
      }).then((result) => {
        if (result.value) {
          $('#frmDeleteItem-'+id).submit();

        }
      })
    }
</script>




{{-- Filter Data --}}
<script>

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $(document).ready(function(){

            // Search by userid
            $('#but_search').click(function(){
                // alert("Hello");
                // var class_id = Number($('#class_id').val().trim());
                var class_id = Number($('#class_id').val().trim());
                var section_id = Number($('#section_id').val().trim());

                // alert(class_id + ' ' + section_id);

                if(class_id > 0){

                    // AJAX POST request
                    $.ajax({
                    url: '../../filterScoresClassID',
                    type: 'get',
                    data: {_token: CSRF_TOKEN, class_id: class_id,section_id:section_id},
                    dataType: 'json',
                    success: function(response){
                        // alert(response);
                        createRows(response);

                    }
                    });
                }

            });

            });

    // Create table rows
    function createRows(response){
       var len = 0;
       $('#empTable tbody').empty(); // Empty <tbody>
       if(response['data'] != null){
          len = response['data'].length;
       }

       console.log(response);

       if(len > 0){
        // var tr_str = "<tr>" +
        //     "<td align='center' colspan='4'>No record found.</td>" +
        //   "</tr>";
         for(var i=0; i<len; i++){
            var id = response['data'][i].id;
            var school_id = response['data'][i].school_id;
            var school_name = response['data'][i].school_name;
            var skill_id = response['data'][i].skill_id;

            var exam_id = response['data'][i].exam_id;
            var exam_name = response['data'][i].exam_name;

            var section_id = response['data'][i].section_id;
            var section_name = response['data'][i].section_name;

            var level_id = response['data'][i].level_id;
            var level_name = response['data'][i].level_name;

            var class_id = response['data'][i].class_id;
            var class_name = response['data'][i].class_name;

            var study_year = response['data'][i].study_year;
            var month = response['data'][i].month;
            var year = response['data'][i].year;
            var student_id = response['data'][i].student_id;
            var student_name = response['data'][i].student_name;
            var code = response['data'][i].code;
            var gender = response['data'][i].gender;
            var dob = response['data'][i].dob;

            var subject_id = response['data'][i].subject_id;
            var subject_name = response['data'][i].subject_name;

            var score = response['data'][i].score;
            var total_score = response['data'][i].total_score;
            var credit = response['data'][i].credit;
            var average = response['data'][i].average;
            var color = response['data'][i].color;
            var final = response['data'][i].final;



            var tr_str = "<tr>" +
                // "<th align='center' colspan='4'>No record found.</th>" +

              "<td align='center'>" + (i+1) + "</td>" +
            //   "<td align='left'>" + getName('students','code',student_id) + "</td>" +
              "<td align='left'>" + code + "</td>" +
              "<td align='left'>" + student_name + "</td>" +
              "<td align='left'>" + gender + "</td>" +
              "<td align='left'>" + dob + "</td>" +
            //   "<td align='left'>" + getName('students','gender',student_id) + "</td>" +

              "<td align='left'>" + total_score + "</td>" +
            //   "<td align='left'>" + total_score + "</td>" +
              "<td align='left'>" + average + "</td>" +
              "<td align='left'>" + credit + "</td>" +
            "</tr>";

            $("#empTable tbody").append(tr_str);
         }
       }else{
          var tr_str = "<tr>" +
            "<td align='center' colspan='4'>No record found.</td>" +
          "</tr>";

          $("#empTable tbody").append(tr_str);
       }


    }
    </script>

@endpush
