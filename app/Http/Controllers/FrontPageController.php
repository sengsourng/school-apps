<?php
// namespace App\Helpers;

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Models\Score;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\Attendance;
use App\Models\AcademicYear;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrontPageController extends Controller
{
   public function index()
   {
       $data['AcademicYear']=AcademicYear::where('is_active','=',1)->get();
        //dd($data['AcademicYear']);
       return view('welcome',$data);
   }


   public function contact()
   {
       return view('frontend.contact');
   }

   public function about()
   {
       return view('frontend.about');
   }

   public function student_absent()
   {

    $date = Carbon::now();// will get you the current date, time

        $data['student_absent']=Attendance::where('attendance','=',2)
                                    ->where('date','=',$date->format("Y-m-d"))
                                    ->get();


        $data['class_absent']=Attendance::groupBy('class_id')
                                    // ->where('attendance','=',2)
                                    // ->where('date','=','2021-07-15')
                                    // ->groupBy('class_id')
                                    ->get();

                                    $data['class_absent'] = Attendance::select('attendances.*','student_id',DB::raw('COUNT(student_id) as count'))
                                // ->join("classes cl","cl.id","=","attendances.class_id")
                                // ->with('attendance')
                                ->where('date','=',$date->format("Y-m-d"))
                                ->where('attendance','=',2)
                                ->groupBy("attendances.class_id")
                                ->get();

        $data['class_absent_female'] = Attendance::select('attendances.*','student_id',DB::raw('COUNT(gender) as count'))
                                ->join("students","students.id","=","attendances.student_id")
                                // ->with('attendance')
                                ->where('date','=',$date->format("Y-m-d"))
                                ->where('attendance','=',2)
                                ->where('gender','=','ស')
                                ->groupBy("students.gender")
                                ->get();

        $data['all_students']=Student::where('is_active','=',1)
                                // ->where('date','=',$date->format("Y-m-d"))
                                ->get();
        $data['all_students_female']=Student::where('is_active','=',1)
                                ->where('gender','=','ស')
                                // ->where('date','=',$date->format("Y-m-d"))
                                ->get();

        // New Students
        $data['all_students_new']=Student::where('is_active','=',1)
                                ->where('register_year','=',$date->format("Y"))
                                ->get();
        $data['all_students_female_new']=Student::where('is_active','=',1)
                                ->where('gender','=','ស')
                                ->where('register_year','=',$date->format("Y"))
                                ->get();

    // dd($data);

       return view('frontend.student-absent',$data);
   }
   public function student_top5($class_id=null)
   {
       $class_id=$class_id===null?1:$class_id;
        $date = Carbon::now();// will get you the current date, time
       if($class_id=='all'){
            $data['student_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
            ->join("students","students.id","=","scores.student_id")
            // ->with('attendance')
            ->where('month','=',$date->format("m"))
            ->where('year','=',$date->format("Y"))
        //    ->where('attendance','=',2)
        //    ->where('gender','=','ស')
            ->groupBy("students.id")
            ->orderBy("score_avg","DESC")

            ->get();

            $data['class_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
                                ->join("students","students.id","=","scores.student_id")
                                // ->with('attendance')
                                ->where('month','=',$date->format("m"))
                                ->where('year','=',$date->format("Y"))
                                //    ->where('attendance','=',2)
                                //    ->where('gender','=','ស')
                                ->groupBy("scores.class_id")
                                ->get();

       }else{
            $data['student_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
            ->join("students","students.id","=","scores.student_id")
            // ->with('attendance')
            ->where('month','=',$date->format("m"))
            ->where('year','=',$date->format("Y"))
            ->where('class_id','=',$class_id)
        //    ->where('attendance','=',2)
        //    ->where('gender','=','ស')
            ->groupBy("students.id")
            // ->orderBy("total_score")
            ->orderBy("score_avg","DESC")

            ->get();

        $data['class_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
                                ->join("students","students.id","=","scores.student_id")
                                // ->with('attendance')
                                ->where('month','=',$date->format("m"))
                                ->where('year','=',$date->format("Y"))
                                //    ->where('attendance','=',2)
                                //    ->where('gender','=','ស')
                                ->groupBy("scores.class_id")
                                ->orderBy("score_avg","DESC")
                                ->get();

       }

       $data['score_subject'] = Score::select('scores.*','student_id')
                                ->join("students","students.id","=","scores.student_id")
                                // ->with('attendance')
                                // ->where('student_id','=',26)
                                ->where('month','=',$date->format("m"))
                                ->where('year','=',$date->format("Y"))
                                //    ->where('attendance','=',2)
                                //    ->where('gender','=','ស')
                                // ->groupBy("scores.class_id")
                                ->get();

            // dd($data);
            // foreach($class_score as $key => $score){
            //     $score[$key]= $score->total_score;
            // }
            // dd($score);
            // dd($data);
            $data['class_id']=$class_id;

       return view('frontend.student-top5',$data);
   }

   public function student_top5_print($class_id=null)
   {
        $date = Carbon::now();// will get you the current date, time

        if($class_id=='all'){
                $data['student_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
                ->join("students","students.id","=","scores.student_id")
                // ->with('attendance')
                ->where('month','=',$date->format("m"))
                ->where('year','=',$date->format("Y"))
            //    ->where('attendance','=',2)
            //    ->where('gender','=','ស')
                ->groupBy("students.id")
                ->orderBy("score_avg","DESC")

                ->get();

                $data['class_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
                                    ->join("students","students.id","=","scores.student_id")
                                    // ->with('attendance')
                                    ->where('month','=',$date->format("m"))
                                    ->where('year','=',$date->format("Y"))
                                    //    ->where('attendance','=',2)
                                    //    ->where('gender','=','ស')
                                    ->groupBy("scores.class_id")
                                    ->get();

        }else{
                $data['student_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
                ->join("students","students.id","=","scores.student_id")
                // ->with('attendance')
                ->where('month','=',$date->format("m"))
                ->where('year','=',$date->format("Y"))
                ->where('class_id','=',$class_id)
            //    ->where('attendance','=',2)
            //    ->where('gender','=','ស')
                ->groupBy("students.id")
                // ->orderBy("total_score")
                ->orderBy("score_avg","DESC")


                ->get();

            $data['class_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
                                    ->join("students","students.id","=","scores.student_id")
                                    // ->with('attendance')
                                    ->where('month','=',$date->format("m"))
                                    ->where('year','=',$date->format("Y"))
                                    //    ->where('attendance','=',2)
                                    //    ->where('gender','=','ស')
                                    ->groupBy("scores.class_id")
                                    ->orderBy("score_avg","DESC")
                                    ->get();

        }

        $data['score_subject'] = Score::select('scores.*','student_id')
                                    ->join("students","students.id","=","scores.student_id")
                                    // ->with('attendance')
                                    ->where('student_id','=',26)
                                    ->where('month','=',$date->format("m"))
                                    ->where('year','=',$date->format("Y"))
                                    //    ->where('attendance','=',2)
                                    //    ->where('gender','=','ស')
                                    // ->groupBy("scores.class_id")
                                    ->get();
            // dd($data);
            // foreach($class_score as $key => $score){
            //     $score[$key]= $score->total_score;
            // }
            // dd($score);
            // dd($data);
            $data['class_id']=$class_id;


            return view('frontend.student-top5_print',$data);
   }

   public function teachers()
   {
       $data['teachers']=Teacher::get();
       $data['teachers_female']=Teacher::where('gender','=','f')->get();

       return view('frontend.teachers',$data);
   }

   public function result_exam($class_id=null)
   {
        $date = Carbon::now();// will get you the current date, time

        if($class_id=='all'){
            $data['student_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
            ->join("students","students.id","=","scores.student_id")
            // ->with('attendance')
            ->where('month','=',$date->format("m"))
            ->where('year','=',$date->format("Y"))
        //    ->where('attendance','=',2)
        //    ->where('gender','=','ស')
            ->groupBy("students.id")
            ->orderBy("score_avg","DESC")

            ->get();

            $data['class_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
                                ->join("students","students.id","=","scores.student_id")
                                // ->with('attendance')
                                ->where('month','=',$date->format("m"))
                                ->where('year','=',$date->format("Y"))
                                //    ->where('attendance','=',2)
                                //    ->where('gender','=','ស')
                                ->groupBy("scores.class_id")
                                ->get();

        }else{
            $data['student_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
            ->join("students","students.id","=","scores.student_id")
            // ->with('attendance')
            ->where('month','=',$date->format("m"))
            ->where('year','=',$date->format("Y"))
            ->where('class_id','=',$class_id)
        //    ->where('attendance','=',2)
        //    ->where('gender','=','ស')
            ->groupBy("students.id")
            // ->orderBy("total_score")
            ->orderBy("score_avg","DESC")
            ->limit(5)
            ->get();

        $data['class_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
                                ->join("students","students.id","=","scores.student_id")
                                // ->with('attendance')
                                ->where('month','=',$date->format("m"))
                                ->where('year','=',$date->format("Y"))
                                //    ->where('attendance','=',2)
                                //    ->where('gender','=','ស')
                                ->groupBy("scores.class_id")
                                ->orderBy("score_avg","DESC")
                                ->get();

        }



    // $data['score_subject'] = Score::select('scores.*','student_id')
    //                          ->join("students","students.id","=","scores.student_id")
    //                          // ->with('attendance')
    //                          ->where('student_id','=',26)
    //                          ->where('month','=',$date->format("m"))
    //                          ->where('year','=',$date->format("Y"))
    //                          //    ->where('attendance','=',2)
    //                          //    ->where('gender','=','ស')
    //                          // ->groupBy("scores.class_id")
    //                          ->get();



         // dd($data);
         // foreach($class_score as $key => $score){
         //     $score[$key]= $score->total_score;
         // }
         // dd($score);
         // dd($data);
         $data['class_id']=$class_id;

       return view('frontend.result-exam',$data);
   }

   public function student_register()
   {
       return view('frontend.student-register');
   }

}
