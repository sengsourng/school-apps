<?php

namespace App\Http\Controllers\Admin;

//use Carbon\Carbon;
use Carbon\Carbon;
use App\Models\Level;
use App\Models\Score;
use App\Models\Classes;
use App\Models\Section;
use App\Models\Student;
use App\Models\Attendance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ExamController extends Controller
{
    public function index(Request $request)
    {

        // dd($request->all());
        $attendance = [];
        $level_id = $request->level_id;
		$class_id = $request->class_id;
		$section_id = $request->section_id;
		$date = $request->date;
        // dd($request->all());
        $date = Carbon::now();// will get you the current date, time

        if($request->date==null){
            $date =date('Y-m-d',strtotime(str_replace('-','/', $date->now())));// '01/07/2021';
        }else{

            $date =date('Y-m-d',strtotime(str_replace('-','/', $request->date)));// '01/07/2021';
        }
        // $month=07;
        $month =date('m',strtotime(str_replace('-','/', $request->date)));// '01/07/2021';
        $year =date('Y',strtotime(str_replace('-','/', $request->date)));// '01/07/2021';


		if($class_id == "" || $section_id == "" || $date == ""||$level_id==""){
            // dd($attendance);
			return view('backend.exams.home',compact('attendance','date','class_id','section_id','level_id'));
		}else{
			$class = Classes::find($class_id)->name;
			$section = Section::find($section_id)->name;
			$sectionSubject = Section::find($section_id)->subject_id;

            $section_id = $request->section_id;

            // dd($sectionSubject);
            $level=Level::find($level_id)->name;



            $checkStudentScore=Score::where('date','=',$date)->get();
            // dd($checkStudentScore->count());
            if ($checkStudentScore->count()>0) {
                    $attendance = Student::select('*','scores.id AS score_id')
                    ->leftJoin('scores',function($join) use ($date) {
                        $join->on('scores.student_id','=','students.id');
                        $join->where('scores.date','=',$date);
                        // $join->where('scores.month','=',07);
                        // $join->where('scores.year','=',$year);
                    })
                    ->join('student_infos','student_infos.student_id','=','students.id')
                    // ->where('student_infos.session_id',get_option('academic_year'))
                    ->where('student_infos.class_id',$class_id)
                    // ->where('scores.section_id',$section_id)
                    ->orderBy('student_infos.student_id', 'ASC')
                    ->groupBy('scores.student_id')
                    ->get();
            } else {
                $attendance = Student::select('*','scores.id AS score_id')
									->leftJoin('scores',function($join) use ($date) {
										$join->on('scores.student_id','=','students.id');
										$join->where('scores.date','=',$date);
										// $join->where('scores.month','=',07);
										// $join->where('scores.year','=',$year);
									})
									->join('student_infos','student_infos.student_id','=','students.id')
									// ->where('student_infos.session_id',get_option('academic_year'))
									->where('student_infos.class_id',$class_id)
                                    // ->where('scores.section_id',$section_id)
									->orderBy('student_infos.student_id', 'ASC')
                                    // ->groupBy('scores.student_id')
									->get();
            }









                                // dd($attendance);
			// return view('backend.attendance.student-attendance',compact('attendance','date','class','section','class_id','section_id'));
			return view('backend.exams.home',compact('attendance','date','class','section','level',
                                                            'class_id','section_id','level_id','sectionSubject'));
		}

        // $data['attendances']=Attendance::get();

        // return view('backend.attendances.home',$data);
    }


    public function student_score_save(Request $request)
    {
        // dd($request->all());
        // $result = array_merge($request->student_id, $request);
        // dd($request->score);
        // dd($request->date);
        // dd($request->class_id[0]);
        // dd($request->level_id);
        // dd($request->section_id);
        $validator = Validator::make($request->all(), [
            'score' => 'required',
        ]);
        if ($validator->fails()) {

			if($request->ajax()){
			    return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
			}else{
				return redirect('/admin/exams')
                            ->withErrors($validator)
                            ->withInput();
			}
        }


            $month=$request->date;
            $year =date('Y',strtotime(str_replace('-','/', $request->date)));// '01/07/2021';
            $month =date('m',strtotime(str_replace('-','/', $request->date)));// '01/07/2021';
            $date=$request->date;
            // $day =date('d',strtotime(str_replace('-','/', $request->date)));// '01/07/2021';
            // // dd($day);
            // // dd($month);
            // // dd($year);

            $checkScore = Score::select('scores.*','student_id')
            ->join("students","students.id","=","scores.student_id")
            ->join("subjects","scores.subject_id","=","subjects.id")
            // ->with('attendance')
            // ->where('student_id','=',$item->student_id)
            ->where('scores.month','=',$month)
            // ->where('scores.month','=',date('m', strtotime($data->date)))
            ->where('scores.year','=',$year)
            // ->where('scores.date','=',date('Y-m-d', strtotime($data->date)))
            ->where('scores.class_id','=',$request->class_id[0])
            ->where('scores.section_id','=',$request->section_id[0])
            //    ->where('attendance','=',2)
            // ->where('student_id','=',$student_id)
            ->groupBy("scores.subject_id")
            ->orderBy("subjects.short","ASC")
            ->get();

// dd($checkScore->count());
        if($checkScore->count()>0){
            $deleteScore=DB::table('scores')->where('month','=', $month)
                                ->where('year','=',$year)
                                ->where('class_id','=',$request->class_id[0])
                                ->where('section_id','=',$request->section_id[0])
                                ->delete();
        }



            $score_rank=array();

            foreach($request->score as $stu => $score){
                foreach($score as $subj => $mark){
                    $score_rank[]=$mark !=null?$mark:0;
                    // $score_avg[]=$mark+$mark
                }
            }

            foreach($request->score as $stu => $score){
                $s_avg=array();

                foreach($score as $score_avg){
                    // $s_avg=$s_avg + $mark !=null?$mark:0;
                    $s_avg[] = $score_avg !=null?$score_avg:0;
                }

                // dd(array_sum($s_avg));

                foreach($score as $subj => $mark){
                    // Save to tables
                    // echo "Student " . $stu ."-".$subj."-". $mark."<br>";
                    if($mark>=9){
                        $credit=1;
                    }else if($mark>=8){
                        $credit=2;
                    }else if($mark>=7){
                        $credit=3;
                    }else if($mark>=6){
                        $credit=4;
                    }else if($mark>=5){
                        $credit=5;
                    }else{
                        $credit=6;
                    }



                    if($checkScore->count()>0){

                        if($deleteScore){
                            Score::create([
                                'school_id' =>1,
                                'skill_id'      => 1,
                                'exam_id'   =>  1,
                                'section_id'   =>  $request->section_id[0],
                                'level_id'  => $request->level_id,
                                'level_name'    =>  getName2('levels','name',$request->level_id),
                                'class_id'  =>  $request->class_id[0],
                                'class_name'    =>  getName2('classes','name',$request->class_id[0]),
                                'student_id'    =>  $stu,
                                'student_name'  =>  getName2('students','name_kh',$stu),
                                'study_year'    =>  $year,
                                'year'          =>  $year,
                                'month'         =>  $month,
                                'date'          =>  $date,
                                'subject_id'    =>  $subj,
                                // getSubjectNameID($sID)
                                'subject_name'  =>  getSubjectNameID($subj),
                                'score'         =>  $mark !=null?$mark:0,
                                'total_score'       =>  array_sum($s_avg),//ceil( array_sum($s_avg) / count($s_avg) ),//array_sum($s_avg),
                                'average'       =>  ( array_sum($s_avg) / count($s_avg) ),//array_sum($s_avg),
                                // 'color'        =>'',
                                // 'rank',
                                'credit'    =>$credit,
                                // 'final'
                            ]);
                        }

                    }else{
                        Score::create([
                            'school_id' =>1,
                            'skill_id'      => 1,
                            'exam_id'   =>  1,
                            'section_id'   =>  $request->section_id[0],
                            'level_id'  => $request->level_id,
                            'level_name'    =>  getName2('levels','name',$request->level_id),
                            'class_id'  =>  $request->class_id[0],
                            'class_name'    =>  getName2('classes','name',$request->class_id[0]),
                            'student_id'    =>  $stu,
                            'student_name'  =>  getName2('students','name_kh',$stu),
                            'study_year'    =>  $year,
                            'year'          =>  $year,
                            'month'         =>  $month,
                            'date'          =>  $date,
                            'subject_id'    =>  $subj,
                            // getSubjectNameID($sID)
                            'subject_name'  =>  getSubjectNameID($subj),
                            'score'         =>  $mark !=null?$mark:0,
                            'total_score'       =>  array_sum($s_avg),//ceil( array_sum($s_avg) / count($s_avg) ),//array_sum($s_avg),
                            'average'       =>  ( array_sum($s_avg) / count($s_avg) ),//array_sum($s_avg),
                            // 'color'        =>'',
                            // 'rank',
                            'credit'    =>$credit,
                            // 'final'
                        ]);
                    }







                }
            }



        // if($SaveScore){
		//    return redirect('/admin/exams')->with('success',('Saved Sucessfully'));
        // }


        if(! $request->ajax()){
            return redirect('/admin/exams')->with('success',('Saved Sucessfully'));
         }else{
            return response()->json(['result'=>'success','action'=>'store','message'=>('Saved Sucessfully')]);
         }




    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            // dd($request->all());
            // $result = array_merge($request->student_id, $request);
            // dd($request->score);
            // dd($request->date);
            // dd($request->class_id[0]);
            // dd($request->level_id);
            // dd($request->section_id);
            $month=$request->date;
            $year =date('Y',strtotime(str_replace('-','/', $request->date)));// '01/07/2021';
            $month =date('m',strtotime(str_replace('-','/', $request->date)));// '01/07/2021';
            $date=$request->date;
            // $day =date('d',strtotime(str_replace('-','/', $request->date)));// '01/07/2021';
            // dd($day);
            // dd($month);
            // dd($year);
            $score_rank=array();

            foreach($request->score as $stu => $score){
                foreach($score as $subj => $mark){
                    $score_rank[]=$mark !=null?$mark:0;
                    // $score_avg[]=$mark+$mark
                }
            }

            foreach($request->score as $stu => $score){
                $s_avg=array();

                foreach($score as $score_avg){
                    // $s_avg=$s_avg + $mark !=null?$mark:0;
                    $s_avg[] = $score_avg !=null?$score_avg:0;
                }

                // dd(array_sum($s_avg));

                foreach($score as $subj => $mark){
                    // Save to tables
                    // echo "Student " . $stu ."-".$subj."-". $mark."<br>";
                    if($mark>=9){
                        $credit=1;
                    }else if($mark>=8){
                        $credit=2;
                    }else if($mark>=7){
                        $credit=3;
                    }else if($mark>=6){
                        $credit=4;
                    }else if($mark>=5){
                        $credit=5;
                    }else{
                        $credit=6;
                    }

                    $SaveScore=Score::create([
                        'school_id' =>1,
                        'skill_id'      => 1,
                        'exam_id'   =>  1,
                        'section_id'   =>  $request->section_id[0],
                        'level_id'  => $request->level_id,
                        'level_name'    =>  getName2('levels','name',$request->level_id),
                        'class_id'  =>  $request->class_id[0],
                        'class_name'    =>  getName2('classes','name',$request->class_id[0]),
                        'student_id'    =>  $stu,
                        'student_name'  =>  getName2('students','name_kh',$stu),
                        'study_year'    =>  $year,
                        'year'          =>  $year,
                        'month'         =>  $month,
                        'date'          =>  $date,
                        'subject_id'    =>  $subj,
                        // getSubjectNameID($sID)
                        'subject_name'  =>  getSubjectNameID($subj),
                        'score'         =>  $mark !=null?$mark:0,
                        'total_score'       =>  array_sum($s_avg),//ceil( array_sum($s_avg) / count($s_avg) ),//array_sum($s_avg),
                        'average'       =>  ( array_sum($s_avg) / count($s_avg) ),//array_sum($s_avg),
                        'color',
                        // 'rank',
                        'credit'    =>$credit,
                        'final'
                    ]);
                }
            }

        // if($SaveScore){
        //    return redirect('/admin/exams')->with('success',('Saved Sucessfully'));
        // }


        if(! $request->ajax()){
            return redirect('/admin/exams')->with('success',('Saved Sucessfully'));
        }else{
            return response()->json(['result'=>'success','action'=>'store','message'=>('Saved Sucessfully')]);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
