<?php

namespace Database\Seeders;

use App\Models\Subject;
use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S001",
            'name'          =>  "អំនាន",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  1,
        ]);

        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S002",
            'name'          =>  "សរ.អាន",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  2,
        ]);

        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S003",
            'name'          =>  "តែងសេចក្តី",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  3,
        ]);

        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S004",
            'name'          =>  "មេសូត្រ",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  4,
        ]);

        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S005",
            'name'          =>  "និទានរឿង",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  5,
        ]);

        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S006",
            'name'          =>  "អក្សរផ្ចង់",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  6,
        ]);

        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S007",
            'name'          =>  "គណិតវិទ្យា",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  7,
        ]);

        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S008",
            'name'          =>  "វិទ្យាសាស្រ្ត",
            'description'   =>   "",
            'status'          =>  1,
            'short'          => 8,
        ]);

        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S009",
            'name'          =>  "គំនូរ",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  9,
        ]);

        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S010",
            'name'          =>  "អប់រំកាយ",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  10,
        ]);

        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S011",
            'name'          =>  "សីលធម៌-ពលរដ្ឋ",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  11,
        ]);

        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S012",
            'name'          =>  "ប្រវត្តិវិទ្យា",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  12,
        ]);

        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S013",
            'name'          =>  "ភូមិវិទ្យា",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  13,
        ]);


        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S014",
            'name'          =>  "រូបវិទ្យា",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  14,
        ]);


        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S015",
            'name'          =>  "គីមីវិទ្យា",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  15,
        ]);


        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S016",
            'name'          =>  "ជីវវិទ្យា",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  16,
        ]);


        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S017",
            'name'          =>  "ផែនដីវិទ្យា",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  17,
        ]);


        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S018",
            'name'          =>  "អង់គ្លេស",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  18,
        ]);

        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S019",
            'name'          =>  "អក្សរសាស្រ្តខ្មែរ",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  19,
        ]);


        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S020",
            'name'          =>  "វេយ្យាករណ៍",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  20,
        ]);

        Subject::create([
            'school_id'     => 1,
            'code'          =>  "S021",
            'name'          =>  "សិក្សាសង្គម",
            'description'   =>   "",
            'status'          =>  1,
            'short'          =>  21,
        ]);
    }
}
