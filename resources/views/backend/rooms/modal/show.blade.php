<div class="container pt-1">
    {{-- <form action="{{ route('schools.update',$school) }}" method="post"> --}}
    <form>
        <div class="row">
            <div class="col-md-12  pt-3">

                    <div class="form-group row">
                        <label for="lbl_code" class="col-sm-3 col-form-label">{{trans('controls.rooms.code')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="code" value="{{$room->code}}"  class="form-control" id="lbl_code" placeholder="S001">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lable_s_name" class="col-sm-3 col-form-label">{{trans('controls.rooms.name')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="name" value="{{$room->name}}"  class="form-control" id="lable_s_name" placeholder="ភាសាខ្មែរ">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lbl_description" class="col-sm-3 col-form-label">{{trans('controls.rooms.description')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="description" value="{{$room->description}}"  class="form-control" id="lbl_description" placeholder="Khmer Lelteral">
                        </div>
                    </div>



                    <div class="form-group row">
                        <label for="lbl_status" class="col-sm-3 col-form-label">{{trans('controls.rooms.status')}}</label>
                        <div class="col-sm-9">
                            <select name="status" id="lbl_status" class="form-control">
                                <option value="">{{trans('controls.forms.Please Select')}}</option>
                                <option value="active" {{$room->status=='active'?'selected':''}} >{{trans('controls.rooms.active')}}</option>
                                <option value="disable" {{$room->status=='disable'?'selected':''}}>{{trans('controls.rooms.disable')}}</option>
                            </select>
                        </div>
                    </div>

            </div>



        </div>
    </form>
</div>
