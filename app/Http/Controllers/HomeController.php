<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Student;
use App\Models\Attendance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $date = Carbon::now();// will get you the current date, time

        $data['student_absent']=Attendance::where('attendance','=',2)
                                    ->where('date','=',$date->format("Y-m-d"))
                                    ->get();


        $data['class_absent_female'] = Attendance::select('attendances.*','student_id',DB::raw('COUNT(gender) as count'))
                                ->join("students","students.id","=","attendances.student_id")
                                // ->with('attendance')
                                ->where('date','=',$date->format("Y-m-d"))
                                ->where('attendance','=',2)
                                ->where('gender','=','ស')
                                ->groupBy("students.gender")
                                ->get();

        $data['all_students']=Student::where('is_active','=',1)
        // ->where('date','=',$date->format("Y-m-d"))
        ->get();
        $data['all_students_female']=Student::where('is_active','=',1)
                ->where('gender','=','ស')
                // ->where('date','=',$date->format("Y-m-d"))
                ->get();

        // New Students
        $data['all_students_new']=Student::where('is_active','=',1)
                ->where('register_year','=',$date->format("Y"))
                ->get();
        $data['all_students_female_new']=Student::where('is_active','=',1)
                ->where('gender','=','ស')
                ->where('register_year','=',$date->format("Y"))
                ->get();

// User
        $data['user_count']=User::all();
        $data['user_count_active']=User::where('confirm','=',1)
                                  ->where('status','=',1)->get();

        return view('home',$data);
    }
}
