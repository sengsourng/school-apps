<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Score extends Model
{
    use HasFactory;
protected $fillable=[
    'school_id','school_name',
    'skill_id','skill_name',
    'exam_id','exam_name',
    'section_id','section_name',
    'level_id','level_name',
    'class_id','class_name',
    'student_id','student_name',
    'study_year',
    'year','month',
    'date',
    'subject_id','subject_name',
    'score','average','color','credit','final','total_score'
];


public function getPointRankAttribute() {
    return $this->hasMany('App\PointLog')->select(DB::raw('
            SELECT s.*, @rank := @rank + 1 rank FROM (
              SELECT user_id, sum(points) TotalPoints FROM t
              GROUP BY user_id
            ) s, (SELECT @rank := 0) init
            ORDER BY TotalPoints DESC
          ')
      );
  }


}
