<?php

use App\Models\Attendance;
use App\Models\User;
use App\Models\School;
use Illuminate\Support\Facades\DB;


if(! function_exists('pageActive')){
    function pageActive($name,$segment=2,$class='active'){
        return request()->segment($segment)==$name?$class:'';
    }
}

if(! function_exists('pageActiveOpen')){
    function pageActiveOpen($name,$segment=2,$class='menu-open'){
        return request()->segment($segment)==$name?$class:'';
    }
}



if (!function_exists('human_file_size')) {
    function human_file_size($bytes, $decimals = 2)    {
        $sz = 'BKMGTPE';
        $factor = (int)floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . $sz[$factor];
    }
}


if (!function_exists('getSchool')) {
    function getSchool($school_id)
    {
        // $SchoolName=DB::select('select name from schools where id='.$school_id);
        $SchoolName=School::where("id",$school_id)
        ->first()->name;
        // dd($SchoolName);
        if($SchoolName !=null){
            return $SchoolName;
        }else{
            return "គ្រប់គ្រងសាលា";
        }
        // dd($SchoolName);
    }
}



if ( ! function_exists('user_count')){
	function user_count($user_type) 	{
		$count =User::where("usertype",$user_type)
						->selectRaw("COUNT(id) as total")
						->first()->total;
	    return $count;
	}
}

if ( ! function_exists('student_attend_count')){
	function student_attend_count($attendance,$year=null,$month=null,$student_id) 	{
		$count =Attendance::where("attendance",$attendance)
						->selectRaw("COUNT(attendance) as total")
                        ->where('year',$year)
                        ->where('month',$month)
                        ->where('student_id',$student_id)
						->first()->total;
	    return $count;
	}
}



if ( ! function_exists('checkCredit')){
	function checkCredit($avg) {
        if($avg>=9.00){
            echo "ល្អប្រសើ";
        }else if($avg>=8.00){
            echo "ល្អ";
        }else if($avg>=7.00){
            echo "ល្អបង្គួរ";
        }else if($avg>=6.00){
            echo "មធ្យម";
        }else if($avg>=5.00){
            echo "ខ្សោយ";
        }else{
            echo "ធ្លាក់";
        }

	}
}




if ( ! function_exists('create_option')){
	function create_option($table,$value,$display,$selected="",$where=NULL){
		$options = "";
		$condition = "";
		if($where != NULL){
			$condition .= "WHERE ";
			foreach( $where as $key => $v ){
				$condition.=$key."'".$v."' ";
			}
		}

		$query = DB::select("SELECT $value, $display FROM $table $condition");
		foreach($query as $d){
			if( $selected!="" && $selected == $d->$value ){
				$options.="<option value='".$d->$value."' selected='true'>".$d->$display."</option>";
			}else{
				$options.="<option value='".$d->$value."'>".$d->$display."</option>";
			}
		}

		echo $options;
	}
}


if ( ! function_exists('getName')){
	function getName($table,$display,$id=NULL){
		$options = "";
		if($id != NULL){
            $query = DB::select("SELECT $display FROM $table WHERE id=$id");
            foreach($query as $d){
                $options= $d->$display;
            }
		}
		echo $options;
	}
}

if ( ! function_exists('getName2')){
	function getName2($table,$display,$id=NULL){
		$options = "";
		if($id != NULL){
            $query = DB::select("SELECT $display FROM $table WHERE id=$id");
            foreach($query as $d){
                $options= $d->$display;
            }
		}
		return $options;
	}
}

if ( ! function_exists('getStudentYear')){
	function getStudentYear($table,$display,$is_active=NULL){
		$options = "";
		if($is_active != NULL){
            $query = DB::select("SELECT $display FROM $table WHERE is_active=$is_active");
            foreach($query as $d){
                $options= $d->$display;
            }
		}
		return $options;
	}
}


if ( ! function_exists('getSubjectNameID')){
	function getSubjectNameID($id=NULL){
        // function getSubjectNameID(){
		$options = "";
		if($id != NULL){
            // $query = DB::select("SELECT $display FROM $table WHERE id=$id");
            $query=DB::table('subjects')->where('id',$id)->get();
            foreach($query as $d){
                // $options= $d->$display;
                $options= $d->name;
            }
		}
		return $options;
	}
}

if (! function_exists('create_option2')) {
	function create_option2($table = '',$value = '',$show = '',$selected = '', $where = null) {
		if($where != null){
			$results = DB::table($table)->where($where)->orderBy('id','DESC')->get();
		}else{
			$results = DB::table($table)->orderBy('id','DESC')->get();
		}
		$option = '';
		foreach ($results as $data) {
			if($data->$value == $selected){
				$option .= '<option value="' . $data->$value . '" selected>' . $data->$show . '</option>';
			}else{
				$option .= '<option value="' . $data->$value . '">' . $data->$show . '</option>';
			}
		}
		echo $option;
	}
}
