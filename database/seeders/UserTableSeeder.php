<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\Permission;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::create(['name'=>'Admin','slug'=>'admin']);
        $user = User::create([
            'role_id' =>$admin->id,
            'school_id' =>1,
        	'name' => 'Roueng Run',
        	'fname' => 'Run',
            'lname' => 'Admin',
            'username' => 'admin',
        	'email' => 'admin@gmail.com',
        	'password' => bcrypt('123456'),
            'phone' => '078343143',
            'usertype' => 'admin',
            'profile' => 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50'
        ]);
        $teacher = Teacher::create([
            'user_id' =>$user->id,
            'school_id' =>1,
        	'name_en' => 'Roueng Run',
        	'name_kh' => 'រឿន រុណ',
            'gender' => 'm',
            // 'dob' => 'manager',
        	// 'pob' => 'manager@gmail.com',

            'profile' => 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50'
        ]);


        // $user->assignRole([$admin->id]);
        $manager = Role::create(['name' => 'Manager','slug'=>'manager']);

        $user = User::create([
            'role_id' =>$manager->id,
            'school_id' =>1,
        	'name' => 'roueng run',
        	'fname' => 'Run',
            'lname' => 'Manager',
            'username' => 'manager',
        	'email' => 'manager@gmail.com',
        	'password' => bcrypt('123456'),
            'phone' => '092771244',
            'usertype' => 'manager',
            'profile' => 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50'
        ]);

        $teacher = Teacher::create([
            'user_id' =>$user->id,
            'school_id' =>1,
        	'name_en' => 'KEA',
        	'name_kh' => 'មាគ គា',
            'gender' => 'm',
            // 'dob' => 'manager',
        	// 'pob' => 'manager@gmail.com',

            'profile' => 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50'
        ]);


        // $permissions = Permission::pluck('id','id')->all();
        // $manager->syncPermissions($permissions);
        // $user->assignRole([$role->id]);

        $teacher = Role::create(['name' => 'Teacher','slug'=>'teacher']);
        $user = User::create([
            'role_id' =>$teacher->id,
            'school_id' =>1,
        	'name' => 'ថុក ផល្លី',
        	'fname' => 'PHALLY',
            'lname' => 'Teacher',
            'username' => 'teacher',
        	'email' => 'teacher@gmail.com',
        	'password' => bcrypt('123456'),
            'phone' => '092771244',
            'usertype' => 'teacher',
            'profile' => 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50'
        ]);

        Teacher::create([
            'user_id' => $user->id,
            'school_id' => 1,
        	'name_en' => 'Han Sopharat',
        	'name_kh' => 'ហាន សុផារ៉ាត់',
            'gender' => 'm',
            // 'dob' => 'manager',
        	// 'pob' => 'manager@gmail.com',

            'profile' => 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50'
        ]);

        $role = Role::create(['name' => 'Student','slug'=>'student']);

        // $user = User::create([
        //     'role_id' =>$role->id,
        //     'school_id' =>1,
        // 	'name' => 'Student 1',
        // 	'fname' => 'SENG',
        //     'lname' => 'Student',
        //     'username' => 'student1',
        // 	'email' => 'student@gmail.com',
        // 	'password' => bcrypt('123456'),
        //     'phone' => '095771244',
        //     'usertype' => 'student',
        //     'profile' => 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50'
        // ]);
        // $student = Student::create([
        //     'user_id' =>$user->id,
        //     'school_id' =>1,
        // 	'code' => '10001-20001',
        // 	'name_en' => 'Student 1',
        // 	'name_kh' => 'សិស្ស ១',
        // 	'gender' => 'M',
        // 	'dob' => '1983-05-12',
        // 	'pob' => 'គោករុន មុខប៉ែន ពួក សៀមរាប',
        // 	'address' => 'គោករុន មុខប៉ែន ពួក សៀមរាប',
        // 	'father' => 'ជុក សុង',
        // 	'father_job' => 'កសិករ',
        // 	'mother' => 'អ៊ុក ង៉ាត់',
        // 	'mother_job' => 'ស្លាប់',
        // 	'city' => 'Siem Reap',
        //     'city_slug' => 'siem-reap',
        //     'register_year' => '2000',
        //     'profile' => 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50'
        // ]);


        // $permissions = Permission::pluck('id','id')->all();
        // $teacher->syncPermissions($permissions);

    }
}
