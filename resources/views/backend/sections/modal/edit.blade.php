<div class="container pt-1">
    <form action="{{ route('rooms.update',$room) }}" method="post">
        @csrf
        {{ method_field('PUT') }}

        <div class="row">
            <div class="col-md-8  pt-3">

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label" for="lbl_skill_id">{{trans('controls.skills.name')}}</label>
                   <div class="col-sm-9">
                    <select name="skill_id" id="lbl_skill_id" class="form-control select2"  required>
                        <option value="">{{ trans('controls.levels.select') }}</option>
                        {{ create_option('skills','id','name') }}
                    </select>
                   </div>
                </div>

                <div class="form-group row">
                    <label for="lbl_level_id" class="col-sm-3 col-form-label">{{trans('controls.levels.name')}}</label>
                   <div class="col-sm 9">
                    <select name="level_id" id="lbl_level_id" class="form-control select2" required>
                        <option value="">{{ trans('controls.levels.select') }}</option>
                        {{ create_option('levels','id','name') }}
                    </select>
                   </div>
                </div>


                    <div class="form-group row">
                        <label for="lbl_code" class="col-sm-3 col-form-label">{{trans('controls.sections.code')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="code"  class="form-control" id="lbl_code" placeholder="S001">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lable_s_name" class="col-sm-3 col-form-label">{{trans('controls.sections.name')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="name"  class="form-control" id="lable_s_name" placeholder="ឆមាសទី១">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lbl_description" class="col-sm-3 col-form-label">{{trans('controls.sections.description')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="description"  class="form-control" id="lbl_description" placeholder="Description">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="score_type" class="col-sm-3 col-form-label">{{trans('controls.sections.score_type')}}</label>
                        <div class="col-sm-9">
                            <select name="score_type" id="score_type" class="form-control">
                                <option value="">{{trans('controls.sections.select')}}</option>
                                <option value="average">{{trans('controls.sections.average')}}</option>
                                <option value="formula">{{trans('controls.sections.formula')}}</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="lbl_status" class="col-sm-3 col-form-label">{{trans('controls.sections.status')}}</label>
                        <div class="col-sm-9">
                            <select name="status" id="lbl_status" class="form-control">
                                <option value="">{{trans('controls.forms.Please Select')}}</option>
                                <option value="active">{{trans('controls.rooms.active')}}</option>
                                <option value="disable">{{trans('controls.rooms.disable')}}</option>
                            </select>
                        </div>
                    </div>

            </div>

            <div class="col-md-4 pt-3">
                <div class="form-group">
                    <label for="lbl_subjects">{{trans('controls.subjects.name')}}</label>
                    <select class="form-control" id="lbl_subjects" name="subject_id[]" multiple="" style="height: 350px !important;">
                        {{-- <option value="php">PHP</option>
                        <option value="react">React</option>
                        <option value="jquery">JQuery</option>
                        <option value="javascript">Javascript</option>
                        <option value="angular">Angular</option>
                        <option value="vue">Vue</option> --}}

                        {{ create_option('subjects','id','name') }}
                      </select>


                </div>


            </div>

        </div>

    </form>
</div>
