<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\School;
use App\Models\Teacher;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class TeacherController extends Controller
{
    public function index()
    {
        $data['teachers']=Teacher::get();
        return view('backend.teachers.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if( request()->ajax()){
		    return view('backend.teachers.modal.add');
		}
        return view('backend.teachers.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = [
            'status' => false,
            'data' => [],
        ];

        // dd($request->all());
         // Create User
            if ($files = $request->file('profile')) {
                    // $old_file=$student->profile;
                // for save original image

                    $ImageUpload = Image::make($files);
                    $ImageUpload_User = Image::make($files);
                    $originalPath = 'images/teachers/';
                    $originalPath_User = 'images/users/';

                    $ImageUpload->save($originalPath.time().$files->getClientOriginalName());
                    $ImageUpload_User->save($originalPath_User.time().$files->getClientOriginalName());

                    // for save thumnail image
                    $thumbnailPath = 'images/teachers/thumbnail/';
                    $ImageUpload->resize(250,250);
                    $ImageUpload = $ImageUpload->save($thumbnailPath.time().$files->getClientOriginalName());

                    $profile=time().$files->getClientOriginalName();
            }else{
                $profile='https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50';
            }
                        $full_name=explode(" ",$request['name_en']);

                        $user=User::create([
                            'role_id'   => 3,
                            'school_id' =>  auth()->user()->school_id,
                        //    'school_id'  => $request['school_id'],
                            'name' =>  $request['name_en'],
                            'fname' =>  $full_name[0],
                            'lname' =>  $full_name[1],
                            'username' =>  $request['code'],
                            'email' =>  $request['code']."@gmail.com",
                            'password' =>  Hash::make('123456'),
                            'usertype' =>  'teacher',
                            'profile' =>  $profile,

                        ]);

                        // Create Student
                        $currentDOB = $request['dob'];
                        $dob=date('Y-m-d', strtotime($currentDOB));

                        $teacher = Teacher::create([
                        // $insert_data[] = array(
                            'user_id'  => $user->id,
                            //    'school_id'  => $request['school_id'],
                            'school_id'  => auth()->user()->school_id,
                            'code'      => $request['code'],
                            'name_en'  => $request['name_en'],
                            'name_kh'   => $request['name_kh'],
                            'gender'   => $request['gender'],

                            'dob'    => $dob,
                            'pob'    => $request['pob'],

                            'address'    => $request['address'],

                            'city'   => $request['city'],
                            'city_slug'   => Str::slug($request['city']),

                            'is_active'   => 1,
                            'status'   => true,
                            'profile' =>  $profile,

                        ]);

        if ($teacher) {
            $response = [
                'status' => true,
                'data' => $teacher,
                'dom' => view('backend.teachers.rows',['row' => $teacher])->render(),
            ];
        }
        if(request()->ajax()){
            return $response;
        }else{
            // redirect(route('teachers.index'));
             return redirect()->route('teachers.index');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        // $user=User::findOrFail($teacher->user_id);
        $teacher=Teacher::findOrFail($teacher->id);
        // dd($teacher);
        if( request()->ajax()){
		    return view('backend.teachers.modal.show',compact('teacher'));
		}

        // return view('backend.teachers.show',compact('school','user','teacher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {

        // $user=User::findOrFail($teacher->user_id);
        $teacher=Teacher::findOrFail($teacher->id);
        // dd($user);
        if( request()->ajax()){
		    return view('backend.teachers.modal.edit',compact('teacher'));
		}

        return view('backend.teachers.edit',compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Teacher $teacher)
    {
        $response = [
            'status' => false,
            'data' => [],
        ];

        // $teacher_name=explode("-",$request->teacher_name);
        // $update = $school->update([
        //     'name'          => $request->school_name,
        //     'email'          => $request->school_email,
        //     'phone'    => $request->school_phone,
        //     'city'    => $request->school_city,
        //     'city_slug' => $this->makeSlug($request->school_city),
        //     'address'    => $request->school_address,
        //     'director_name'    => $teacher_name[0],


        //     // 'user_id'    => auth()->user()->id
        // ]);

        $teacher=Teacher::findOrFail($teacher->id);
        // $teacher_name=explode("-",$request->teacher_name);
        $old_file=$teacher->profile;

        if ($files = $request->file('profile')) {

                // for save original image

                $ImageUpload = Image::make($files);
                $originalPath = 'images/teachers/';

                $ImageUpload->save($originalPath.time().$files->getClientOriginalName());

                // for save thumnail image
                $thumbnailPath = 'images/teachers/thumbnail/';
                $ImageUpload->resize(250,250);
                $ImageUpload = $ImageUpload->save($thumbnailPath.time().$files->getClientOriginalName());

                $profile=time().$files->getClientOriginalName();

                if($old_file !="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50"){

                    if($old_file !=null){
                        if(file_exists(public_path('images/teachers/'.$old_file))){
                            // dd($old_file);
                            unlink(public_path('images/teachers/'.$old_file));
                            unlink(public_path('images/teachers/thumbnail/'.$old_file));
                        }
                    }
                }
        }else{
            if($old_file !=null){
                if(file_exists(public_path('images/teachers/'.$old_file))){
                    $profile=$old_file;
                }
            }else{
                $profile='https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50';

            }
        }

        // Create Student
        $currentDOB = $request['dob'];
        $dob=date('Y-m-d', strtotime($currentDOB));

        $data_teacher=[
            // 'name_kh'   =>$teacher_name[0],
            // 'name_en'   =>$teacher_name[1],
            'code'   =>$request->code,
            'name_kh'   =>$request->name_kh,
            'name_en'   =>$request->name_en,
            'gender'    => $request->gender,
            'dob'    => $dob,
            'pob'    => $request->pob,
            'address'    => $request->address,
            'city'    => $request->teacher_city,
            'city_slug' => $this->makeSlug($request->teacher_city),
            'profile'   =>$profile,


        ];
        $update=$teacher->update($data_teacher); //Update Teacher Table


        // $user=User::findOrFail(auth()->user()->id);
        // $full_name=explode(" ",$teacher_name[0]);
        // $username=explode("@",$request->teacher_email);

        // $data_user=[

        //     'name'    => $teacher_name[0],
        //     'fname'   =>$full_name[0],
        //     'lname'   =>$full_name[1],
        //     'username'    => $username[0],
        //     'email'    => $request->teacher_email,
        //     // 'username'    => $request->gender,

        // ];
        // $user->update($data_user); //Update Teacher Table

        if ($update) {
            $response = [
                'status' => true,
                'data' => $teacher,
                'dom' => view('backend.teachers.rows',['row' => $teacher])->render(),
            ];
        }
        if(request()->ajax()){
            return $response;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = [
            'status' => false,
        ];
        $teacher=Teacher::findOrFail($id);

        // if ($teacher->products->count() == 0) {
            $user=User::findOrFail($teacher->user_id);
            $teacher->delete();
            $user->delete();
            $response = [
                'status' => true,
            ];
        // }
        if (request()->ajax()) {
            return $response;
        }

        return redirect()->route('teachers.index');
    }
}
