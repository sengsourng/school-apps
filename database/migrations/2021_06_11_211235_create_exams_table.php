<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams', function (Blueprint $table) {
            $table->id();
            $table->date('date')->nullable();
            $table->unsignedBigInteger('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->unsignedBigInteger('skill_id')->references('id')->on('skills')->onDelete('cascade');
            $table->string('year')->nullable();
            $table->string('month')->nullable();

            $table->unsignedBigInteger('class_id')->references('id')->on('classes')->onDelete('cascade');
            $table->string('class_name')->nullable();

            $table->unsignedBigInteger('section_id')->references('id')->on('sections')->onDelete('cascade');
            $table->string('section_name')->nullable();

            $table->string('initial_file')->nullable();
            $table->string('final_file')->nullable();
            $table->text('note')->nullable();

            $table->unsignedBigInteger('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('updated_by')->references('id')->on('users')->onDelete('cascade');

            $table->string('status')->default('pending');
            $table->tinyInteger('final')->default('0');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exams');
    }
}
