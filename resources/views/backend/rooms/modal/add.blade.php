<div class="container pt-1">
    <form action="{{ route('rooms.store') }}" method="post" enctype="multipart/form-data">
        @csrf

        <div class="row">
            <div class="col-md-12  pt-3">

                    <div class="form-group row">
                        <label for="lbl_code" class="col-sm-3 col-form-label">{{trans('controls.rooms.code')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="code"  class="form-control" id="lbl_code" placeholder="S001">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lable_s_name" class="col-sm-3 col-form-label">{{trans('controls.rooms.name')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="name"  class="form-control" id="lable_s_name" placeholder="ឈ្មោះ ឬលេខបន្ទប់">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lbl_description" class="col-sm-3 col-form-label">{{trans('controls.rooms.description')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="description"  class="form-control" id="lbl_description" placeholder="Room name">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="lbl_status" class="col-sm-3 col-form-label">{{trans('controls.rooms.status')}}</label>
                        <div class="col-sm-9">
                            <select name="status" id="lbl_status" class="form-control">
                                <option value="">{{trans('controls.forms.Please Select')}}</option>
                                <option value="active">{{trans('controls.rooms.active')}}</option>
                                <option value="disable">{{trans('controls.rooms.disable')}}</option>
                            </select>
                        </div>
                    </div>

            </div>



        </div>


    </form>
</div>
