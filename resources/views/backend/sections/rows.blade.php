<tr data-id="{{ $row->id}}">
    <th scope="row" style="width: 40px !important;">{{$row->code}}</th>
    <td>
        {{$row->name}}
    </td>

    <td>

        <?php

            $Subjects=explode(",",$row->subject_id);
            // echo count($Subjects);
            $arrLength = count($Subjects);
            for ($i=0; $i<$arrLength; $i++) {
                $sID= $Subjects[$i];
               if($i==$arrLength-1){
                    echo $row->GetSubject($sID)." ";
               }else{
                    echo $row->GetSubject($sID) ." - ";
               }
            }
        ?>

    </td>

    <td>
        {{$row->status}}

    </td>


    <td>
        <a class="btn btn-primary  edit_button btn-sm" href="{{route('sections.show',$row)}}"
                data-toggle="modal-ajax"
                {{-- data-modal-size="modal-lg" --}}
                {{-- data-modal-size="modal-xl" --}}
                data-mdb-toggle="modal"
                data-target="#openModal"
        ><i class="fa fa-eye left"></i> {{ trans('controls.forms.Show')}}</a>

        <a class="btn btn-primary  edit_button btn-sm" href="{{route('sections.edit',$row)}}"
                data-toggle="modal-ajax"
                {{-- data-modal-size="modal-lg" --}}
                {{-- data-modal-size="modal-xl" --}}
                data-mdb-toggle="modal"
                data-target="#openModal"
        ><i class="fa fa-edit left"></i> {{ trans('controls.forms.Edit')}}</a>


        <a href="javascript:deleteItem({{ $row->id }})"​​ class="btn btn-danger delete_button btn-sm"><i class="fa fa-trash-alt left"></i> {{ trans('controls.forms.Delete')}}</a>


        <form data-toggle="formDelete"  id="frmDeleteItem-{{ $row->id }}" style="display: none" action="{{ route('sections.destroy',$row->id) }}" role="form" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        </form>

    </td>
</tr>
