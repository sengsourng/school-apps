<?php

namespace App\Imports;

use Exception;
use App\Models\User;
use App\Models\Student;
use App\Models\StudentInfo;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StudentImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        try {

                // return new Student([
                        //     //
                        // ]);
                        // dd($_REQUEST['level_id']);
                        $student_info=([
                            'level_id'  =>$_REQUEST['level_id'],
                            'class_id'  =>$_REQUEST['class_id'],
                            'study_year'  =>$_REQUEST['study_year'],
                            'date'  =>$_REQUEST['date'],
                            'skill_id'  =>$_REQUEST['skill_id'],
                        ]);

                    // dd($student_info['level_id']);
                    // dd($student_info);


                        $full_name=explode(" ",$row['name_en']);

                    $user=User::create([
                            'role_id'   => 4,
                            'school_id' =>  auth()->user()->school_id,
                        //    'school_id'  => $row['school_id'],
                            'name' =>  $row['name_en'],
                            'fname' =>  $full_name[0],
                            'lname' =>  $full_name[1],
                            'username' =>  $row['code'],
                            'email' =>  $row['code']."@gmail.com",
                            'password' =>  Hash::make('123456'),
                            'usertype' =>  'student',
                            'profile' =>  'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50',

                        ]);

                        $timestamp = strtotime($row['dob']);
                        $dob=date('d-m-Y', $timestamp );

                    $student=Student::create([
                            'user_id'  => $user->id,
                        //    'school_id'  => $row['school_id'],
                        'school_id'  => auth()->user()->school_id,
                        'code'  => $row['code'],
                        'name_en'  => $row['name_en'],
                        'name_kh'   => $row['name_kh'],
                        'gender'   => $row['gender'],

                        'dob'    => $dob,
                        //    'dob'    => \Carbon\Carbon::parse($row['dob'])->format('d/m/Y'),

                        'pob'  => $row['pob'],
                        'address'   => $row['address'],
                        'father'   => $row['father'],
                        'father_job'   => $row['father_job'],
                        'mother'   => $row['mother'],
                        'mother_job'   => $row['mother_job'],
                        'city'   => $row['city'],
                        'city_slug'   => Str::slug($row['city']),
                        'register_year'   => $row['register_year'],
                        'is_active'   => 1,
                    ]);

                    return new StudentInfo([
                        'school_id' => auth()->user()->school_id,
                        'level_id'  => $student_info['level_id'],
                        'class_id'  => $student_info['class_id'],
                        'skill_id'  => $student_info['skill_id'],
                        'date'  => $student_info['date'],
                        'student_id'  => $student->id,
                        'study_year'  => $student_info['study_year'],
                    ]);


        } catch (Exception $e) {
            // Log::debug(trans('messages.token_invalid'));

            return redirect()->url('/manager/import_view')->with('alert', 'danger|' . trans('controls.students.import'));

        }



    }
}
