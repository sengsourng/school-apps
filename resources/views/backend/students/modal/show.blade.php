<div class="container pt-1">
    {{-- <form action="{{ route('schools.update',$school) }}" method="post"> --}}
    <form>
        {{-- @csrf
        {{ method_field('PUT') }} --}}

        <div class="row">
            <div class="col-md-5 bg-info pt-3">

                <div class="form-group row">
                    <label for="lbl_code" class="col-sm-4 col-form-label">{{trans('controls.students.code')}}</label>
                    <div class="col-sm-8">
                        <input type="text" name="code"  class="form-control" id="lbl_code" value="{{$student->code}}">
                    </div>
                </div>

                <div class="form-group row">
                        <label for="lbl_name_kh" class="col-sm-4 col-form-label">{{trans('controls.students.name')}}</label>
                        <div class="col-sm-8">
                            <input type="text" name="name_kh"  class="form-control" id="lbl_name_kh" value="{{$student->name_kh}}" placeholder="នាមត្រកូល នាមខ្លួន">
                        </div>
                </div>

                <div class="form-group row">
                    <label for="lbl_name_en" class="col-sm-4 col-form-label">{{trans('controls.students.name_en')}}</label>
                    <div class="col-sm-8">
                        <input type="text" name="name_en"  class="form-control" id="lbl_name_en" value="{{$student->name_en}}" placeholder="FirstName LastName">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="lbl_gender" class="col-sm-4 col-form-label">{{trans('controls.students.gender')}}</label>
                    <div class="col-sm-8">
                        <select class="custom-select" name="gender">
                            <option>{{trans('controls.forms.Please Select')}}</option>
                            <option {{$student->gender=='ប'?'selected':''}}  value="ប">{{trans('controls.forms.Male')}}</option>
                            <option  {{$student->gender=='ស'?'selected':''}} value="ស">{{trans('controls.forms.Female')}}</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="lbl_dob" class="col-sm-4 col-form-label">{{trans('controls.students.dob')}}</label>
                    <div class="col-sm-8">
                        <input type="text" name="dob"  class="form-control" id="lbl_dob" value="{{$student->dob}}" placeholder="Date of Birth">
                    </div>
                </div>


                <div class="form-group row">
                    <label for="lbl_pob" class="col-sm-4 col-form-label">{{trans('controls.students.pob')}}</label>
                    <div class="col-sm-8">
                        <input type="text" name="pob"  class="form-control" id="lbl_pob" value="{{$student->pob}}" placeholder="ភូមិ ឃុំ​ ស្រុក ខេត្ត">
                    </div>
                </div>
                {{-- address --}}
                <div class="form-group row">
                    <label for="lbl_address" class="col-sm-4 col-form-label">{{trans('controls.students.address')}}</label>
                    <div class="col-sm-8">
                        <input type="text" name="address"  class="form-control" id="lbl_address" value="{{$student->address}}" placeholder="ផ្ទះលេខ ក្រុម ភូមិ ឃុំ​ ស្រុក ខេត្ត">
                    </div>
                </div>

            </div>


            <div class="col-md-5 bg-success pt-3">
                {{-- father --}}
                <div class="form-group row">
                    <label for="lable_father" class="col-sm-4 col-form-label">{{trans('controls.students.father')}}</label>
                    <div class="col-sm-8">
                        <input type="text" name="teacher_name" class="form-control" id="lable_father" value="{{$student->father}}" placeholder="នាមត្រកូល នាមខ្លួន">
                    </div>
                </div>

                {{-- father_job --}}
                <div class="form-group row">
                    <label for="lbl_father_job" class="col-sm-4 col-form-label">{{trans('controls.students.father_job')}}</label>
                    <div class="col-sm-8">
                        <input type="text" name="father" class="form-control" id="lbl_father_job" value="{{$student->father_job}}" placeholder="មុខរបរឪពុក">
                    </div>
                </div>

                {{-- mother --}}
                <div class="form-group row">
                    <label for="lable_mother" class="col-sm-4 col-form-label">{{trans('controls.students.mother')}}</label>
                    <div class="col-sm-8">
                        <input type="text" name="mother" class="form-control" id="lable_mother" value="{{$student->mother}}" placeholder="ឈ្មោះម្តាយ">
                    </div>
                </div>

                {{-- mother_job --}}
                <div class="form-group row">
                    <label for="lbl_mother_job" class="col-sm-4 col-form-label">{{trans('controls.students.mother_job')}}</label>
                    <div class="col-sm-8">
                        <input type="text" name="mother_job" class="form-control" id="lbl_mother_job" value="{{$student->mother_job}}" placeholder="មុខរបរម្តាយ">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="lbl_city" class="col-sm-4 col-form-label">{{trans('controls.students.city')}}</label>
                    <div class="col-sm-8">
                        <input type="text" name="city" class="form-control" id="lbl_city" value="{{$student->city}}" placeholder="ខេត្ត/ក្រុង">
                    </div>
                </div>


                <div class="form-group row">
                    <label for="lbl_is_active" class="col-sm-4 col-form-label">{{trans('controls.students.is_active')}}</label>
                    <div class="col-sm-8">
                        <select class="custom-select">
                            <option>{{trans('controls.forms.Please Select')}}</option>
                            <option {{$student->is_active==1?'selected':''}}  value="1">Active</option>
                            <option {{$student->is_active==0?'selected':''}} value="0">Disactive</option>
                        </select>
                    </div>
                </div>



            </div>

            <div class="col-md-2 pt-3">

                <div class="card" style="width: 100%;">
                    @if ($student->profile==="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50")
                        <img src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50" alt="" class="rounded mx-auto d-block mb-2" style="width: 100%;">

                    @elseif ($student->profile==NULL)
                        <img src="{{asset('images/students/default_student.jpg')}}" alt="" class="rounded mx-auto d-block mb-2" style="width: 100%;">
                    @else
                        <img src="{{asset('images/students/'.$student->profile)}}" class="uploadPreview rounded mx-auto d-block mb-2" style="width: 100%;" />
                    @endif


                  </div>


            </div>

        </div>

        {{-- <div class="row bg-secondary">
            <div class="col-sm-3 mt-2">
                <div class="form-group">
                    <label class="control-label">{{trans('controls.levels.name')}}</label>
                    <select name="level_id" class="form-control select2" onChange="getData(this.value);" required>
                        <option value="">{{ trans('controls.levels.select') }}</option>
                        {{ create_option('levels','id','name') }}
                    </select>
                </div>
            </div>

            <div class="col-sm-3 mt-2">
                <div class="form-group">
                    <label class="control-label">{{trans('controls.classes.name')}}</label>
                    <select name="class_id" id="class_id" class="form-control select2" required>
                        <option value="">{{ trans('controls.classes.select') }}</option>
                        {{ create_option('classes','id','name') }}
                    </select>
                </div>
            </div>

            <div class="col-md-3 mt-2">
                <div class="form-group">
                    <label for="date">{{trans('controls.students.date')}}</label>
                    <input type="date" id="date" class="form-control" name="date">
                </div>
            </div>
            <div class="col-md-3 mt-2">
                <div class="form-group">
                    <label for="study_year">{{trans('controls.students.register_year')}}</label>

                    <select name="register_year" id="study_year" class="form-control">
                            <option value="2020">2019-2020</option>
                            <option value="2021">2020-2021</option>

                    </select>
                </div>
            </div>
        </div> --}}


        <div class="row">
            <table class="table table-striped">
                <thead>
                  <tr>
                    <th>{{trans('controls.levels.name')}}</th>
                    <th>{{trans('controls.classes.name')}}</th>
                    <th>{{trans('controls.students.register_year')}}</th>
                  </tr>
                </thead>
                <tbody>

                    @foreach ($student_infos as $item)
                        <tr>
                            <td>{{getName('levels','name',$item->level_id)}}</td>
                            <td>{{getName('classes','name',$item->class_id)}}</td>
                            <td>{{$item->study_year}}</td>
                        </tr>
                    @endforeach

                </tbody>
              </table>
        </div>


    </form>
</div>
