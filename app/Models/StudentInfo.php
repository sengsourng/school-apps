<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentInfo extends Model
{
    use HasFactory;

    protected $fillable=([
        'school_id','level_id','class_id','skill_id','student_id','date','description','study_year'
    ]);


    public function classes()
    {
        return $this->hasOn(Classes::class,'id','class_id');
    }

    public function levels()
    {
        return $this->hasOne(Levels::class,'id','level_id');
    }
}
