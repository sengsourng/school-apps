<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Skill;
use App\Models\School;
use App\Models\Teacher;
use Illuminate\Http\Request;

class SkillController extends Controller
{
    public function index()
    {
        $data['skills']=Skill::get();
        return view('backend.skills.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if( request()->ajax()){
		    return view('backend.skills.modal.add');
		}
        return view('backend.skills.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = [
            'status' => false,
            'data' => [],
        ];
        $add = Skill::create([
            // 'business_id'  =>  1,
            'name'          => $request->name,
            'description'    => $request->description,
        ]);

        if ($add) {
            $response = [
                'status' => true,
                'data' => $add,
                'dom' => view('backend.skills.rows',['row' => $add])->render(),
            ];
        }
        if(request()->ajax()){

            return $response;
        }
        // dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Skill $skill)
    {
        // $user=User::findOrFail($skill->user_id);
        // $teacher=Teacher::findOrFail($skill->user_id);
        // dd($user);
        if( request()->ajax()){
		    return view('backend.skills.modal.show',compact('skill'));
		}

        // return view('backend.schools.show',compact('school','user','teacher'));
    }

    public function edit(Skill $skill)
    {

        if( request()->ajax()){
		    return view('backend.skills.modal.edit',compact('skill'));
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Skill $skill)
    {
        $response = [
            'status' => false,
            'data' => [],
            // 'id' => $category->id,
        ];
        $update = $skill->update([
            'name'          => $request->name,
            'description'    => $request->description,
        ]);

        if ($update) {
            $response = [
                'status' => true,
                'data' =>['id'=> $skill->id],
                // 'data' => $skilles,
                'dom' => view('backend.skills.rows',['row' => $skill])->render(),

            ];

        }
        // dd($response);

        if(request()->ajax()){
            return $response;
        }
    }

    public function destroy($id)
    {
        //
        $response = [
            'status' => false,
        ];
        $delete=Skill::findOrFail($id);

        // $userCheck=Classes::where('id','=',$id)->count();
        // dd($userCheck);

        // if ($userCheck == 0) {
        //     $delete->delete();
        //     $response = [
        //         'status' => true,
        //     ];
        // }

        $delete->delete();
        $response = [
            'status' => true,
        ];


        if (request()->ajax()) {
            return $response;
        }

        return redirect()->route('skills.index');
    }
}
