<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // public function getName($var = null)
    // {
    //     return $var;
    // }

    function makeSlug($string)
    {
        $LNSH = '/\s+/u';
        $SADH   = '/[\-\s]+/';

        $string = preg_replace($LNSH, '', mb_strtolower($string, 'UTF-8'));
        $string = preg_replace($SADH, '-', $string);
        $string = trim($string, '-');

        return $string;
    }
}
