
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@stack('title','School Management')</title>

      <script src="{{asset('js/app.js')}}"></script>
      <link rel="stylesheet" href="{{asset('css/app.css')}}">

      <link rel="preconnect" href="https://fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css2?family=Hanuman:wght@400;700&display=swap" rel="stylesheet">

        {{-- Data Table --}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('themes/sweetalert2/sweetalert2.css')}}">


      <!-- flag-icon-css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">
      <style>
        body{
            font-family: 'Hanuman' !important;
            font-size: 1em;
            overflow-y: scroll !important;

            /* visibility:hidden; */
        }
        a:visited {
            text-decoration: none !important;
        }
        h3{
            text-decoration: none !important;
        }

        input[type="file"] {
            display: none;
        }

        /* body {visibility:hidden;} */

    </style>
      @stack('custom_css')

</head>
<?php $theme_color=config('app.theme_color'); ?>
{{-- <body class="hold-transition {{$theme_color}} sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed"> --}}
    <body class="hold-transition {{$theme_color}} sidebar-mini layout-fixed layout-navbar-fixed">
<div class="wrapper">

  <!-- Preloader -->
  {{-- <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__wobble" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
  </div> --}}

  <!-- Navbar -->
  @include('layouts.includes.top_navbar')
  <!-- /.navbar -->

   <!-- Authentication Links -->
   @guest
   {{-- @if (Route::has('login'))
       <li class="nav-item">
           <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
       </li>
   @endif

   @if (Route::has('register'))
       <li class="nav-item">
           <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
       </li>
   @endif --}}


     <!-- Main Sidebar Container -->
       @include('layouts.includes.main_sidebar_front')

@else

   <!-- Main Sidebar Container  Login-->
       @include('layouts.includes.main_slidebar_login')

@endguest

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    @yield('content')

  </div>
  <!-- /.content-wrapper -->



  <div class="modal fade"
    id="openModal"
    {{-- tabindex="-1"
    aria-labelledby="openModalLabel"
    aria-hidden="true" --}}
     >
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          {{-- <h4 class="modal-title">Large Modal</h4> --}}
          <h5 class="modal-title" id="openModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         {{-- @include('brands.modal.add') --}}
        </div>
        <div class="modal-footer justify-content-right">
          <button type="button" class="btn btn-danger" data-dismiss="modal"> {{__('controls.cancel')}}</button>
          <button type="button" id="btn-submit" class="btn btn-primary" data-allow-close="true">{{__('controls.save close')}}</button>
          <button type="button" id="btn-submit" class="btn btn-success">{{__('controls.save')}}</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  {{-- Modal Print --}}

  <div class="modal fade"
  id="openModalPrint"
  {{-- tabindex="-1"
  aria-labelledby="openModalLabel"
  aria-hidden="true" --}}
   >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        {{-- <h4 class="modal-title">Large Modal</h4> --}}
        <h5 class="modal-title" id="openModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       {{-- @include('brands.modal.add') --}}
      </div>
      <div class="modal-footer justify-content-right">
        <button type="button" class="btn btn-danger" data-dismiss="modal"> {{__('controls.cancel')}}</button>
        {{-- <button type="button" id="btn-submit" class="btn btn-primary" data-allow-close="true">{{__('controls.save close')}}</button> --}}
        <button type="button" id="btn-submit" class="btn btn-success" onclick="window.print()">Print</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>



  <!-- Main Footer -->
  <footer class="main-footer noPrint">
    <strong>Copyright &copy; 2014-2021 <a href="https://learn24kh.com">សាលាបឋមសិក្សា ស្វាយធំ</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>ជំនាន់</b> 1.1.0
    </div>
  </footer>
</div>
<!-- ./wrapper -->
 <!-- Scripts -->

<script src="{{ asset('themes/js/script_translate.js')}}"></script>


<script src="{{ asset('https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap5.min.js')}}"></script>

<script type="text/javascript" src="{{asset('themes/sweetalert2/sweetalert2.min.js')}}"></script>


 @stack('custom_js')

</body>

<script>
    function Preview_Image_Before_Upload(fileinput_id, preview_id) {
            var oFReader = new FileReader();
            var fileArray = [];
            fileArray.push(document.getElementById(fileinput_id).files[0])
            fileArray.forEach(function(entry) {
                oFReader.readAsDataURL(fileArray[0]);
            });

            //console.log(fileArray)
            // oFReader.readAsDataURL(fileArray[0]);
            oFReader.onload = function(oFREvent) {
                if (window.FileReader && window.File && window.FileList && window.Blob) {

                var elem = document.getElementById("uploadPreview");
                elem.src = oFREvent.target.result;
                // document.getElementById("placehere").appendChild(elem);
                document.getElementById("uploadPreview").innerHTML=elem;
                }
            };
        };
        function removeFns(){
            //document.getElementById("uploadPreview").innerHTML=null;
            document.getElementById('uploadImage').value = ""
            document.getElementById('uploadPreview').src = "{{asset('images/students/default_student.jpg')}}";

        }

</script>




</html>
