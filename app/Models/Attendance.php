<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use HasFactory;

    protected $fillable=([
        'student_id',
        'school_id',
        'skill_id',
        'student_id',
        'class_id',
        'section_id',
        'level_id',
        'section_name',
        'year',
        'month',
        'date',
        'attendance',
        'reason',
        'academic_years_id',
        'study_year',
        // 'month',
    ]);


    public function student()
    {
        return $this->belongsTo(Student::class);
    }

}
