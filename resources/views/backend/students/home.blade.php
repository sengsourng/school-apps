@extends('layouts.admin')

@push('custom_css')

<!-- DataTables -->
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

 <!-- daterange picker -->
 <link rel="stylesheet" href="{{asset('themes')}}/plugins/daterangepicker/daterangepicker.css">
@endpush

{{-- @push('title','Manage Brands')

@section('content-header',"Manage Brands") --}}


@section('content')

     <!--Section: Sales Performance KPIs-->
     <section class="content pt-2">
        <div class="card">
          <div class="card-header text-left py-3" style="height: 9vh;">
            <h5 class="mb-0 text-left">
                <strong>{{trans('controls.students.all')}}</strong>

                <span >
                    <a href="{{route('students.create')}}"
                    class="btn btn-primary btn-sm float-right"
                    >
                    <i class="fas fa-plus-circle"></i> {{trans('controls.students.add')}}</a>
                </span>


                <span>
                    <a href="{{route('students.import_view')}}"
                    class="btn btn-success btn-sm float-right mr-2"
                    >
                    <i class="fas fa-file-excel"></i> {{trans('controls.students.import')}}</a>
                </span>
                <span class="float-right pr-2">
                    <button type="button" class="btn btn-info btn-sm" data-toggle="collapse" data-target="#demo">{{trans('controls.students.filter_student')}}</button>
                </span>
            </h5>
          </div>


          <div class="card-body">
            <div id="demo" class="collapse">
                <form id="jquery-datatable-filters" action="{{ route('students.filter')}}" method="post" autocomplete="off" accept-charset="utf-8">
                    @csrf


                    <div class="row bg-secondary mb-2">
                        <div class="col-sm-2 mt-2">
                            <div class="form-group">
                                <label class="control-label">{{trans('controls.levels.name')}}</label>
                                <select name="level_id" class="form-control select2" onChange="getData(this.value);" >
                                    <option value="">{{ trans('controls.levels.select') }}</option>
                                    {{ create_option('levels','id','name',$level_id) }}
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-2 mt-2">
                            <div class="form-group">
                                <label class="control-label">{{trans('controls.classes.name')}}</label>
                                <select name="class_id" id="class_id" class="form-control select2" >
                                    <option value="">{{ trans('controls.classes.select') }}</option>
                                    {{ create_option('classes','id','name',$class_id) }}
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3 mt-2">
                            <div class="form-group">
                                <label for="date">{{trans('controls.students.date')}}</label>
                                <input type="date" id="date" class="form-control" name="date">
                            </div>
                        </div>

                        <div class="col-md-2 mt-2">
                            <div class="form-group">
                                <label for="study_year">{{trans('controls.students.register_year')}}</label>
                                <select name="register_year" id="study_year" class="form-control">

                                    {{ create_option('academic_years','year','name',$register_year) }}

                                </select>
                            </div>
                        </div>

                        <div class="col-md-2 mt-2">
                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="fa fa-search"></i> ស្វែងរក</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>


            <div class="table-responsive">
              <table class="table table-hover text-nowrap" id="DataTableList">
                <thead>
                  <tr>
                    <th scope="col" style="width: 40px !important;">{{trans('controls.forms.ID')}}</th>
                    <th scope="col">{{__('controls.students.name')}}</th>
                    <th scope="col">{{__('controls.students.gender')}}</th>
                    <th scope="col">{{__('controls.students.dob')}}</th>
                    <th scope="col">{{__('controls.students.address')}}</th>
                    <th scope="col">{{__('controls.students.father')}}-{{__('controls.students.mother')}}</th>
                    {{-- <th scope="col">Gender</th> --}}
                    {{-- <th scope="col">{{__('controls.forms.Created By')}}</th> --}}
                    <th scope="col">{{__('controls.forms.Action')}}</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($students as $row)
                        @include('backend.students.rows')
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>


        </div>
     </section>
      <!--Section: Sales Performance KPIs-->

{{-- @endif --}}


@endsection


@push('custom_js')

<!-- DataTables  & Plugins -->
<script src="{{asset('themes')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/jszip/jszip.min.js"></script>
<script src="{{asset('themes')}}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{asset('themes')}}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<!-- date-range-picker -->
<script src="{{asset('themes')}}/plugins/daterangepicker/daterangepicker.js"></script>

<script src="{{asset('dropzone/dist/dropzone.js')}}"></script>


<script>

$('#class_id').attr('disabled', 'disabled');

function getData(val) {
    var _token=$('input[name=_token]').val();
    var level_id=$('select[name=level_id]').val();
    if(level_id !=""){
        $('#class_id').removeAttr('disabled');
    }else{
        $('#class_id').attr('disabled', 'disabled');
    }


    $.ajax({
        type: "POST",
        url: "{{url('admin/classes/get_classes')}}",
        data:{_token:_token,level_id:level_id},
        beforeSend: function(){
            $("#preloader").css("display","block");
        },success: function(classes){
            $("#preloader").css("display","none");
            $('select[name=class_id]').html(classes);
        }
    });
}


    $(document).ready(function() {
           $('#DataTableList').DataTable({
            ordering:true,
            processing: false,
            // serverSide: true,
            "order": [[ 4, "asc" ]],
            language : $('html').attr('lang') != 'en' ? window.datatableI18n[$('html').attr('lang')] : null,
        });
    } );

        $(document).on('click',`[data-toggle="modal-ajax"]`,function(ev){
                    ev.preventDefault();
                    var url = $(this).attr('href');
                    var modalSize = $(this).data('modal-size');
                    var title = $(this).text();
                    // var target = $(this).data('mdb-target');
                    var target = $(this).data('target');


                    $.get(url).done((res)=>{
                        var $modal = $(target);
                        $modal.find('.modal-title').text(title);
                        $modal.find('.modal-dialog').addClass(modalSize);
                        $modal.find('.modal-body').html(res);

                      //  $.getScript(`${location.origin }/themes/js/mdb.min.js`).done();

                        $modal.modal('show');

                        $modal.find('button#btn-submit').unbind().click(function(ev){
                            ev.preventDefault();

                            var $form = $modal.find('form');
                            var formData = new FormData($form[0]);
                            $.ajax({
                                url : $form.attr('action'),
                                method : 'post',
                                data : formData,
                                processData : false,
                                contentType : false,
                                success : (res)=>{
                                    if(res.status){

                                        Swal.fire({
                                            position: 'top-end',
                                            icon: 'success',
                                            title: '{{__('controls.Your data has been saved or updated!')}}',
                                            showConfirmButton: false,
                                            timer: 1500
                                        });


                                        if($(this).data('allow-close')){
                                            $modal.find(`[data-dismiss="modal"]`).click();
                                        }
                                        if ( url.match('edit')) {
                                            $('#DataTableList').find(`tr[data-id="${res.data.id}"]`).html($(res.dom).html());
                                        }else{
                                            $('#DataTableList').find('tbody').prepend(res.dom);
                                        }


                                        $form[0].reset();
                                    }

                                },
                            })

                        });
                        $modal.find(`[data-dismiss="modal"]`).click(function(ev){
                            ev.preventDefault();
                            $modal.modal('hide');
                            $modal.find('.modal-body').html("");

                        });


                    });

                });

</script>


<script type="text/javascript">
    $(document).on('submit',`[data-toggle="formDelete"]`,function(ev){
        ev.preventDefault();
        $.ajax({
            url : $(this).attr('action'),
            method : 'post',
            data : new FormData(ev.target),
            processData : false,
            contentType : false,
            success : (res)=>{
                if(res.status){
                    Swal.fire(
                        '{{trans('controls.delete')}}!',
                        '{{trans('controls.Your file has been deleted.')}}',
                        'success'
                    );
                    $(this).parents("tr").remove();
                }else{
                    Swal.fire(
                        '{{trans('controls.delete')}}!',
                        '{{trans('controls.delete_message_can_not')}}',
                        'error'
                    )
                }
            }

        })
    });

    function deleteItem(id){
      Swal.fire({
        title: "{{trans('controls.Are you sure?')}}",
        text: "{{trans('controls.You will not be able to delete this!')}}",
        icon: 'warning',
        showCancelButton: true,

        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText :"{{trans('controls.cancel')}}",
        confirmButtonText:  "{{trans('controls.Yes, delete it!')}}"
      }).then((result) => {
        if (result.value) {
          $('#frmDeleteItem-'+id).submit();

        }
      })
    }
</script>

@endpush
