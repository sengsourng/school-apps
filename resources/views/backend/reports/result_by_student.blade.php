@extends('layouts.admin')

@push('custom_css')

<!-- DataTables -->
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

 <!-- daterange picker -->
 <link rel="stylesheet" href="{{asset('themes')}}/plugins/daterangepicker/daterangepicker.css">







 <style>
    body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        /* font: 12pt "Tahoma"; */
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 210mm;
        min-height: 297mm;
        padding: 20mm;
        margin: 10mm auto;
        /* border: 1px #D3D3D3 solid; */
        border-radius: 5px;
        /* background: white; */
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
        /* background-image:url({{asset('images/alis_frame.png')}}) */

    }
    .subpage {
        padding: 1cm;
        /* border: 5px rgb(243, 242, 242) solid; */
        height: 257mm;
        /* outline: 1cm #ffffff solid; */
    }

    @page {
        size: A4;
        margin: 0;

    }
    @media print {
        html, body {
            padding-top: 0px;
            width: 290mm !important;
            height: 310mm !important;
            /* background: rgb(255, 255, 255); */
        /* backgournd-image:url({{asset('images/bg_card.jpg')}}); */
            /* background-image:url({{asset('images/bg_card.jpg')}}); */

        }

        .bgSizeCover {
            background-image: url('{{asset("images/bg_card.jpg")}}');
            background-size: cover;
            /* background-size: auto;
            background-repeat: no-repeat !important; */
            /* width: 160px;
            height: 160px; */
            /* width: 210mm; */
            width: 320mm !important;
            /* height: 297mm; */
            height: 448mm !important;

            margin-top: -2cm !important;
            margin-left: -30px !important;

            /* border: 2px solid; */
            /* color: pink; */
            resize: both;
            /* overflow: scroll; */
        }

        .subpage {
            /* padding-top: 4cm; */
            /* border: 5px rgb(243, 242, 242) solid; */
            /* height: 257mm; */
            /* outline: 1cm #ffffff solid; */
        }


        .noPrint{
                display: none !important;
            }

        .page {
            margin-top: 0px;
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always !important;


        }
        .page_break {
                /* page-break-after: always !important; */
                page-break-before: always !important;
                /* page-break-inside: always !important; */
        }
    }
</style>



@endpush


@section('content')

     <!--Section: Sales Performance KPIs-->
     <section class="content">

                <div class="col-md-12 pt-3">
                    <div class="card card-outline card-success">
                    <div class="card-header">
                        {{-- <form id="search_form" class="params-panel validate" action="{{ route('exams.create')}}" method="post" autocomplete="off" accept-charset="utf-8">
                            @csrf --}}
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('controls.levels.name')}}</label>
                                        <select id='search' name="level_id" class="form-control select2" onChange="getData(this.value);" required>
                                            <option value="">{{ trans('controls.levels.select') }}</option>
                                            {{ create_option('levels','id','name',1) }}
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('controls.classes.name')}}</label>
                                        <select name="class_id" id="class_id" class="form-control select2" required>
                                            <option value="">{{ trans('controls.classes.select') }}</option>
                                            {{ create_option('classes','id','name',1) }}
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('controls.sections.name')}}</label>
                                        <select name="section_id" class="form-control select2" required>
                                            <option value="">{{ trans('controls.forms.Please Select') }}</option>
                                            {{ create_option('sections','id','name',1) }}
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="date" class="control-label">{{trans('controls.students.date')}}</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            <input type="date" class="form-control datepicker" name="date" value="" required>
                                        </div>
                                    </div>


                                </div>
                                <div class="col-sm-2 pt-4">
                                    <div class="form-group pt-2">
                                        <button type="submit" class="btn btn-primary btn-block rect-btn" id='but_search'><i class="fas fa-filter"></i> {{trans('controls.forms.filter')}}</button>
                                    </div>
                                </div>
                                <div class="col-sm-2 pt-4">
                                    <div class="form-group pt-2">
                                        <button type="submit" class="btn btn-success btn-block rect-btn"><i class="fas fa-print"></i> {{trans('controls.forms.print')}}</button>
                                    </div>
                                </div>
                            </div>

                        {{-- </form> --}}
                    </div>
                    </div>
                </div>

                <div>
                    <div>
                        @php
                            $class_id=1;
                            $level_id=1;
                            $student_id=1;
                            $section_id=1;


                        @endphp

                                @foreach ($student_monthly as $student)

                                    <div class="page">
                                        <div class="col-sm-12 div_tab c_43">
                                            <table style="width:100%; line-height:25px;  cellpadding=" 0"="" cellspacing="0">
                                                <thead>
                                                    <tr>
                                                        <th colspan="2" style="font-family: Khmer OS Muol Light" class="text-center">
                                                            ព្រះរាជាណាចក្រកម្ពុជា
                                                            <br>
                                                            ជាតិ សាសនា ព្រះមហាក្សត្រ
                                                            <br> <img width="120px" height="10px" src="http://weschool\/assets/uploads/tachi.png">
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">
                                                        ក្រសួងអប់រំ យុវជន និងកីឡា
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">
                                                            {{getName("schools","name",1)}}
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2" style="font-family: Khmer OS Muol Light" class="text-center">
                                                            តារាងស្រង់ពិន្ទុសិស្សប្រចាំឆមាសទី១   ឆ្នាំសិក្សា ២០១៩ - ២០២០
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width:70%">សិស្សឈ្មោះ៖  {{$student->student_name}}</th>
                                                        <th style="width:30%">ថ្នាក់ទី៖  {{getName("classes","name",$student->class_id)}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width:70%">អត្តលេខ៖  {{getName("students","code",$student->student_id)}}</th>
                                                        <th style="width:30%">គ្រូប្រចាំថ្នាក់៖  </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table style="width:100%" cellpadding="0" cellspacing="0" border="1">
                                                                <thead>
                                                                    <tr>
                                                                        <th rowspan="3" class="text-center">មុខវិជ្ជា</th>
                                                                        <th colspan="8" class="text-center">ពិន្ទុ/ចំណាត់ថ្នាក់</th>
                                                                        <th rowspan="2" colspan="2" class="text-center">ឆមាសទី១</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th colspan="2" class="text-center">វិច្ឆិកា</th><th colspan="2" class="text-center">ធ្នូ</th><th colspan="2" class="text-center">មករា</th><th colspan="2" class="text-center">តុលា</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="text-center">ពិន្ទុ</th>
                                                                        <th class="text-center">ចំ.ថ្នាក់</th><th class="text-center">ពិន្ទុ</th>
                                                                        <th class="text-center">ចំ.ថ្នាក់</th><th class="text-center">ពិន្ទុ</th>
                                                                        <th class="text-center">ចំ.ថ្នាក់</th><th class="text-center">ពិន្ទុ</th>
                                                                        <th class="text-center">ចំ.ថ្នាក់</th>
                                                                        <th class="text-center">ពិន្ទុ</th>
                                                                        <th class="text-center">ចំ.ថ្នាក់</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @php
                                                                        $score_subject = App\Models\Score::select('scores.*','student_id')
                                                                                        ->join("students","students.id","=","scores.student_id")
                                                                                        ->join("subjects","scores.subject_id","=","subjects.id")
                                                                                        // ->with('attendance')
                                                                                        // ->where('student_id','=',$item->student_id)
                                                                                        // ->where('month','=',$date->format("m"))
                                                                                        // ->where('year','=',$date->format("Y"))
                                                                                        //    ->where('attendance','=',2)
                                                                                        ->where('student_id','=',$student->student_id)
                                                                                        ->groupBy("scores.subject_id")
                                                                                        ->orderBy("subjects.short","ASC")
                                                                                        ->get();
                                                                    @endphp
                                                                    @foreach ($score_subject as $subject)
                                                                        <tr>
                                                                            <td style="padding-left:4px">{{$subject->subject_name}}</td>
                                                                            <td class="text-center">{{$subject->score}}</td><td style="color:red; border-color:black !important" class="text-center">19</td><td class="text-center">8.63</td><td style="color:red; border-color:black !important" class="text-center">20</td><td class="text-center">7.63</td><td style="color:red; border-color:black !important" class="text-center">20</td><td class="text-center">9.12</td><td style="color:red; border-color:black !important" class="text-center">17</td><td class="text-center">6.75</td><td style="color:red; border-color:black !important" class="text-center">20</td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                                <tfoot>


                                                                    <tr>
                                                                        <th style="padding-left:4px">សរុបពិន្ទុ</th>
                                                                        <th class="text-center">78.75</th><th></th><th class="text-center">77.13</th><th></th><th class="text-center">69.04</th><th></th><th class="text-center">60.95</th><th></th><th class="text-center">47.25</th><th></th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th style="padding-left:4px">មធ្យមភាគ</th>
                                                                        <th class="text-center">8.75</th><th></th><th class="text-center">8.57</th><th></th><th class="text-center">7.67</th><th></th><th class="text-center">7.62</th><th></th><th class="text-center">6.75</th><th></th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th style="padding-left:4px">ចំណាត់ថ្នាក់</th>
                                                                        <th></th><th style="color:red; border-color:black !important" class="text-center">7</th><th></th><th style="color:red; border-color:black !important" class="text-center">12</th><th></th><th style="color:red; border-color:black !important" class="text-center">17</th><th></th><th style="color:red; border-color:black !important" class="text-center">15</th><th></th><th style="color:red; border-color:black !important" class="text-center">20</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th style="padding-left:4px">មធ្យមភាគខែ/ឆមាសទី១</th>
                                                                        <th colspan="8" <="" th=""></th><th class="text-center">{{$student->average}}</th><th></th>
                                                                    </tr>
                                                                    {{-- <tr>
                                                                        <th style="padding-left:4px">មធ្យមភាគប្រចាំឆមាស</th>
                                                                        <th colspan="8" <="" th=""></th><th class="text-center">7.45</th><th></th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th style="padding-left:4px">ចំណាត់ថ្នាក់</th>
                                                                        <th colspan="8"></th><th></th><th style="color:red; border-color:black !important" class="text-center">18</th>
                                                                    </tr> --}}
                                                                </tfoot>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="2">
                                                            <table style="width:100%">
                                                                <tbody><tr>
                                                                    <th style="width:20%; vertical-align : top !important">
                                                                        <table style="width:100%">
                                                                            <tbody><tr>
                                                                                <th>សិស្សសរុបៈ 21 នាក់</th>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>ស្រីៈ 0 នាក់</th>
                                                                            </tr>
                                                                        </tbody></table>
                                                                    </th>
                                                                    <th class="text-right" style="width:80%">
                                                                        <div style="float:right">
                                                                            <table style="width:100%">
                                                                                <tbody><tr>
                                                                                    <th class="text-center">ថ្ងៃព្រហស្បតិ៍ ១០កើត ខែមាឃ ឆ្នាំច សំរិទ្ធិស័ក ព.ស ២៥៦២</th>
                                                                                </tr>
                                                                                <tr>
                                                                                <!-- ថ្ងៃគស-->
                                                                                <th class="text-center">រាជធានីភ្នំពេញ ថ្ងៃទី១០ ខែតុលា ឆ្នាំ២០២០</th>

                                                                                </tr>
                                                                                <tr>
                                                                                    <th class="text-center">បានឃើញ និងឯកភាព
                                                                                        <div style="margin-top:20%">

                                                                                        </div>
                                                                                    </th>
                                                                                </tr>

                                                                            </tbody></table>
                                                                        </div>
                                                                    </th>
                                                                </tr>
                                                            </tbody></table>
                                                        </th>

                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>

                                @endforeach

                    </div>
                </div>



     </section>
      <!--Section: Sales Performance KPIs-->

{{-- @endif --}}


@endsection


@push('custom_js')

<!-- DataTables  & Plugins -->
<script src="{{asset('themes')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/jszip/jszip.min.js"></script>
<script src="{{asset('themes')}}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{asset('themes')}}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<!-- date-range-picker -->
<script src="{{asset('themes')}}/plugins/daterangepicker/daterangepicker.js"></script>

<script src="{{asset('dropzone/dist/dropzone.js')}}"></script>



<script>

    $(document).ready(function() {
           $('#DataTableList').DataTable({
            ordering:true,
            processing: false,
            // serverSide: true,
            "order": [[ 4, "asc" ]],
            language : $('html').attr('lang') != 'en' ? window.datatableI18n[$('html').attr('lang')] : null,
        });
    } );

        $(document).on('click',`[data-toggle="modal-ajax"]`,function(ev){
                    ev.preventDefault();
                    var url = $(this).attr('href');
                    var modalSize = $(this).data('modal-size');
                    var title = $(this).text();
                    // var target = $(this).data('mdb-target');
                    var target = $(this).data('target');


                    $.get(url).done((res)=>{
                        var $modal = $(target);
                        $modal.find('.modal-title').text(title);
                        $modal.find('.modal-dialog').addClass(modalSize);
                        $modal.find('.modal-body').html(res);

                      //  $.getScript(`${location.origin }/themes/js/mdb.min.js`).done();

                        $modal.modal('show');

                        $modal.find('button#btn-submit').unbind().click(function(ev){
                            ev.preventDefault();

                            var $form = $modal.find('form');
                            var formData = new FormData($form[0]);
                            $.ajax({
                                url : $form.attr('action'),
                                method : 'post',
                                data : formData,
                                processData : false,
                                contentType : false,
                                success : (res)=>{
                                    if(res.status){

                                        Swal.fire({
                                            position: 'top-end',
                                            icon: 'success',
                                            title: '{{__('controls.Your data has been saved or updated!')}}',
                                            showConfirmButton: false,
                                            timer: 1500
                                        });


                                        if($(this).data('allow-close')){
                                            $modal.find(`[data-dismiss="modal"]`).click();
                                        }
                                        if ( url.match('edit')) {
                                            $('#DataTableList').find(`tr[data-id="${res.data.id}"]`).html($(res.dom).html());
                                        }else{
                                            $('#DataTableList').find('tbody').prepend(res.dom);
                                        }


                                        $form[0].reset();
                                    }

                                },
                            })

                        });
                        $modal.find(`[data-dismiss="modal"]`).click(function(ev){
                            ev.preventDefault();
                            $modal.modal('hide');
                            $modal.find('.modal-body').html("");

                        });


                    });

                });

</script>


<script type="text/javascript">
    $(document).on('submit',`[data-toggle="formDelete"]`,function(ev){
        ev.preventDefault();
        $.ajax({
            url : $(this).attr('action'),
            method : 'post',
            data : new FormData(ev.target),
            processData : false,
            contentType : false,
            success : (res)=>{
                if(res.status){
                    Swal.fire(
                        '{{trans('controls.delete')}}!',
                        '{{trans('controls.Your file has been deleted.')}}',
                        'success'
                    );
                    $(this).parents("tr").remove();
                }else{
                    Swal.fire(
                        '{{trans('controls.delete')}}!',
                        '{{trans('controls.delete_message_can_not')}}',
                        'error'
                    )
                }
            }

        })
    });

    function deleteItem(id){
      Swal.fire({
        title: "{{trans('controls.Are you sure?')}}",
        text: "{{trans('controls.You will not be able to delete this!')}}",
        icon: 'warning',
        showCancelButton: true,

        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText :"{{trans('controls.cancel')}}",
        confirmButtonText:  "{{trans('controls.Yes, delete it!')}}"
      }).then((result) => {
        if (result.value) {
          $('#frmDeleteItem-'+id).submit();

        }
      })
    }
</script>




@endpush
