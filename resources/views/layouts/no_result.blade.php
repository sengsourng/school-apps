@extends('layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">{{trans('controls.top five students')}}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('/')}}">{{trans('controls.home')}}</a></li>
            <li class="breadcrumb-item active">{{trans('controls.top five students')}}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
        @if ($class_score->count()>0)
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-body">
                    <h4 class="card-title">{{trans('controls.classes.list')}}</h4><br><hr>

                    <div class="row">
                        @foreach ($class_score as $item)
                        <div class="col-md-4">
                            <a href="{{url('/student-top5/'.$item->class_id)}}" style="text-decoration: none;">
                            <div class="info-box">
                                <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-chalkboard-teacher"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">{{getName('classes','name',$item->class_id)}}</span>
                                        <span class="info-box-number text-blue">
                                        <small>បង្រៀនដោយ:</small> {{getName('teachers','name_kh',getName2('classes','teacher_id',$item->class_id))}}
                                        </span>
                                    </div>
                                <!-- /.info-box-content -->
                            </div>
                        <!-- /.info-box -->
                    </a>

                    </div>

                        @endforeach

                    </div>

                </div>
            </div>
        </div>

        @else
        <h3>មិនមានទិន្ន​ន័យ...</h3>

        @endif
    </div><!--/. container-fluid -->
  </section>
  <!-- /.content -->
@endsection


