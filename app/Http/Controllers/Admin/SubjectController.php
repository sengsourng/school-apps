<?php

namespace App\Http\Controllers\Admin;

use App\Models\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubjectController extends Controller
{
    public function index()
    {
        $data['subjects']=Subject::get();
        return view('backend.subjects.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if( request()->ajax()){
		    return view('backend.subjects.modal.add');
		}
        return view('backend.subjects.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = [
            'status' => false,
            'data' => [],
        ];
        $add = Subject::create([
            // 'business_id'  =>  1,
            'school_id'          => auth()->user()->school_id,
            'code'          => $request->code,
            'name'          => $request->name,
            'description'    => $request->description,

            'status'          => $request->status,
            'short'          => $request->short,
        ]);

        if ($add) {
            $response = [
                'status' => true,
                'data' => $add,
                'dom' => view('backend.subjects.rows',['row' => $add])->render(),
            ];
        }
        if(request()->ajax()){

            return $response;
        }
        // dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {

        if( request()->ajax()){
		    return view('backend.subjects.modal.show',compact('subject'));
		}

        // return view('backend.schools.show',compact('school','user','teacher'));
    }

    public function edit(Subject $subject)
    {

        // dd($subject);
        if( request()->ajax()){
		    return view('backend.subjects.modal.edit',compact('subject'));
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Subject $subject)
    {
        $response = [
            'status' => false,
            'data' => [],
            // 'id' => $category->id,
        ];
        $update = $subject->update([
            'school_id'          => auth()->user()->school_id,
            'code'          => $request->code,
            'name'          => $request->name,
            'description'    => $request->description,

            'status'          => $request->status,
            'short'          => $request->short,
        ]);

        if ($update) {
            $response = [
                'status' => true,
                'data' =>['id'=> $subject->id],
                'dom' => view('backend.subjects.rows',['row' => $subject])->render(),

            ];

        }
        // dd($response);

        if(request()->ajax()){
            return $response;
        }
    }

    public function destroy($id)
    {
        //
        $response = [
            'status' => false,
        ];
        $delete=Subject::findOrFail($id);

        // $userCheck=Classes::where('id','=',$id)->count();
        // dd($userCheck);

        // if ($userCheck == 0) {
        //     $delete->delete();
        //     $response = [
        //         'status' => true,
        //     ];
        // }

        $delete->delete();
        $response = [
            'status' => true,
        ];


        if (request()->ajax()) {
            return $response;
        }

        return redirect()->route('subjects.index');
    }
}
