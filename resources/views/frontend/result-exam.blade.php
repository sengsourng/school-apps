@extends('layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">{{trans('controls.top five students')}}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('/')}}">{{trans('controls.home')}}</a></li>
            <li class="breadcrumb-item active">{{trans('controls.top five students')}}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      {{-- Score List --}}
      <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-info">
                    <div class="card-body">
                        <h4 class="card-title">{{trans('controls.classes.list')}}</h4><br><hr>

                        <div class="row overflow-auto" style="height: 200px;">
                            @foreach ($class_score as $item)
                            <div class="col-md-4">
                                <a href="{{url('/result-exam/'.$item->class_id)}}" style="text-decoration: none;">
                                        <div class="info-box">
                                            <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-chalkboard-teacher"></i></span>

                                                <div class="info-box-content">
                                                    <span class="info-box-text">{{getName('classes','name',$item->class_id)}}</span>
                                                    <span class="info-box-number text-blue">
                                                    <small>បង្រៀនដោយ:</small> {{getName('teachers','name_kh',getName2('classes','teacher_id',$item->class_id))}}
                                                    </span>
                                                </div>
                                            <!-- /.info-box-content -->
                                        </div>
                                    <!-- /.info-box -->
                                </a>
                            </div>



                            @endforeach
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="card card-outline card-info">
                    <div class="card-body">
                        <h4 class="card-title">{{trans('controls.top five students')}}</h4>
                        <div class="table-responsive">
                                    @php
                                        $score_rank=array();
                                    @endphp

                                    @foreach ($student_score as $item)
                                        @php
                                            $score_rank[]=$item->total_score;
                                        @endphp
                                    @endforeach
                                    {{-- {{$score_rank}} --}}
                                    @php
                                    rsort($score_rank);

                                        $arrlength = count($score_rank);
                                        $rank = 1;
                                        $prev_rank = $rank;
                                    @endphp
                                @foreach ($student_score as $key => $item)
                                        @if ($key<5)
                                            @php
                                                $student_top5[$key]=$item->student_name;
                                                if ($key==0) {
                                                    // echo $score_rank[$key]."- Rank".($rank);
                                                    $rank2[$key]=$rank;
                                                    // echo $rank;
                                                }
                                                elseif ($score_rank[$key] != $score_rank[$key-1]) {
                                                    $rank++;
                                                    $prev_rank = $rank;
                                                    $rank2[$key]=$rank;
                                                    // echo $score_rank[$key]."- Rank".($rank);
                                                    // echo $rank;
                                                }
                                                else{
                                                    $rank++;
                                                    // echo $score_rank[$key]."- Rank".($prev_rank);
                                                    $rank2[$key]=$prev_rank;

                                                    // echo $prev_rank;
                                                }
                                                // echo "<br>";
                                            @endphp
                                        @endif
                                @endforeach

                            </div>

                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col" style="width:300xp;">{{trans('controls.students.name')}}</th>
                                    <th scope="col">{{trans('controls.students.gender')}}</th>
                                        @php
                                            use KhmerDateTime\KhmerDateTime;

                                            $date = Carbon\Carbon::now();// will get you the current date, time
                                            $month=$date->format("m");
                                            $year=$date->format("Y");

                                                $score_subject = App\Models\Score::select('scores.*','student_id')
                                                ->join("students","students.id","=","scores.student_id")
                                                // ->with('attendance')
                                                // ->where('student_id','=',$item->student_id)
                                                ->where('month','=',$month)
                                                ->where('year','=',$year)
                                                // ->where('section_id','=',1)
                                                ->where('class_id','=',$class_id)
                                                ->groupBy("scores.subject_id")
                                                ->get();
                                        @endphp
                                        @foreach ($score_subject as $item)
                                            {{-- <th>{{$item->subject_name}}</th> --}}
                                        @endforeach
                                    <th scope="col">ពិន្ទុសរុប</th>
                                    <th scope="col">ម.ភាគ</th>
                                    <th scope="col">ចំ.ថ្នាក់</th>
                                    <th scope="col">និទ្ទេស</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $score_rank=array();
                                    @endphp

                                    @foreach ($student_score as $item)
                                        @php
                                            $score_rank[]=$item->total_score;
                                        @endphp
                                    @endforeach
                                    {{-- {{$score_rank}} --}}
                                    @php
                                    rsort($score_rank);

                                        $arrlength = count($score_rank);
                                        $rank = 1;
                                        $prev_rank = $rank;


                                    @endphp

                                    {{-- @php
                                        $rank=1;
                                        $prev_rank = $rank;
                                    @endphp --}}

                                @foreach ($student_score as $key => $item)
                                <tr>
                                    <th scope="row">{{$item->student_id}}</th>
                                    <td>{{$item->student_name}}</td>
                                    <td>{{getName('students','gender',$item->student_id)}}</td>
                                    @php
                                            $score_subject = App\Models\Score::select('scores.*','student_id')
                                            ->join("students","students.id","=","scores.student_id")
                                            // ->with('attendance')
                                            ->where('student_id','=',$item->student_id)
                                            ->where('month','=',$month)
                                            ->where('year','=',$year)
                                            //    ->where('attendance','=',2)
                                            //    ->where('gender','=','ស')
                                            // ->groupBy("scores.class_id")
                                            ->get();
                                    @endphp
                                        @foreach ($score_subject as $item)
                                            {{-- <td>{{$item->score}}</td> --}}
                                        @endforeach
                                        <td class="text-blue">{{number_format(($item->total_score), 2, '.', '')}}</td>
                                        <td class="text-blue">{{number_format(($item->total_score/$score_subject->count()), 2, '.', '')}}</td>
                                    {{-- <td class="{{ number_format(($item->total_score/$score_subject->count()), 2, '.', '')<4.99?'text-red':'text-blue'}}">{{number_format($item->total_score, 2, '.', '')}} </td> --}}
                                    {{-- <td class="{{ number_format(($item->total_score/$score_subject->count()), 2, '.', '')<4.99?'text-red':'text-blue'}}">{{number_format(($item->total_score/$score_subject->count()), 2, '.', '')}}</td> --}}
                                    <td class="text-red">
                                        @php

                                                if ($key==0) {
                                                    // echo $score_rank[$key]."- Rank".($rank);
                                                    echo $rank;
                                                }
                                                elseif ($score_rank[$key] != $score_rank[$key-1]) {
                                                    $rank++;
                                                    $prev_rank = $rank;
                                                    // echo $score_rank[$key]."- Rank".($rank);
                                                    echo $rank;
                                                }
                                                else{
                                                    $rank++;
                                                    // echo $score_rank[$key]."- Rank".($prev_rank);
                                                    echo $prev_rank;
                                                }
                                                echo "<br>";
                                        @endphp

                                    </td>
                                    <td>
                                        {{checkCredit(number_format(($item->total_score/$score_subject->count()), 2, '.', ''))}}
                                    </td>
                                </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>







      </div>
    </div><!--/. container-fluid -->
  </section>
  <!-- /.content -->
@endsection


