<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    use HasFactory;
    protected $fillable=[
        'skill_id',
        'level_id',
        // 'school_id',
        'code',
        'name' ,
        'subject_id' ,
        'description',
        'score_type',
        'status',
    ];



    public function setSubjectAttribute($value)
    {
        $this->attributes['subject_id'] = json_encode($value);
    }

    /**
     * Get the categories
     *
     */
    public function getSubjectAttribute($value)
    {
        return $this->attributes['subject_id'] = json_decode($value);
    }

    public function GetSubject($id = null)
    {
        return Subject::findOrFail($id)->name;
    }

}
