@extends('layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">ផ្ទាំងគ្រប់គ្រង</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">ទំព័រដើម</a></li>
            <li class="breadcrumb-item active">ផ្ទាំងគ្រប់គ្រង</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user-graduate"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">សិស្សសរុប</span>
              {{-- <span class="info-box-number">
                400
                <small>នាក់</small>
                ស្រី 230 <small>នាក់</small>
              </span> --}}

              <span class="info-box-number">
                {{$all_students->count()}}
                <small>នាក់</small>
                ស្រី {{$all_students_female->count()}} <small>នាក់</small>
              </span>

            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user-edit"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">សិស្សឆ្នាំថ្មី</span>
              <span class="info-box-number">
                {{$all_students_new->count()}}

                <small>នាក់</small>
                ស្រី {{$all_students_female_new->count()}} <small>នាក់</small>

              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box mb-3">
            <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-chalkboard-teacher"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">សិស្សអវត្តមាន</span>
                <span class="info-box-number">{{$student_absent->count()}} <small>នាក់</small>, ស្រី {{$class_absent_female->count()}} <small>នាក់</small></span>
              </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box mb-3">
            <span class="info-box-icon bg-success elevation-1"><i class="fas fa-chalkboard-teacher"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">សាស្រ្តាចារ្យ</span>
              <span class="info-box-number">50 <small>នាក់</small>, ស្រី <small>នាក់</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box mb-3">
            <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-chalkboard-teacher"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">សាស្រ្តាចារ្យអវត្តមាន</span>
              <span class="info-box-number">50 <small>នាក់</small>, ស្រី <small>នាក់</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->


        <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box mb-3">
            <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">អ្នក​ប្រើ​ប្រាស់​</span>
              <span class="info-box-number">{{$user_count->count()}} <small>នាក់</small>, ប្រើបាន {{$user_count_active->count()}}<small>នាក់</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->




    </div><!--/. container-fluid -->
  </section>
  <!-- /.content -->
@endsection
