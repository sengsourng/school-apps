<?php

use App\Models\Student;
use App\Models\Teacher;
use App\Models\AcademicYear;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SkillController;
use App\Http\Controllers\SchoolController;
use Psr\Http\Message\ServerRequestInterface;
use App\Http\Controllers\Admin\ExamController;
use App\Http\Controllers\Admin\RoomController;
use App\Http\Controllers\Admin\ClassController;
use App\Http\Controllers\Admin\SectionController;
use App\Http\Controllers\Admin\StudentController;
use App\Http\Controllers\Admin\SubjectController;
use App\Http\Controllers\Admin\TeacherController;
use App\Http\Controllers\Admin\AttendanceController;
use App\Http\Controllers\Admin\AcademicYearController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/result_by_student',function(){
    // return view('backend.reports.result_by_student');
});

Route::get('/test', function () {
//  getSchool(1,'active');
    return view('backend.students.import');

    // return human_file_size(1024*1024);
    // return getSchool(1);
});



Route::get('lang/{locale}', function ($locale) {
    App::setLocale($locale);
    session()->put('locale', $locale);
    return redirect()->back();
});


Route::get('/', [App\Http\Controllers\FrontPageController::class, 'index'])->name('index');
Route::get('/contact', [App\Http\Controllers\FrontPageController::class, 'contact'])->name('contact');
Route::get('/about', [App\Http\Controllers\FrontPageController::class, 'about'])->name('about');
Route::get('/student-absent', [App\Http\Controllers\FrontPageController::class, 'student_absent'])->name('student.absent');
Route::get('/student-top5/{class}', [App\Http\Controllers\FrontPageController::class, 'student_top5'])->name('student.top5');
Route::get('/student-top5_print/{class}', [App\Http\Controllers\FrontPageController::class, 'student_top5_print'])->name('student.top5.print');
Route::get('/teachers', [App\Http\Controllers\FrontPageController::class, 'teachers'])->name('teachers');
Route::get('/result-exam/{class}', [App\Http\Controllers\FrontPageController::class, 'result_exam'])->name('result.exam');
Route::get('/student_register', [App\Http\Controllers\FrontPageController::class, 'student_register'])->name('student_register');




Route::get('/get_top_5_monthly',[App\Http\Controllers\Admin\GeneralController::class,'get_top_5_monthly'])->name('get_top_5_monthly');

// Filter Scores Table
Route::get('/filterScores',[App\Http\Controllers\Admin\GeneralController::class,'filterScores'])->name('filterScores');
// filterScoresClassID
Route::get('/filterScoresClassID',[App\Http\Controllers\Admin\GeneralController::class,'filterScoresClassID'])->name('filterScoresClassID');

// Route::post('get_classes', [App\Http\Controllers\Admin\ClassController::class,'get_classes'])->name('get_classes');


// Route::match(['get','post'],'exams',[App\Http\Controllers\Admin\ExamController::class,'index'])->name('exams.create');



Auth::routes();

// Admin Route
Route::group(['prefix' =>'admin','middleware'=>['web','auth']], function () {
// Route::prefix('admin')->group(function(){
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('admin.home');

    Route::resources([
        'settings/schools' => SchoolController::class,
        'settings/skills' => SkillController::class,
        'settings/subjects' => SubjectController::class,
        'settings/sections' => SectionController::class,
        'settings/rooms' => RoomController::class,
        'settings/classes' => ClassController::class,
        'settings/credit_score' => SkillController::class,

        'teachers' => TeacherController::class,
        'students' => StudentController::class,
        // 'exams' => ExamController::class,
        'exam_details' => TeacherController::class,
        // 'attendances' => AttendanceController::class,
        'attendance_teachers' => TeacherController::class,
        // 'attendance_students' => AttendanceController::class,
        'settings/academic_years' => AcademicYearController::class,

    ]);

    // Student
    Route::get('students/import_view',[App\Http\Controllers\Admin\StudentController::class, 'ImportView'])->name('students.import_view');
    Route::post('students/import',[App\Http\Controllers\Admin\StudentController::class, 'import'])->name('students.import');


    Route::post('students/filter',[App\Http\Controllers\Admin\StudentController::class, 'filter'])->name('students.filter');

    //Attendance Controller
			Route::match(['get','post'],'attendance_students',[App\Http\Controllers\Admin\AttendanceController::class,'index'])->name('student_attendance.create');
			Route::post('student_attendance/save', [App\Http\Controllers\Admin\AttendanceController::class,'student_attendance_save'])->name('student_attendance.save');
			Route::post('classes/get_classes', [App\Http\Controllers\Admin\ClassController::class,'get_classes'])->name('classes.get_classes');

    // Exams
    Route::match(['get','post'],'exams',[App\Http\Controllers\Admin\ExamController::class,'index'])->name('exams.create');
    // student_score_save
    Route::post('student_score/save', [App\Http\Controllers\Admin\ExamController::class,'student_score_save'])->name('student_score.save');



    // Reports
    Route::get('score/monthly_top_five',[App\Http\Controllers\Admin\GeneralController::class, 'monthly_top_five'])->name('admin.monthly_top_five');
    Route::get('score/yearly_top_five',[App\Http\Controllers\Admin\GeneralController::class, 'yearly_top_five'])->name('admin.yearly_top_five');
    Route::get('score/result_by_student',[App\Http\Controllers\Admin\GeneralController::class, 'result_by_student'])->name('admin.result_by_student');

    Route::match(['get','post'],'score/student_atten_report',[App\Http\Controllers\Admin\AttendanceController::class,'student_atten_report'])->name('admin.student_attendance.student_atten_report');



    Route::match(['get','post'],'score/generateMonltyScoreReport',[App\Http\Controllers\Admin\GeneralController::class,'generateMonltyScoreReport'])->name('admin.generateMonltyScoreReport');
    Route::post('score/generateMonltyScoreReportSave', [App\Http\Controllers\Admin\GeneralController::class,'generateMonltyScoreReportSave'])->name('admin.generateMonltyScoreReportSave');

    // student_score_by_class
    Route::match(['get','post'],'score/student_score_by_class',[App\Http\Controllers\Admin\GeneralController::class,'student_score_by_class'])->name('admin.student_score_by_class');

    // student_report_by_class
    Route::match(['get','post'],'score/student_report_by_class',[App\Http\Controllers\Admin\GeneralController::class,'student_report_by_class'])->name('admin.student_report_by_class');




    // Route::resource('schools', SchoolController::class)->only([
    //     'index', 'show'
    // ]);
});

// Manager
Route::group(['prefix' =>'manager','middleware'=>['web','auth']], function () {
// Route::prefix('manager')->group(function(){

    Route::get('import_view',[App\Http\Controllers\Admin\StudentController::class, 'ImportView'])->name('students.import_view');
    Route::get('export', [App\Http\Controllers\Admin\StudentController::class, 'export'])->name('students.export');
    Route::post('import', [App\Http\Controllers\Admin\StudentController::class, 'import'])->name('students.import');

    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('manager.home');



});

// Teacher
Route::group(['prefix' =>'teacher','middleware'=>['web','auth']], function () {
// Route::prefix('teacher')->group(function(){
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('teacher.home');
});
