<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreditScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_scores', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->unsignedBigInteger('skill_id')->references('id')->on('skills')->onDelete('cascade');

            $table->string('credit')->nullable();

            $table->text('level_id')->nullable();
            $table->text('level_name')->nullable();

            $table->text('subject_id')->nullable();
            $table->text('subject_name')->nullable();

            $table->double('full_score')->nullable();
            $table->double('min_score')->nullable();
            $table->double('max_score')->nullable();
            $table->double('cal_score')->nullable();

            $table->string('color')->default('#6495ED');

            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_scores');
    }
}
