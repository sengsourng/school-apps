<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Score;
use App\Models\Classes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class GeneralController extends Controller
{
    public function monthly_top_five($class_id=null)
    {
        $date = Carbon::now();// will get you the current date, time

       if($class_id=='all'){
            $data['student_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
            ->join("students","students.id","=","scores.student_id")
            // ->with('attendance')
            ->where('month','=',$date->format("m"))
            ->where('year','=',$date->format("Y"))
        //    ->where('attendance','=',2)
        //    ->where('gender','=','ស')
            ->groupBy("students.id")
            ->orderBy("score_avg","DESC")

            ->get();

            $data['class_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
                                ->join("students","students.id","=","scores.student_id")
                                // ->with('attendance')
                                ->where('month','=',$date->format("m"))
                                ->where('year','=',$date->format("Y"))
                                //    ->where('attendance','=',2)
                                //    ->where('gender','=','ស')
                                ->groupBy("scores.class_id")
                                ->get();

       }else{
            $data['student_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
            ->join("students","students.id","=","scores.student_id")
            // ->with('attendance')
            ->where('month','=',$date->format("m"))
            ->where('year','=',$date->format("Y"))
            ->where('class_id','=',$class_id)
        //    ->where('attendance','=',2)
        //    ->where('gender','=','ស')
            ->groupBy("students.id")
            // ->orderBy("total_score")
            ->orderBy("score_avg","DESC")


            ->get();

        $data['class_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
                                ->join("students","students.id","=","scores.student_id")
                                // ->with('attendance')
                                ->where('month','=',$date->format("m"))
                                ->where('year','=',$date->format("Y"))
                                //    ->where('attendance','=',2)
                                //    ->where('gender','=','ស')
                                ->groupBy("scores.class_id")
                                ->orderBy("score_avg","DESC")
                                ->get();

       }
       $data['score_subject'] = Score::select('scores.*','student_id')
                                ->join("students","students.id","=","scores.student_id")
                                // ->with('attendance')
                                // ->where('student_id','=',26)
                                ->where('month','=',$date->format("m"))
                                ->where('year','=',$date->format("Y"))
                                //    ->where('attendance','=',2)
                                //    ->where('gender','=','ស')
                                // ->groupBy("scores.class_id")
                                ->get();
            // dd($data);
            // foreach($class_score as $key => $score){
            //     $score[$key]= $score->total_score;
            // }
            // dd($score);
            // dd($data);
            $data['class_id']=$class_id;

            $data['classes']=Classes::get();

            // dd($data['class_score']);
            // dd($data);

        return view('backend.reports.monthly_top_five',$data);
    }

    public function get_top_5_monthly($class_id= null)
    {
        $date = Carbon::now();// will get you the current date, time

       if($class_id=='all'){
            $data['student_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
            ->join("students","students.id","=","scores.student_id")
            // ->with('attendance')
            ->where('month','=',$date->format("m"))
            ->where('year','=',$date->format("Y"))
        //    ->where('attendance','=',2)
        //    ->where('gender','=','ស')
            ->groupBy("students.id")
            ->orderBy("score_avg","DESC")

            ->get();

            $data['class_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
                                ->join("students","students.id","=","scores.student_id")
                                // ->with('attendance')
                                ->where('month','=',$date->format("m"))
                                ->where('year','=',$date->format("Y"))
                                //    ->where('attendance','=',2)
                                //    ->where('gender','=','ស')
                                ->groupBy("scores.class_id")
                                ->get();

       }else{
            $data['student_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
            ->join("students","students.id","=","scores.student_id")
            // ->with('attendance')
            ->where('month','=',$date->format("m"))
            ->where('year','=',$date->format("Y"))
            ->where('class_id','=',$class_id)
        //    ->where('attendance','=',2)
        //    ->where('gender','=','ស')
            ->groupBy("students.id")
            // ->orderBy("total_score")
            ->orderBy("score_avg","DESC")


            ->get();

        $data['class_score'] = Score::select('scores.*','student_id',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
                                ->join("students","students.id","=","scores.student_id")
                                // ->with('attendance')
                                ->where('month','=',$date->format("m"))
                                ->where('year','=',$date->format("Y"))
                                //    ->where('attendance','=',2)
                                //    ->where('gender','=','ស')
                                ->groupBy("scores.class_id")
                                ->orderBy("score_avg","DESC")
                                ->get();

       }
       $data['score_subject'] = Score::select('scores.*','student_id')
                                ->join("students","students.id","=","scores.student_id")
                                // ->with('attendance')
                                // ->where('student_id','=',26)
                                ->where('month','=',$date->format("m"))
                                ->where('year','=',$date->format("Y"))
                                //    ->where('attendance','=',2)
                                //    ->where('gender','=','ស')
                                // ->groupBy("scores.class_id")
                                ->get();
            // dd($data);
            // foreach($class_score as $key => $score){
            //     $score[$key]= $score->total_score;
            // }
            // dd($score);
            // dd($data);
            $data['class_id']=$class_id;

            $data['classes']=Classes::get();

            // dd($data['class_score']);


            // if( request()->ajax()){
                return view('backend.reports.includes.top_5_monthly',$data);
            // }

    }

    public function yearly_top_five()
    {
        return view('backend.reports.yearly_top_five');

    }

    public function result_by_student()
    {

        $data['student_monthly']=Score::where('year','=',2021)
                                // ->where('year','=',2021)
                                ->where('class_id','=',1)
                                ->groupBy('student_id')
                                // ->where('class_id','=',1)
                                ->get();
// dd($data);
        return view('backend.reports.result_by_student',$data);

    }


    public function ResultStudentMonthly(){



    }

    public function filterScores()
    {

        $student_score = DB::table('scores')
            ->where('section_id', '=', 2)
            ->orWhere(function($query) {
                $query->where('class_id', 1)
                      ->where('year', '=', 2021);
            })
            // ->orderBy('student_id','DESC')
            ->orderBy('subject_id','ASC')
            // ->groupBy('month')
            // ->groupBy('student_id')
            ->groupBy('subject_id')
            ->get();

        $response['data'] = $student_score;

        return response()->json($response);
    }

    public function filterScoresClassID(Request $request)
    {
        $class_id = $request->class_id;
        $section_id = $request->section_id;
            // $student_scores=Score::where('year','=',2021)
            // ->where('section_id','=',$section_id)
            // ->where('class_id','=',$class_id)
            // ->groupBy('student_id')
            // // ->where('class_id','=',1)
            // ->get();

            $student_scores= Score::select('scores.*','student_id','students.code','students.dob','gender','sections.name as section_name',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
            ->join("students","students.id","=","scores.student_id")
            ->join("sections","sections.id","=","scores.section_id")
            // ->with('attendance')
            // ->where('month','=',$date->format("m"))
            // ->where('year','=',$date->format("Y"))
            ->where('class_id','=',$class_id)
            ->where('section_id','=',$section_id)
            // ->where('class_id','=',1)
        //    ->where('attendance','=',2)
        //    ->where('gender','=','ស')
            ->groupBy("students.id")
            // ->orderBy("total_score")
            ->orderBy("score_avg","DESC")
            ->get();
        // dd($student_scores);
        $response['data'] = $student_scores;

        return response()->json($response);
    }

    public function generateMonltyScoreReport(Request $request)
    {

        // dd($request->all());
        $student_score = [];
        // dd($student_score);
        $class_id = $request->class_id;
        $level_id = $request->level_id;
        $section_id = $request->section_id;
        $date = $request->date;
            // $student_scores=Score::where('year','=',2021)
            // ->where('section_id','=',$section_id)
            // ->where('class_id','=',$class_id)
            // ->groupBy('student_id')
            // // ->where('class_id','=',1)
            // ->get();
            if($class_id == "" || $section_id == "" || $date == ""||$level_id==""){
                return view('backend.reports.generate_score_student_monthly',compact('student_score','date','class_id','section_id','level_id'));
            }else{

                    $data['student_score']= Score::select('scores.*','student_id','students.code','students.dob','gender','sections.name as section_name',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
                    ->join("students","students.id","=","scores.student_id")
                    ->join("sections","sections.id","=","scores.section_id")
                    // ->with('attendance')

                    ->where('month','=',date('m', strtotime($date)))
                    ->where('year','=',date('Y', strtotime($date)))

                    ->where('class_id','=',$class_id)
                    ->where('section_id','=',$section_id)
                    // ->where('class_id','=',1)
                //    ->where('attendance','=',2)
                //    ->where('gender','=','ស')
                    ->groupBy("students.id")
                    // ->orderBy("total_score")
                    ->orderBy("score_avg","DESC")
                    ->get();
                // dd($student_scores);
                // $response['data'] = $student_scores;
                // // return response()->json($response);

                // $data['student_score']=response()->json($response);


                    $data['level_id']=$level_id;
                    $data['class_id']=$class_id;
                    $data['section_id']=$section_id;
                    $data['date']=$date;

            // dd($data);

                return view('backend.reports.generate_score_student_monthly',$data);
            }
    }

    // Save Score Monthly
    public function generateMonltyScoreReportSave(Request $request)
    {
        dd($request->all());
    }




    // Get Student Score by Class
    public function student_score_by_class(Request $request)
    {

        // dd($request->all());
        $student_score = [];
        // dd($student_score);
        $class_id = $request->class_id;
        $level_id = $request->level_id;
        $section_id = $request->section_id;
        $date = $request->date;
            // $student_scores=Score::where('year','=',2021)
            // ->where('section_id','=',$section_id)
            // ->where('class_id','=',$class_id)
            // ->groupBy('student_id')
            // // ->where('class_id','=',1)
            // ->get();
            if($class_id == "" || $section_id == "" || $date == ""||$level_id==""){
                return view('backend.reports.student_score_by_class',compact('student_score','date','class_id','section_id','level_id'));
            }else{

                    $data['student_score']= Score::select('scores.*','student_id','students.code','students.dob','gender','sections.name as section_name',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
                    ->join("students","students.id","=","scores.student_id")
                    ->join("sections","sections.id","=","scores.section_id")
                    // ->with('attendance')

                    ->where('month','=',date('m', strtotime($date)))
                    ->where('year','=',date('Y', strtotime($date)))

                    ->where('class_id','=',$class_id)
                    ->where('section_id','=',$section_id)
                    // ->where('class_id','=',1)
                //    ->where('attendance','=',2)
                //    ->where('gender','=','ស')
                    ->groupBy("students.id")
                    // ->orderBy("total_score")
                    ->orderBy("score_avg","DESC")
                    ->get();
                // dd($student_scores);
                // $response['data'] = $student_scores;
                // // return response()->json($response);

                // $data['student_score']=response()->json($response);


                    $data['level_id']=$level_id;
                    $data['class_id']=$class_id;
                    $data['section_id']=$section_id;
                    $data['date']=$date;

            // dd($data);

                return view('backend.reports.student_score_by_class',$data);
            }
    }

    // student_report_by_class

    public function student_report_by_class(Request $request)
    {

        // dd($request->all());
        $student_score = [];
        // dd($student_score);
        $class_id = $request->class_id;
        $level_id = $request->level_id;
        $section_id = $request->section_id;
        $date = $request->date;
            // $student_scores=Score::where('year','=',2021)
            // ->where('section_id','=',$section_id)
            // ->where('class_id','=',$class_id)
            // ->groupBy('student_id')
            // // ->where('class_id','=',1)
            // ->get();
            if($class_id == "" || $section_id == "" || $date == ""||$level_id==""){
                return view('backend.reports.student_report_by_class',compact('student_score','date','class_id','section_id','level_id'));
            }else{

                    $data['student_score']= Score::select('scores.*','student_id','students.code','students.dob','gender','sections.name as section_name','students.*',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
                    ->join("students","students.id","=","scores.student_id")
                    ->join("sections","sections.id","=","scores.section_id")
                    // ->with('attendance')

                    ->where('month','=',date('m', strtotime($date)))
                    ->where('year','=',date('Y', strtotime($date)))

                    ->where('class_id','=',$class_id)
                    ->where('section_id','=',$section_id)
                    // ->where('class_id','=',1)
                //    ->where('attendance','=',2)
                //    ->where('gender','=','ស')
                    ->groupBy("students.id")
                    // ->orderBy("total_score")
                    ->orderBy("score_avg","DESC")
                    ->get();
                // dd($student_scores);
                // $response['data'] = $student_scores;
                // // return response()->json($response);

                // $data['student_score']=response()->json($response);


                    $data['level_id']=$level_id;
                    $data['class_id']=$class_id;
                    $data['section_id']=$section_id;
                    $data['date']=$date;

            // dd($data);

                return view('backend.reports.student_report_by_class',$data);
            }
    }

}
