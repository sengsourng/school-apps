<?php

namespace App\Http\Controllers\Admin;

use App\Models\Room;
use App\Models\Section;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SectionController extends Controller
{
    public function index()
    {
        $data['sections']=Section::get();
        return view('backend.sections.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if( request()->ajax()){
		    return view('backend.sections.modal.add');
		}
        return view('backend.sections.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request);
        $response = [
            'status' => false,
            'data' => [],
        ];

        $subjects=implode(',', $request->subject_id);
        $add = Section::create([
            'skill_id'  =>  1,
            'level_id'  => 1,
            // 'school_id'          => auth()->user()->school_id,
            'code'          => $request->code,
            'name'          => $request->name,
            'subject_id'          => $subjects, //$request->subject_id,
            'description'    => $request->description,
            'score_type'    => $request->score_type,
            // 'status'    => $request->status,
        ]);

        if ($add) {
            $response = [
                'status' => true,
                'data' => $add,
                'dom' => view('backend.sections.rows',['row' => $add])->render(),
            ];
        }
        if(request()->ajax()){
            return $response;
        }else{
            return redirect()->route('sections.index');
        }

    }

    public function get_section(Request $request)
    {
        $results = Section::where('class_id',$request->class_id)->get();
        $sections = '';
        $sections .= '<option value="">'.trans('controls.menus.sections').'</option>';
        foreach($results as $data){
            $sections .= '<option value="'.$data->id.'">'.$data->name.'</option>';
        }
        return $sections;
    }

    public function show(Room $room)
    {

        if( request()->ajax()){
		    return view('backend.sections.modal.show',compact('room'));
		}

        // return view('backend.schools.show',compact('school','user','teacher'));
    }

    public function edit(Room $room)
    {

        if( request()->ajax()){
		    return view('backend.sections.modal.edit',compact('room'));
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Room $room)
    {
        $response = [
            'status' => false,
            'data' => [],
            // 'id' => $category->id,
        ];
        $update = $room->update([
            'school_id'          => auth()->user()->school_id,
            'code'          => $request->code,
            'name'          => $request->name,
            'description'    => $request->description,
            'status'    => $request->status,
        ]);

        if ($update) {
            $response = [
                'status' => true,
                'data' =>['id'=> $room->id],
                // 'data' => $roomes,
                'dom' => view('backend.sections.rows',['row' => $room])->render(),

            ];

        }
        // dd($response);

        if(request()->ajax()){
            return $response;
        }
    }

    public function destroy($id)
    {
        //
        $response = [
            'status' => false,
        ];
        $delete=Section::findOrFail($id);

        // $userCheck=Classes::where('id','=',$id)->count();
        // dd($userCheck);

        // if ($userCheck == 0) {
        //     $delete->delete();
        //     $response = [
        //         'status' => true,
        //     ];
        // }

        $delete->delete();
        $response = [
            'status' => true,
        ];


        if (request()->ajax()) {
            return $response;
        }

        return redirect()->route('sections.index');
    }
}
