<div class="container pt-1">
    <form action="{{ route('academic_years.store') }}" method="post" enctype="multipart/form-data">
        @csrf

        <div class="row">
            <div class="col-md-12 bg-info pt-3">

                  <div class="form-group row">
                        <label for="lable_s_name" class="col-sm-3 col-form-label">{{trans('controls.acadamic_years.name')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="name"  class="form-control" id="lable_s_name" placeholder="២០២០-២០២១">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="lable_s_phone" class="col-sm-3 col-form-label">{{trans('controls.acadamic_years.year')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="year"  class="form-control" id="lable_s_phone" placeholder="២០២០">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lable_s_email" class="col-sm-3 col-form-label">{{trans('controls.acadamic_years.decription')}}</label>
                        <div class="col-sm-9">
                            <input type="email" name="description"  class="form-control" id="lable_s_email" placeholder="2020-2021">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lable_s_city" class="col-sm-3 col-form-label">{{trans('controls.acadamic_years.active')}}</label>
                        <div class="col-sm-9">
                           <select name="active" id="active" class="form-control">
                               <option value="1" class="form-control">{{ trans('controls.acadamic_years.active') }}</option>
                               <option value="0" class="form-control">{{ trans('controls.acadamic_years.disactive') }}</option>
                           </select>
                        </div>
                    </div>




            </div>



        </div>

    </form>
</div>
