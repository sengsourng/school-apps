<div class="container pt-1">

    <form>
        <div class="row">
            <div class="col-md-8 bg-info pt-3">

                <div class="form-group row">
                    <label for="lbl_code" class="col-sm-4 col-form-label">{{trans('controls.teachers.code')}}</label>
                    <div class="col-sm-8">
                        <input type="text" name="code" value="{{$teacher->code}}"  class="form-control" id="lbl_code" placeholder="{{trans('controls.students.code')}}">
                    </div>
                </div>

                <div class="form-group row">
                        <label for="lbl_name_kh" class="col-sm-4 col-form-label">{{trans('controls.students.name')}}</label>
                        <div class="col-sm-8">
                            <input type="text" name="name_kh" value="{{$teacher->name_kh}}" class="form-control" id="lbl_name_kh" placeholder="នាមត្រកូល នាមខ្លួន">
                        </div>
                </div>

                <div class="form-group row">
                    <label for="lbl_name_en" class="col-sm-4 col-form-label">{{trans('controls.students.name_en')}}</label>
                    <div class="col-sm-8">
                        <input type="text" name="name_en" value="{{$teacher->name_en}}" class="form-control" id="lbl_name_en" placeholder="FirstName LastName">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="lbl_gender" class="col-sm-4 col-form-label">{{trans('controls.students.gender')}}</label>
                    <div class="col-sm-8">
                        <select class="custom-select" name="gender">
                            <option value="">{{trans('controls.forms.Please Select')}}</option>
                            <option {{$teacher->gender=='m'?'selected':''}}  value="m">{{trans('controls.forms.Male')}}</option>
                            <option {{$teacher->gender=='f'?'selected':''}}  value="f">{{trans('controls.forms.Female')}}</option>
                        </select>
                    </div>
                </div>

                @php
                    $today = \Carbon\Carbon::now();
                    $currentDateTime = \Carbon\Carbon::now();
                    // $newDateTime = \Carbon\Carbon::now()->subMonth(12);
                    $newDateTime = \Carbon\Carbon::now()->subYear(20);
                    // dd( date('Y-m-d', strtotime($newDateTime)));
                @endphp

                <div class="form-group row">
                    <label for="lbl_dob" class="col-sm-4 col-form-label">{{trans('controls.teachers.dob')}}</label>
                    <div class="col-sm-8">
                        <input type="date" name="dob" value="{{date('Y-m-d', strtotime($teacher->dob))}}" class="form-control" id="lbl_dob" placeholder="Date of Birth" ">
                    </div>
                </div>


                <div class="form-group row">
                    <label for="lbl_pob" class="col-sm-4 col-form-label">{{trans('controls.students.pob')}}</label>
                    <div class="col-sm-8">
                        <input type="text" name="pob" value="{{$teacher->pob}}" class="form-control" id="lbl_pob" placeholder="ភូមិ ឃុំ​ ស្រុក ខេត្ត">
                    </div>
                </div>
                {{-- address --}}
                <div class="form-group row">
                    <label for="lbl_address" class="col-sm-4 col-form-label">{{trans('controls.students.address')}}</label>
                    <div class="col-sm-8">
                        <input type="text" name="address" value="{{$teacher->address}}" class="form-control" id="lbl_address" placeholder="ផ្ទះលេខ ក្រុម ភូមិ ឃុំ​ ស្រុក ខេត្ត">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="lbl_city" class="col-sm-4 col-form-label">{{trans('controls.students.city')}}</label>
                    <div class="col-sm-8">
                        <input type="text" name="city" value="{{$teacher->city}}" class="form-control" id="lbl_city" placeholder="Siem Reap (ភាសារអង់គ្លេស)">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="lbl_gender" class="col-sm-4 col-form-label">{{trans('controls.teachers.is_active')}}</label>
                    <div class="col-sm-8">
                        <select class="custom-select" name="gender">
                            <option value="">{{trans('controls.forms.Please Select')}}</option>
                            <option {{$teacher->gender=='1'?'selected':''}} value="1">Active</option>
                            <option  {{$teacher->gender=='0'?'selected':''}} value="0">Disable</option>
                        </select>
                    </div>
                </div>

            </div>

            <div class="col-md-4 pt-3">
                <div class="card" style="width: 100%;">
                    <label for="lbl_city" class="col-sm-4 col-form-label">{{trans('controls.students.profile')}}</label>

                    <div id="placehere" class="uploadPreview rounded mx-auto d-block mb-2" width="196"></div>

                    @if ($teacher->profile===null)
                        <img src="{{asset('images/teachers/default_teacher.jpg')}}" id="uploadPreview" onclick="removeFns()" class="uploadPreview rounded mx-auto d-block mb-2" width="192" />
                    @elseif($teacher->profile==="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50")
                        <img src="{{asset('images/teachers/default_teacher.jpg')}}" id="uploadPreview" onclick="removeFns()" class="uploadPreview rounded mx-auto d-block mb-2" width="192" />
                    @else
                        <img src="{{asset('images/teachers/'. $teacher->profile )}}" id="uploadPreview" onclick="removeFns()" class="uploadPreview rounded mx-auto d-block mb-2" width="192" />
                    @endif



                    <div class="card-body">
                      {{-- <h5 class="card-title">Card title</h5> --}}
                      <div class="btn-group d-flex justify-content-center" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-primary">
                            <label>
                                <i class="fas fa-paperclip"></i> {{trans('controls.forms.brow_image')}}
                                <input  type="file" id="uploadImage" onchange="Preview_Image_Before_Upload('uploadImage', 'uploadPreview');" name="profile"/>
                            </label>
                        </button>
                        <button type="button" class="btn btn-danger " onclick="removeFns()"><i class="fas fa-trash"></i> {{trans('controls.forms.remove')}}</button>
                    </div>
                    </div>
                  </div>


            </div>

        </div>

    </form>
</div>
