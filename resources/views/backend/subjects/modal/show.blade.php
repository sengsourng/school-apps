<div class="container pt-1">
    {{-- <form action="{{ route('schools.update',$school) }}" method="post"> --}}
    <form>
        <div class="row">
            <div class="col-md-12  pt-3">

                    <div class="form-group row">
                        <label for="lbl_code" class="col-sm-3 col-form-label">{{trans('controls.subjects.code')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="code" value="{{$subject->code}}"  class="form-control" id="lbl_code" placeholder="S001">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lable_s_name" class="col-sm-3 col-form-label">{{trans('controls.subjects.name')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="name" value="{{$subject->name}}"  class="form-control" id="lable_s_name" placeholder="ភាសាខ្មែរ">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lbl_description" class="col-sm-3 col-form-label">{{trans('controls.subjects.description')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="description" value="{{$subject->description}}"  class="form-control" id="lbl_description" placeholder="Khmer Lelteral">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lbl_sort" class="col-sm-3 col-form-label">{{trans('controls.subjects.sort')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="short" value="{{$subject->short}}"  class="form-control" id="lbl_sort" placeholder="10">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lbl_status" class="col-sm-3 col-form-label">{{trans('controls.subjects.status')}}</label>
                        <div class="col-sm-9">
                            <select name="status" id="lbl_status" class="form-control">
                                <option value="">{{trans('controls.forms.Please Select')}}</option>
                                <option value="1" {{$subject->status==1?'selected':''}} >{{trans('controls.subjects.active')}}</option>
                                <option value="0" {{$subject->status==0?'selected':''}}>{{trans('controls.subjects.disable')}}</option>
                            </select>
                        </div>
                    </div>

            </div>



        </div>



    </form>
</div>
