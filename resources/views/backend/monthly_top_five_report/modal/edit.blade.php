<div class="container pt-1">
    <form action="{{ route('schools.update',$school) }}" method="post">
        @csrf
        {{ method_field('PUT') }}

        <div class="row">
            <div class="col-md-6 bg-info pt-3">

                  <div class="form-group row">
                        <label for="lable_s_name" class="col-sm-3 col-form-label">{{trans('controls.schools.name')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="school_name" value="{{$school->name}}" class="form-control" id="lable_s_name" placeholder="បឋម.ស្វាយធំ">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="lable_s_phone" class="col-sm-3 col-form-label">{{trans('controls.schools.phone')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="school_phone" value="{{$school->phone}}" class="form-control" id="lable_s_phone" placeholder="092771244">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lable_s_email" class="col-sm-3 col-form-label">{{trans('controls.schools.email')}}</label>
                        <div class="col-sm-9">
                            <input type="email" name="school_email" value="{{$school->email}}" class="form-control" id="lable_s_email" placeholder="yourschool@gmail.com">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lable_s_city" class="col-sm-3 col-form-label">{{trans('controls.schools.city')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="school_city" value="{{$school->city}}" class="form-control" id="lable_s_city" placeholder="Siem Reap">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lable_s_address" class="col-sm-3 col-form-label">{{trans('controls.schools.address')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="school_address" value="{{$school->address}}"  class="form-control" id="lable_s_address" placeholder="ភូមិ ឃុំ ស្រុក">
                        </div>
                    </div>



            </div>


            <div class="col-md-6 bg-success pt-3">
                {{-- Director --}}
                <div class="form-group row">
                    <label for="lable_name" class="col-sm-3 col-form-label">{{trans('controls.schools.director_name')}}</label>
                    <div class="col-sm-9">
                        <input type="text" name="teacher_name" value="{{$teacher->name_kh}}-{{$teacher->name_en}}" class="form-control" id="lable_name" placeholder="នាមត្រកូល នាមខ្លួន - FirstName LastName">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="lable_gender" class="col-sm-3 col-form-label">{{trans('controls.teachers.gender')}}</label>
                    <div class="col-sm-9">
                        <select class="custom-select" name="gender">
                            <option>{{trans('controls.forms.Please Select')}}</option>
                            <option {{$teacher->gender=='m'?'selected':''}} value="m">{{trans('controls.forms.Male')}}</option>
                            <option {{$teacher->gender=='f'?'selected':''}} value="f">{{trans('controls.forms.Female')}}</option>
                          </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="lable_dob" class="col-sm-3 col-form-label">{{trans('controls.teachers.dob')}}</label>
                    <div class="col-sm-9">
                        <div class="input-group date" id="lable_dob" data-target-input="nearest">
                            <input type="date" name="teacher_dob" value="{{$teacher->dob}}" id="lable_dob" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="lable_pob" class="col-sm-3 col-form-label">{{trans('controls.teachers.pob')}}</label>
                    <div class="col-sm-9">
                        <input type="text" name="teacher_pob" value="{{$teacher->pob}}" class="form-control" id="lable_pob" placeholder="គោករុន មុខប៉ែន ពួក សៀមរាប">
                    </div>
                </div>

                 <div class="form-group row">
                    <label for="lable_t_address" class="col-sm-3 col-form-label">{{trans('controls.teachers.address')}}</label>
                    <div class="col-sm-9">
                        <input type="text"  name="teacher_address" value="{{$teacher->address}}" class="form-control" id="lable_t_address" placeholder="គោករុន មុខប៉ែន ពួក សៀមរាប">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="lable_t_city" class="col-sm-3 col-form-label">{{trans('controls.schools.city')}}</label>
                    <div class="col-sm-9">
                        <input type="text" name="teacher_city" value="{{$teacher->city}}" class="form-control" id="lable_t_city" placeholder="សៀមរាប">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="lable_t_email" class="col-sm-3 col-form-label">{{trans('controls.schools.email')}}</label>
                    <div class="col-sm-9">
                        <input type="text" name="teacher_email" value="{{$user->email}}" class="form-control" id="lable_t_email" placeholder="teacher_name@gmail.com">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="lable_t_profile" class="col-sm-3 col-form-label">{{trans('controls.teachers.profile')}}</label>
                    <div class="col-sm-9">
                        <div class="custom-file">
                            <input type="file">
                        </div>
                    </div>
                </div>

            </div>

        </div>



    </form>
</div>
