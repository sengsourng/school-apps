<div class="container pt-1">
    <form action="{{ route('classes.update',$class) }}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('PUT') }}

        <div class="row">
            <div class="col-md-12  pt-3">

                    <div class="form-group row">
                        <label for="skill_id" class="col-sm-3 col-form-label">{{trans('controls.skills.name')}}</label>
                        <div class="col-sm-9">
                            <select name="skill_id" id="skill_id" class="form-control" required>
                                <option value="">{{ trans('controls.forms.Please Select') }}</option>
                                {{ create_option('skills','id','name',$class->skill_id) }}
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="level_id" class="col-sm-3 col-form-label">{{trans('controls.levels.name')}}</label>
                        <div class="col-sm-9">
                            <select name="level_id" id="level_id" class="form-control" required>
                                <option value="">{{ trans('controls.levels.select') }}</option>
                                {{ create_option('levels','id','name',$class->level_id) }}
                            </select>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="lbl_code" class="col-sm-3 col-form-label">{{trans('controls.classes.code')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="code" value="{{$class->code}}"  class="form-control" id="lbl_code" placeholder="C001">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lable_s_name" class="col-sm-3 col-form-label">{{trans('controls.classes.name')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="name" value="{{$class->name}}" class="form-control" id="lable_s_name" placeholder="2020-2021">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lbl_description" class="col-sm-3 col-form-label">{{trans('controls.classes.description')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="description" value="{{$class->description}}" class="form-control" id="lbl_description" placeholder="2020-2021">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="teacher_id" class="col-sm-3 col-form-label">{{trans('controls.teachers.name')}}</label>
                        <div class="col-sm-9">
                            <select name="teacher_id" id="teacher_id" class="form-control" required>
                                <option value="">{{ trans('controls.forms.Please Select') }}</option>
                                {{ create_option('teachers','id','name_kh',$class->teacher_id) }}
                            </select>
                        </div>
                    </div>


            </div>



        </div>

    </form>
</div>
