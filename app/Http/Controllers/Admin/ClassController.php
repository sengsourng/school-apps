<?php

namespace App\Http\Controllers\Admin;

use App\Models\Classes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use KhmerDateTime\KhmerDateTime;

class ClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['classes']=Classes::get();
        return view('backend.classes.home',$data);
    }

    public function get_classes(Request $request)
    {
        $results = Classes::where('level_id',$request->level_id)->get();
        $classes = '';
        $classes .= '<option value="">'.trans('controls.classes.select').'</option>';
        foreach($results as $data){
            $classes .= '<option value="'.$data->id.'">'.$data->name.'</option>';
        }
        return $classes;
    }

    public function create()
    {
        if( request()->ajax()){
		    return view('backend.classes.modal.add');
		}
        return view('backend.classes.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $response = [
            'status' => false,
            'data' => [],
        ];
        $add = Classes::create([
            // 'business_id'  =>  1,
            'school_id' => 1,//auth()->user()->school_id,
            'level_id' =>$request ->level_id,
            'skill_id' =>$request ->skill_id,
            'code' =>$request ->code,
            'name' =>$request ->name,
            'description' =>$request ->description,
            'teacher_id' =>$request ->teacher_id,
            'teacher_name' => getName2('teachers','name_kh',$request ->teacher_id) ,
            'status' => 'active',
        ]);
// dd($add);

// dd($response);
        if ($add) {
            $response = [
                'status' => true,
                'data' => $add,
                'dom' => view('backend.classes.rows',['row' => $add])->render(),
            ];
        }
        if(request()->ajax()){
            return $response;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data=Classes::findOrFail($id);
        if( request()->ajax()){
		    return view('backend.classes.modal.show',(['year'=>$data]));
		}

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Classes $class)
    {
        //
        // $academic=Classes::findOrFail($id);


        // dd($class);
        if( request()->ajax()){
		    return view('backend.classes.modal.edit',compact('class'));
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Classes $class)
    {
        $response = [
            'status' => false,
            'data' => [],
            // 'id' => $category->id,
        ];
        $update = $class->update([
            // 'business_id'  =>  1,
            'code'    => $request->code,
            'name'          => $request->name,
            'level_id'          => $request->level_id,
            'description'    => $request->description,
            'skill_id'    => $request->skill_id,
            'teacher_id'    => $request->teacher_id,
        ]);

        if ($update) {
            $response = [
                'status' => true,
                'data' =>['id'=> $class->id],
                // 'data' => $Classes,
                'dom' => view('backend.classes.rows',['row' => $class])->render(),

            ];

        }
        // dd($response);

        if(request()->ajax()){
            return $response;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $response = [
            'status' => false,
        ];
        $year=Classes::findOrFail($id);

        // $userCheck=Classes::where('id','=',$id)->count();
        // dd($userCheck);

        // if ($userCheck == 0) {
        //     $year->delete();
        //     $response = [
        //         'status' => true,
        //     ];
        // }

        $year->delete();
        $response = [
            'status' => true,
        ];


        if (request()->ajax()) {
            return $response;
        }

        return redirect()->route('classess.index');
    }


}
