<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AcademicYear;
use Illuminate\Http\Request;

class AcademicYearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['academic_years']=AcademicYear::get();
        return view('backend.academic_year.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if( request()->ajax()){
		    return view('backend.academic_year.modal.add');
		}
        return view('backend.academic_year.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $response = [
            'status' => false,
            'data' => [],
        ];
        $add = AcademicYear::create([
            // 'business_id'  =>  1,
            'name' =>$request ->name,
            'year' =>$request ->year,
            'description' =>$request ->description,
            'active' =>$request ->active
        ]);

        if ($add) {
            $response = [
                'status' => true,
                'data' => $add,
                'dom' => view('backend.academic_year.rows',['row' => $add])->render(),
            ];
        }
        if(request()->ajax()){

            return $response;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data=AcademicYear::findOrFail($id);
        if( request()->ajax()){
		    return view('backend.academic_year.modal.show',(['year'=>$data]));
		}

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(AcademicYear $AcademicYear)
    {
        //
        // $academic=AcademicYear::findOrFail($id);
        if( request()->ajax()){
		    return view('backend.academic_year.modal.edit',compact('AcademicYear'));
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,AcademicYear $AcademicYear)
    {
        $response = [
            'status' => false,
            'data' => [],
            // 'id' => $category->id,
        ];
        $update = $AcademicYear->update([
            // 'business_id'  =>  1,
            'name'          => $request->name,
            'year'          => $request->year,
            'description'    => $request->description,
            'is_active'    => $request->is_active,
        ]);

        if ($update) {
            $response = [
                'status' => true,
                'data' =>['id'=> $AcademicYear->id],
                // 'data' => $AcademicYear,
                'dom' => view('backend.academic_year.rows',['row' => $AcademicYear])->render(),

            ];

        }
        // dd($response);

        if(request()->ajax()){
            return $response;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $response = [
            'status' => false,
        ];
        $year=AcademicYear::findOrFail($id);

        // $userCheck=AcademicYear::where('id','=',$id)->count();
        // dd($userCheck);

        // if ($userCheck == 0) {
        //     $year->delete();
        //     $response = [
        //         'status' => true,
        //     ];
        // }

        $year->delete();
        $response = [
            'status' => true,
        ];


        if (request()->ajax()) {
            return $response;
        }

        return redirect()->route('academic_years.index');
    }


}
