@extends('layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">{{trans('controls.about')}} វិទ្យាល័យនិងបឋមសិក្សាស្វាយធំ</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('/')}}">{{trans('controls.home')}}</a></li>
            <li class="breadcrumb-item active">{{trans('controls.about')}}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Info boxes -->
      <div class="row">

            <div class="col-md-8">
                <p>
                    សាលាបឋមសិក្សាស្វាយធំចាប់ផ្តើមកើតឡើងនៅឆ្នាំ១៩៨០ ក្រោយរបបប្រល័យពូជសាសន៍បានដួលរលំក្រោមទិដ្ឋភាពដ៏ក្រខ្សត់របស់ប្រជាពលរដ្ឋដែលទើបតែងើបពីរបបខ្មៅងងឹត ទោះបីជាក្រលំបាកយ៉ាងណាក៏ដោយប្រជាពលរដ្ឋទាំងអស់បានសាងសង់អគារមួយខ្នង ធ្វើអំពើឈើប្រក់ស្លឹកក្រោមការដឹកនាំរបស់លោកគ្រូ អ៊ិត គឹមសេង និលោកតឹក ឈៀង មានថ្នាក់រៀន ២ ថ្នាក់សិស្ស សិស្សសុរបមាន ៨០ នាស្រី៣៥ នាក់ ។
                </p>

                <p>
                    នៅឆ្នាំ១៩៩៣ ដល់ ១៩៩៤ ដោយសារសិស្សកាន់តែកើនឡើង សហគមន៍ នៅក្នុងភូមិបានពិភាក្សាគ្នាបង្កើត អគារសិក្សាមួយខ្នងមាន៣បន្ទប់ ដោយធ្វើអំពីឈើដើម ត្នោតប្រក់ក្បឿង។ ថវិកានេះបានមកពីព្រះសង្ឃ ព្រះតេជគុណ ជាម និងប្រជាពលរដ្ឋនៅក្នុងភូមិទាំង បួននៅក្នុងឆ្នាំនេះ គោលដៅអប់រំរៀបចំប្រព័ន្ធ៦ឆ្នាំ នៅបឋមសិក្សាដោយការខិតខំប្រឹងប្រែងរបស់ លោកនាយករងគឺលោក តឹក ឈៀង បានខិតខំប្រឹងប្រែង តាំងពីចំណុចសូន្យ រហូតបានអគារ ៣ ខ្នងមាន៩ បន្ទប់ ហើយបាន ទទួល មរណៈភាពដោយរោគាពាធ នៅថ្ងៃ១៨ ខែតុលា ឆ្នាំ ១៩៩៣។
                </p>

            </div>

            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <img style="width: 100%;" src="{{asset('images/about/about_01.jpg')}}" alt="About School">
                    </div>
                    <div class="col-md-12">
                      <img style="width: 100%;" src="{{asset('images/about/about_02.jpg')}}" alt="About School">
                    </div>
                    <div class="col-md-12">
                      <img style="width: 100%;" src="{{asset('images/about/about_03.jpg')}}" alt="About School">
                    </div>
                </div>
            </div>
      </div>
      <!-- /.row -->




    </div><!--/. container-fluid -->
  </section>
  <!-- /.content -->
@endsection
