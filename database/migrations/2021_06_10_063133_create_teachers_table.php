<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('code')->nullable();
            $table->unsignedBigInteger('school_id')->references('id')->on('schools')->onDelete('cascade');

            $table->string('name_en')->nullable();
            $table->string('name_kh')->nullable();
            $table->string('gender')->nullable();
            $table->date('dob')->nullable();
            $table->string('pob')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('city_slug')->nullable();
            $table->tinyInteger('is_active')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('profile')->default('default.jpg');
            $table->string('bio')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
