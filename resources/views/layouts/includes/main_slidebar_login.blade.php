<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/')}}" class="brand-link">
      <img src="{{asset('images/logo.png')}}" alt="School Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">{{getSchool(Auth()->user()->school_id)??"គ្រប់គ្រងសាលា" }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"> {{ Auth::user()->name }}</a>
        </div>
      </div>



      <!-- Sidebar Menu  -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

            <li class="nav-item">
                <a href="{{route('admin.home')}}" class="nav-link {{request()->segment(2)==''?'active':''}}">
                    <i class="nav-icon fas fa-home"></i>
                    <p>
                      ផ្ទាំងគ្រប់គ្រង
                    </p>
                  </a>
              </li>

          {{-- <li class="nav-header">គ្រប់គ្រងសិស្ស</li> --}}

          <li class="nav-item">
            <a href="{{route('students.index')}}" class="nav-link {{request()->segment(2)=='students'?'active':''}}">
              {{-- <i class="nav-icon fas fa-th"></i> --}}
              <i class="fas fa-user-graduate nav-icon"></i>

              <p>
                បញ្ចីសិស្សទាំងអស់
                {{-- <span class="right badge badge-success">{{user_count('student') }}</span> --}}
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('student_attendance.create')}}" class="nav-link {{request()->segment(2)=='attendance_students'?'active':''}}">
                <i class="fas fa-user-check nav-icon"></i>
                <p>
                  វត្តមានសិស្ស
                  {{-- <span class="right badge badge-danger">New</span> --}}
                </p>
              </a>
          </li>

          {{-- <li class="nav-header">គ្រប់គ្រងគ្រូ</li> --}}

          <li class="nav-item">
            <a href="{{route('teachers.index')}}" class="nav-link {{request()->segment(2)=='teachers'?'active':''}}">
                <i class="fas fa-chalkboard-teacher nav-icon"></i>
              {{-- <i class="fad fa-chalkboard-teacher nav-icon"></i> --}}
              <p>
                បញ្ចីគ្រូ​ទាំងអស់
                {{-- <span class="right badge badge-info">50​ នាក់</span> --}}
              </p>
            </a>

             {{-- exams --}}
             <li class="nav-item">
                <a href="{{route('exams.create')}}" class="nav-link {{request()->segment(2)=='exams'?'active':''}}">

                    <i class="fas fa-star nav-icon"></i>
                <p>{{trans('controls.menus.exams')}}</p>
                </a>
            </li>

          </li>
          <li class="nav-item {{request()->segment(2)=='score'?'menu-open':''}}">
            <a href="#" class="nav-link {{request()->segment(2)=='score'?'active':''}}">
                {{-- <i class="fas fa-th"></i> --}}
                <i class="fas fa-list-ol nav-icon"></i>
                <p>
                  របាយការណ៍
                  {{-- <span class="right badge badge-danger">New</span> --}}
                </p>
                <i class="right fas fa-angle-left"></i>

              </a>

              <ul class="nav nav-treeview">

                <li class="nav-item">
                    <a href="{{route('admin.generateMonltyScoreReport')}}" class="nav-link {{request()->segment(3)=='generateMonltyScoreReport'?'active':''}}">
                      <i class="fas fa-arrow-circle-right nav-icon"></i>
                      <p>រៀបចំពិន្ទុ​ប្រចាំខែ</p>
                    </a>
                  </li>

                <li class="nav-item">
                  <a href="{{route('admin.monthly_top_five')}}" class="nav-link {{request()->segment(3)=='monthly_top_five'?'active':''}}">
                    <i class="fas fa-arrow-circle-right nav-icon"></i>
                    <p>{{trans('controls.menus.monthly_top_five')}}</p>
                  </a>
                </li>
                {{-- student_report_by_class--}}
                <li class="nav-item">
                  <a href="{{route('admin.student_report_by_class')}}" class="nav-link {{request()->segment(3)=='student_report_by_class'?'active':''}}">
                    <i class="fas fa-arrow-circle-right nav-icon"></i>
                    <p>{{trans('controls.student_report_by_class')}}</p>
                  </a>
                </li>

                 {{-- student_score_by_class--}}
                 <li class="nav-item">
                    <a href="{{route('admin.student_score_by_class')}}" class="nav-link {{request()->segment(3)=='student_score_by_class'?'active':''}}">
                      <i class="fas fa-arrow-circle-right nav-icon"></i>
                      <p>{{trans('controls.student_score_by_class')}}</p>
                    </a>
                  </li>


                {{-- result_by_student --}}
                <li class="nav-item">
                  <a href="{{route('admin.result_by_student')}}" class="nav-link {{request()->segment(3)=='result_by_student'?'active':''}}">
                    <i class="fas fa-arrow-circle-right nav-icon"></i>
                    <p>{{trans('controls.menus.result_by_student')}}</p>
                  </a>
                </li>


                {{-- Report Student Attendance --}}
                <li class="nav-item">
                    <a href="{{route('admin.student_attendance.student_atten_report')}}" class="nav-link {{request()->segment(3)=='student_atten_report'?'active':''}}">

                        <i class="fas fa-star nav-icon"></i>
                    <p>{{trans('controls.student_atten_report')}}</p>
                    </a>
                </li>

              </ul>
          </li>

          @if (auth()->user()->usertype=='admin')
            <li class="nav-item {{request()->segment(2)=='settings'?'menu-open':''}}">
                <a href="#" class="nav-link {{request()->segment(2)=='settings'?'active':''}}">
                {{-- <i class="nav-icon fas fa-tachometer-alt"></i> --}}
                <i class="nav-icon fas fa-cog"></i>
                <p>
                    កំណត់
                    <i class="right fas fa-angle-left"></i>
                </p>
                </a>
                <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{route('schools.index')}}" class="nav-link {{request()->segment(3)=='schools'?'active':''}}">
                    {{-- <i class="far fa-circle nav-icon"></i> --}}
                    <i class="fas fa-school nav-icon"></i>
                    <p>ពត៌មានសាលា</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('academic_years.index')}}" class="nav-link {{request()->segment(3)=='academic_years'?'active':''}}">
                    {{-- <i class="far fa-circle nav-icon"></i> --}}
                    <i class="fas fa-calendar-day nav-icon"></i>
                    <p>ឆ្នាំសិក្សា</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('classes.index')}}" class="nav-link {{request()->segment(3)=='classes'?'active':''}}">
                    {{-- <i class="far fa-circle nav-icon"></i> --}}
                    <i class="fas fa-calendar-day nav-icon"></i>
                    <p>{{trans('controls.menus.classes')}}</p>
                    </a>
                </li>

                {{-- Skills --}}
                <li class="nav-item">
                    <a href="{{route('skills.index')}}" class="nav-link {{request()->segment(3)=='skills'?'active':''}}">
                    {{-- <i class="fas fa-calendar-day "></i> --}}
                    <i class="fas fa-shapes nav-icon"></i>
                    <p>{{trans('controls.menus.skills')}}</p>
                    </a>
                </li>
                {{-- subjects --}}

                <li class="nav-item">
                    <a href="{{route('subjects.index')}}" class="nav-link {{request()->segment(3)=='subjects'?'active':''}}">
                    {{-- <i class="fas fa-calendar-day nav-icon"></i> --}}
                    <i class="fas fa-graduation-cap nav-icon"></i>
                    <p>{{trans('controls.menus.subjects')}}</p>
                    </a>
                </li>
                {{-- sections --}}

                <li class="nav-item">
                    <a href="{{route('sections.index')}}" class="nav-link {{request()->segment(3)=='sections'?'active':''}}">
                    {{-- <i class="fas fa-calendar-day nav-icon"></i> --}}
                    <i class="fas fa-user-clock nav-icon"></i>
                    <p>{{trans('controls.menus.sections')}}</p>
                    </a>
                </li>
                {{-- rooms --}}
                <li class="nav-item">
                    <a href="{{route('rooms.index')}}" class="nav-link {{request()->segment(3)=='rooms'?'active':''}}">
                    {{-- <i class="fas fa-calendar-day nav-icon"></i> --}}
                    <i class="fas fa-chalkboard nav-icon"></i>
                    <p>{{trans('controls.menus.rooms')}}</p>
                    </a>
                </li>

                {{-- credit_score --}}
                <li class="nav-item">
                    <a href="{{route('credit_score.index')}}" class="nav-link {{request()->segment(3)=='credit_score'?'active':''}}">

                        <i class="fas fa-star nav-icon"></i>
                    <p>{{trans('controls.menus.credit_score')}}</p>
                    </a>
                </li>




                <li class="nav-item">
                    <a href="{{url('/')}}" class="nav-link">
                    {{-- <i class="fas fa-user-cog nav-icon"></i> --}}
                    <i class="fas fa-user-shield nav-icon"></i>
                    <p>សិទ្ធិប្រើប្រាស់</p>
                    </a>
                </li>
                </ul>
            </li>
          @endif



          <li class="nav-item">

                <a class="nav-link" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt nav-icon"></i>
                <p>
                    ចាកចេញ
                </p>
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>

          </li>


        </ul>
      </nav>
      <!-- /.sidebar-menu  -->
    </div>
    <!-- /.sidebar -->
  </aside>
