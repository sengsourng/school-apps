<tr data-id="{{ $row->id}}">
    <th scope="row" style="width: 40px !important;">{{$row->id}}</th>
    <td>
        {{$row->code}}
    </td>
    <td>
        {{$row->name}}
    </td>
    <td>

        {{getName('levels','name',$row->level_id)}}
    </td>

    <td>
        {{$row->description}}
    </td>

    <td>
        <span class="text-success">
            <i class="fas fa-user me-1"></i> {{getName('teachers','name_kh',$row->teacher_id)}}
            {{-- <i class="fas fa-clock me-1"></i><span> {{ session()->get('locale')=='kh'?KhmerDateTime\KhmerDateTime::parse($row->created_at)->fromNow():$row->created_at->diffForHumans() }}</span> --}}
        </span>

    </td>


    <td>
        <a class="btn btn-primary  edit_button btn-sm" href="{{route('classes.show',$row)}}"
                data-toggle="modal-ajax"
                {{-- data-modal-size="modal-lg" --}}
                {{-- data-modal-size="modal-xl" --}}
                data-mdb-toggle="modal"
                data-target="#openModal"
        ><i class="fa fa-eye left"></i> {{ trans('controls.forms.Show')}}</a>

        <a class="btn btn-primary  edit_button btn-sm" href="{{route('classes.edit',$row->id)}}"
                data-toggle="modal-ajax"
                {{-- data-modal-size="modal-lg" --}}
                {{-- data-modal-size="modal-xl" --}}
                data-mdb-toggle="modal"
                data-target="#openModal"
        ><i class="fa fa-edit left"></i> {{ trans('controls.forms.Edit')}}</a>


        <a href="javascript:deleteItem({{ $row->id }})"​​ class="btn btn-danger delete_button btn-sm"><i class="fa fa-trash-alt left"></i> {{ trans('controls.forms.Delete')}}</a>


        <form data-toggle="formDelete"  id="frmDeleteItem-{{ $row->id }}" style="display: none" action="{{ route('classes.destroy',$row->id) }}" role="form" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        </form>

    </td>
</tr>
