<tr data-id="{{ $row->id}}">
    <th scope="row" style="width: 40px !important;">{{$row->id}}</th>
    <td>
        <ul class="products-list product-list-in-card pl-2 pr-2">
            <li class="item">
              <div class="product-img">

                @if ($row->profile==="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50")
                        <img src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50" alt="" class="rounded  img-size-50">

                @elseif ($row->profile==NULL)
                    <img src="{{asset('images/students/default_student.jpg')}}" alt="" class="rounded  img-size-50">
                @else
                    <img src="{{asset('images/students/'.$row->profile)}}" class="rounded  img-size-50" />
                @endif

            </div>
              <div class="product-info">
                <a href="javascript:void(0)" class="product-title">
                    {{$row->name_kh}}

                  <span class="badge badge-success float-right">{{trans('controls.students.code')}}: {{$row->code}}</span></a>
                <span class="product-description">
                    <span class="text-success">
                        {{$row->name_en}}
                    </span>
                </span>
              </div>
            </li>
        </ul>

    </td>
    <td>

        {{$row->gender==='ប'?'ប្រុស':'ស្រី'}}
    </td>
    <td>
        {{$row->dob}}
    </td>

    <td>
        {{$row->pob}}
    </td>

    <td>
        <i class="fas fa-male me-1"></i><span> {{$row->father}}</span>
        <br>
        <span class="text-success">
            <i class="fas fa-female me-1"></i><span> {{$row->mother}}</span>

        </span>

    </td>


    <td>
        <a class="btn btn-primary  edit_button btn-sm" href="{{route('students.show',$row)}}"
                data-toggle="modal-ajax"
                {{-- data-modal-size="modal-lg" --}}
                data-modal-size="modal-xl"
                {{-- data-mdb-toggle="modal" --}}
                data-target="#openModal"
        ><i class="fa fa-eye left"></i> {{ trans('controls.forms.Show')}}</a>

        <a class="btn btn-primary  edit_button btn-sm" href="{{route('students.edit',$row)}}"
                {{-- data-toggle="modal-ajax" --}}
                {{-- data-modal-size="modal-lg" --}}
                {{-- data-modal-size="modal-xl" --}}
                {{-- data-mdb-toggle="modal" --}}
                {{-- data-target="#openModal" --}}
        ><i class="fa fa-edit left"></i> {{ trans('controls.forms.Edit')}}</a>


        <a href="javascript:deleteItem({{ $row->id }})"​​ class="btn btn-danger delete_button btn-sm"><i class="fa fa-trash-alt left"></i> {{ trans('controls.forms.Delete')}}</a>


        <form data-toggle="formDelete"  id="frmDeleteItem-{{ $row->id }}" style="display: none" action="{{ route('students.destroy',$row->id) }}" role="form" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        </form>

    </td>
</tr>
