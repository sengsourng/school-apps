
@extends('layouts.admin')

@section('content')

    <section class="content pt-2">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header pt-4">
                  <h3 class="card-title text-center">{{trans('controls.forms.Add New')}}</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{ route('classes.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                  <div class="card-body">

                    <div class="row">
                        <div class="col-md-12  pt-3">
                                <div class="form-group row">
                                    <label for="skill_id" class="col-sm-3 col-form-label">{{trans('controls.skills.name')}}</label>
                                    <div class="col-sm-9">

                                        <select name="skill_id" id="skill_id" class="form-control" required>
                                            <option value="">{{ trans('controls.forms.Please Select') }}</option>
                                            {{ create_option('skills','id','name') }}
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="level_id" class="col-sm-3 col-form-label">{{trans('controls.levels.name')}}</label>
                                    <div class="col-sm-9">

                                        <select name="level_id" id="level_id" class="form-control" required>
                                            <option value="">{{ trans('controls.levels.select') }}</option>
                                            {{ create_option('levels','id','name') }}
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="lbl_code" class="col-sm-3 col-form-label">{{trans('controls.classes.code')}}</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="code"  class="form-control" id="lbl_code" placeholder="C001">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lable_s_name" class="col-sm-3 col-form-label">{{trans('controls.classes.name')}}</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="name"  class="form-control" id="lable_s_name" placeholder="ថ្នាក់ទី ១ (ក)">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lbl_description" class="col-sm-3 col-form-label">{{trans('controls.classes.description')}}</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="description"  class="form-control" id="lbl_description" placeholder="Grade 1 (A)">
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="teacher_id" class="col-sm-3 col-form-label">{{trans('controls.teachers.name')}}</label>
                                    <div class="col-sm-9">
                                        {{-- <input type="text" name="year"  class="form-control" id="lable_s_phone" placeholder="២០២០"> --}}
                                        <select name="teacher_id" id="teacher_id" class="form-control" required>
                                            <option value="">{{ trans('controls.forms.Please Select') }}</option>
                                            {{ create_option('teachers','id','name_kh') }}
                                        </select>
                                    </div>
                                </div>


                        </div>
                    </div>

                  <div class="card-footer float-right">
                    <button type="submit" class="btn btn-primary mr-3">{{trans('controls.save')}}</button>
                    <button type="submit" class="btn btn-danger">{{trans('controls.cancel')}}</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->


            </div>
            <!--/.col (left) -->

          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>

@endsection
