<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentYearliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_yearlies', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->unsignedBigInteger('skill_id')->references('id')->on('skills')->onDelete('cascade');
            $table->unsignedBigInteger('student_id')->references('id')->on('students')->onDelete('cascade');
            $table->unsignedBigInteger('exam_id')->references('id')->on('exams')->onDelete('cascade');

            $table->unsignedBigInteger('level_id')->references('id')->on('levels')->onDelete('cascade');
            $table->unsignedBigInteger('class_id')->references('id')->on('classes')->onDelete('cascade');
            $table->string('class_name')->nullable();

            $table->unsignedBigInteger('section_id')->references('id')->on('sections')->onDelete('cascade');
            $table->string('section_name')->nullable();

            $table->integer('rank')->nullable();
            $table->decimal('score')->nullable();
            $table->decimal('average')->nullable();
            $table->string('result')->nullable();
            $table->string('credit')->nullable();

            $table->tinyInteger('final')->nullable();
            $table->string('teacher_name')->default('No Teacher');
            $table->unsignedBigInteger('teacher_id')->references('id')->on('teachers')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_yearlies');
    }
}
