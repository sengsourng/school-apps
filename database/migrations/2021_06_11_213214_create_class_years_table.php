<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_years', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->string('school_name')->nullable();

            $table->unsignedBigInteger('skill_id')->references('id')->on('skills')->onDelete('cascade');
            $table->string('skill_name')->nullable();

            $table->unsignedBigInteger('class_id')->references('id')->on('skills')->onDelete('cascade');
            $table->string('class_name')->nullable();

            $table->unsignedBigInteger('room_id')->references('id')->on('skills')->onDelete('cascade');
            $table->string('room_name')->nullable();

            $table->unsignedBigInteger('teacher_id')->references('id')->on('skills')->onDelete('cascade');
            $table->string('teacher_name')->nullable();

            $table->unsignedBigInteger('level_id')->references('id')->on('skills')->onDelete('cascade');
            $table->string('level_name')->nullable();

            $table->string('study_year')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_years');
    }
}
