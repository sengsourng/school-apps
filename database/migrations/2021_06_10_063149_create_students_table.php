<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->string('code')->nullable();
            $table->string('name_en')->nullable();
            $table->string('name_kh')->nullable();
            $table->string('gender')->nullable();

            $table->string('dob')->nullable();
            $table->string('pob')->nullable();
            $table->string('address')->nullable();
            $table->string('father')->nullable();
            $table->string('father_job')->nullable();
            $table->string('mother')->nullable();
            $table->string('mother_job')->nullable();

            $table->string('city')->nullable();
            $table->string('city_slug')->nullable();

            $table->string('register_year')->nullable();

            $table->tinyInteger('is_active')->nullable();
            $table->string('profile')->nullable();
            $table->string('bio')->nullable();
            $table->tinyInteger('status')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
