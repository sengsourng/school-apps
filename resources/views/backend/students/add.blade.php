
@extends('layouts.admin')
@push('custom_css')
 <!-- dropzonejs -->
 {{-- <link rel="stylesheet" href="{{asset('themes')}}/dropzone/min/dropzone.min.css"> --}}
 <link rel="stylesheet" href="{{asset('dropzone/dist/min/basic.min.css')}}">

 <style>
    input[type="file"] {
   display: none;
}
</style>

@endpush
@section('content')

    <section class="content pt-2">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">{{trans('controls.students.add')}}</div>
                <div class="card-body">
                    <form action="{{ route('students.store') }}" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="row">
                            <div class="col-md-5 bg-info pt-3">

                                <div class="form-group row">
                                    <label for="lbl_code" class="col-sm-4 col-form-label">{{trans('controls.students.code')}}</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="code"  class="form-control" id="lbl_code" placeholder="{{trans('controls.students.code')}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                        <label for="lbl_name_kh" class="col-sm-4 col-form-label">{{trans('controls.students.name')}}</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="name_kh"  class="form-control" id="lbl_name_kh" placeholder="នាមត្រកូល នាមខ្លួន">
                                        </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lbl_name_en" class="col-sm-4 col-form-label">{{trans('controls.students.name_en')}}</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="name_en"  class="form-control" id="lbl_name_en" placeholder="FirstName LastName">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lbl_gender" class="col-sm-4 col-form-label">{{trans('controls.students.gender')}}</label>
                                    <div class="col-sm-8">
                                        <select class="custom-select" name="gender">
                                            <option value="">{{trans('controls.forms.Please Select')}}</option>
                                            <option  value="ប">{{trans('controls.forms.Male')}}</option>
                                            <option  value="ស">{{trans('controls.forms.Female')}}</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lbl_dob" class="col-sm-4 col-form-label">{{trans('controls.students.dob')}}</label>
                                    <div class="col-sm-8">
                                        <input type="date" name="dob"  class="form-control" id="lbl_dob" placeholder="Date of Birth">
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="lbl_pob" class="col-sm-4 col-form-label">{{trans('controls.students.pob')}}</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="pob"  class="form-control" id="lbl_pob" placeholder="ភូមិ ឃុំ​ ស្រុក ខេត្ត">
                                    </div>
                                </div>
                                {{-- address --}}
                                <div class="form-group row">
                                    <label for="lbl_address" class="col-sm-4 col-form-label">{{trans('controls.students.address')}}</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="address"  class="form-control" id="lbl_address" placeholder="ផ្ទះលេខ ក្រុម ភូមិ ឃុំ​ ស្រុក ខេត្ត">
                                    </div>
                                </div>

                            </div>


                            <div class="col-md-5 bg-success pt-3">
                                {{-- father --}}
                                <div class="form-group row">
                                    <label for="lable_father" class="col-sm-4 col-form-label">{{trans('controls.students.father')}}</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="father" class="form-control" id="lable_father" placeholder="នាមត្រកូល នាមខ្លួន">
                                    </div>
                                </div>

                                {{-- father_job --}}
                                <div class="form-group row">
                                    <label for="lbl_father_job" class="col-sm-4 col-form-label">{{trans('controls.students.father_job')}}</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="father_job" class="form-control" id="lbl_father_job" placeholder="មុខរបរឪពុក">
                                    </div>
                                </div>

                                {{-- mother --}}
                                <div class="form-group row">
                                    <label for="lable_mother" class="col-sm-4 col-form-label">{{trans('controls.students.mother')}}</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="mother" class="form-control" id="lable_mother" placeholder="ឈ្មោះម្តាយ">
                                    </div>
                                </div>

                                {{-- mother_job --}}
                                <div class="form-group row">
                                    <label for="lbl_mother_job" class="col-sm-4 col-form-label">{{trans('controls.students.mother_job')}}</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="mother_job" class="form-control" id="lbl_mother_job" placeholder="មុខរបរម្តាយ">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lbl_city" class="col-sm-4 col-form-label">{{trans('controls.students.city')}}</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="city" class="form-control" id="lbl_city" placeholder="ខេត្ត/ក្រុង">
                                    </div>
                                </div>

                                {{-- <div class="form-group row">
                                    <label for="lbl_register_year" class="col-sm-4 col-form-label">{{trans('controls.students.register_year')}}</label>
                                    <div class="col-sm-8">
                                        <input type="date" name="register_year" class="form-control" id="lbl_register_year" placeholder="ថ្ងៃចុះឈ្មោះ">
                                    </div>
                                </div> --}}

                                <div class="form-group row">
                                    <label for="lbl_is_active" class="col-sm-4 col-form-label">{{trans('controls.students.is_active')}}</label>
                                    <div class="col-sm-8">
                                        <select class="custom-select">
                                            <option>{{trans('controls.forms.Please Select')}}</option>
                                            <option  value="1">Active</option>
                                            <option  value="0">Disactive</option>
                                        </select>
                                    </div>
                                </div>

                                {{-- <div class="form-group row">
                                    <label for="lable_t_profile" class="col-sm-4 col-form-label">{{trans('controls.teachers.profile')}}</label>
                                    <div class="col-sm-8">
                                        <div class="custom-file">
                                            <input type="file">
                                        </div>
                                    </div>
                                </div> --}}

                            </div>

                            <div class="col-md-2 pt-3">

                                <div class="card" style="width: 100%;">
                                    {{-- <label for="lbl_city" class="col-sm-4 col-form-label" style="width: 100%;">{{trans('controls.students.profile')}}</label> --}}

                                    <div id="placehere" style="border-radius: 15px;" class="uploadPreview rounded mx-auto d-block mb-2" width="196"></div>
                                    <img style="width: 100%; border-radiuus:20px;" src="{{asset('images/students/default_student.jpg')}}" id="uploadPreview" onclick="removeFns()" class="uploadPreview rounded mx-auto d-block mb-2" width="192" />

                                    <div class="card-body">
                                      {{-- <h5 class="card-title">Card title</h5> --}}
                                      <div class="btn-group d-flex justify-content-center" role="group" aria-label="Basic example">
                                        <button type="button" class="btn btn-primary">
                                            <label>
                                                <i class="fas fa-paperclip"></i> {{trans('controls.forms.brow_image')}}
                                                <input  type="file" id="uploadImage" onchange="Preview_Image_Before_Upload('uploadImage', 'uploadPreview');" name="profile"/>
                                            </label>
                                        </button>
                                        <button type="button" class="btn btn-danger " onclick="removeFns()"><i class="fas fa-trash"></i> {{trans('controls.forms.remove')}}</button>
                                    </div>
                                    </div>
                                  </div>


                            </div>

                        </div>

                        <div class="row bg-secondary">

                            <div class="col-sm-3 mt-2">
                                <div class="form-group">
                                    <label class="control-label">{{trans('controls.levels.name')}}</label>
                                    <select name="level_id" class="form-control select2" onChange="getData(this.value);" required>
                                        <option value="">{{ trans('controls.levels.select') }}</option>
                                        {{ create_option('levels','id','name') }}
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-3 mt-2">
                                <div class="form-group">
                                    <label class="control-label">{{trans('controls.classes.name')}}</label>
                                    <select name="class_id" id="class_id" class="form-control select2" required>
                                        <option value="">{{ trans('controls.classes.select') }}</option>
                                        {{ create_option('classes','id','name') }}
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3 mt-2">
                                <div class="form-group">
                                    <label for="date">{{trans('controls.students.date')}}</label>
                                    <input type="date" id="date" class="form-control" name="date">
                                </div>
                            </div>
                            <div class="col-md-3 mt-2">
                                <div class="form-group">
                                    <label for="study_year">{{trans('controls.students.register_year')}}</label>

                                    <select name="register_year" id="study_year" class="form-control">
                                            <option value="{{getStudentYear('academic_years','year',1)}}">ជ្រើសរើស</option>
                                            {{ create_option('academic_years','year','name') }}


                                    </select>
                                </div>
                            </div>



                        </div>

                        <div class="float-right pt-2">
                            <button type="submit" class="btn btn-primary mr-3">{{trans('controls.save')}}</button>
                            {{-- <button type="submit" class="btn btn-danger">{{trans('controls.cancel')}}</button> --}}
                            <a href="{{route('students.index')}}" class="btn btn-danger">{{trans('controls.cancel')}}</a>
                        </div>
                    </form>
                </div>
            </div>

        </div><!-- /.container-fluid -->
      </section>

@endsection

@push('custom_js')

<!-- DataTables  & Plugins -->
<script src="{{asset('themes')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/jszip/jszip.min.js"></script>
<script src="{{asset('themes')}}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{asset('themes')}}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<!-- date-range-picker -->
<script src="{{asset('themes')}}/plugins/daterangepicker/daterangepicker.js"></script>

<script src="{{asset('dropzone/dist/dropzone.js')}}"></script>

<script>
    function Preview_Image_Before_Upload(fileinput_id, preview_id) {
            var oFReader = new FileReader();
            var fileArray = [];
            fileArray.push(document.getElementById(fileinput_id).files[0])
            fileArray.forEach(function(entry) {
                oFReader.readAsDataURL(fileArray[0]);
            });

            //console.log(fileArray)
            // oFReader.readAsDataURL(fileArray[0]);
            oFReader.onload = function(oFREvent) {
                if (window.FileReader && window.File && window.FileList && window.Blob) {

                var elem = document.getElementById("uploadPreview");
                elem.src = oFREvent.target.result;
                // document.getElementById("placehere").appendChild(elem);
                document.getElementById("uploadPreview").innerHTML=elem;
                }
            };
        };
        function removeFns(){
            //document.getElementById("uploadPreview").innerHTML=null;
            document.getElementById('uploadImage').value = ""
            document.getElementById('uploadPreview').src = "{{asset('images/students/default_student.jpg')}}";

        }

</script>

<script>

$('#class_id').attr('disabled', 'disabled');

function getData(val) {
    var _token=$('input[name=_token]').val();
    var level_id=$('select[name=level_id]').val();
    if(level_id !=""){
        $('#class_id').removeAttr('disabled');
    }else{
        $('#class_id').attr('disabled', 'disabled');
    }
    $.ajax({
        type: "POST",
        url: "{{url('admin/classes/get_classes')}}",
        data:{_token:_token,level_id:level_id},
        beforeSend: function(){
            $("#preloader").css("display","block");
        },success: function(classes){
            $("#preloader").css("display","none");
            $('select[name=class_id]').html(classes);
        }
    });
}





</script>



@endpush
