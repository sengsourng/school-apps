@extends('layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">{{trans('controls.contact-us')}}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('/')}}">{{trans('controls.home')}}</a></li>
            <li class="breadcrumb-item active">{{trans('controls.contact-us')}}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user-graduate"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">សិស្សសរុប</span>
              <span class="info-box-number">
                400
                <small>នាក់</small>
                ស្រី 230 <small>នាក់</small>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user-edit"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">សិស្សឆ្នាំថ្មី</span>
              <span class="info-box-number">
                100
                <small>នាក់</small>
                ស្រី 30 <small>នាក់</small>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box mb-3">
            <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-chalkboard-teacher"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">សិស្សអវត្តមាន</span>
              <span class="info-box-number">50 <small>នាក់</small>, ស្រី <small>នាក់</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->




    </div><!--/. container-fluid -->
  </section>
  <!-- /.content -->
@endsection
