<?php

namespace App\Http\Controllers\Admin;

use App\Models\Level;
use App\Models\Score;
use App\Models\Classes;
use App\Models\Section;
use App\Models\Student;
use App\Models\Attendance;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // dd($request->all());
        $attendance = [];
		$level_id = $request->level_id;
		$class_id = $request->class_id;
		$section_id = $request->section_id;
		$date = $request->date;
		if($class_id == "" || $section_id == "" || $date == ""||$level_id==""){
			return view('backend.attendances.home',compact('attendance','date','class_id','section_id','level_id'));
		}else{
			$class = Classes::find($class_id)->name;
			$section = Section::find($section_id)->name;
            $level=Level::find($level_id)->name;

            $section_id=Section::find($section_id)->id;
		// $section_id = $request->section_id;


			$attendance = Student::select('*','attendances.id AS attendance_id')
									->leftJoin('attendances',function($join) use ($date) {
										$join->on('attendances.student_id','=','students.id');
										$join->where('attendances.date','=',$date);
									})
									->join('student_infos','student_infos.student_id','=','students.id')
									// ->where('student_infos.session_id',get_option('academic_year'))
									->where('student_infos.class_id',$class_id)
									// ->where('student_infos.section_id',$section_id)
									->orderBy('student_infos.student_id', 'ASC')
									->get();

                                // dd($attendance);
			// return view('backend.attendance.student-attendance',compact('attendance','date','class','section','class_id','section_id'));
			return view('backend.attendances.home',compact('attendance','date','class','section','level','class_id','section_id','level_id'));
		}

        // $data['attendances']=Attendance::get();

        // return view('backend.attendances.home',$data);
    }


    public function student_attendance_save(Request $request)
    {
		// $section_id = $request->section_id;
        // dd($section_id);
        // dd($request->all());
		$validator = Validator::make($request->all(), [
            'attendance' => 'required',
        ]);
        if ($validator->fails()) {

			if($request->ajax()){
			    return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
			}else{
				return redirect('admin/attendance_students')
                            ->withErrors($validator)
                            ->withInput();
			}
        }

        for ($i=0; $i < count($request->student_id) ; $i++) {
			$temp = array();
			$temp['student_id'] = (int)$request->student_id[$i];
			// $temp['class_id'] = (int)$request->class_id[$i];
			// $temp['section_id'] = (int)$request->section_id[$i];
            // $temp['level_id'] = (int)$request->level_id[$i];


            $temp['level_id'] = (int)$request->level_id;
            $temp['class_id'] = (int)$request->class_id;
			$temp['section_id'] = (int)$request->section_id;

			$temp['date'] = $request->date;

            // dd($temp);

			// $studentAtt = SAttendance::firstOrNew($temp);
			$studentAtt = Attendance::firstOrNew($temp);
			$studentAtt->school_id = auth()->user()->school_id;
			$studentAtt->skill_id = 1;
			$studentAtt->level_id = $temp['level_id'];
			$studentAtt->student_id = $temp['student_id'];
			$studentAtt->class_id = $temp['class_id'];
			$studentAtt->section_id = $temp['section_id'];
			$studentAtt->date = $temp['date'];
			$studentAtt->attendance = isset($request->attendance[$i]) ? $request->attendance[$i][0] : 0;
			$studentAtt->reason =$request->reason[$i] ;// isset($request->reason[$i]) ? $request->reason[$i][0] : null;

			$studentAtt->academic_years_id = getStudentYear('academic_years','id',1);

			$studentAtt->month = Carbon::now()->format('m');//Carbon::createFromFormat('m/d/Y', $temp['date'])->format('Y');
			$studentAtt->year = Carbon::now()->format('Y');//Carbon::createFromFormat('m/d/Y', $temp['date'])->format('Y');

			$studentAtt->study_year = Carbon::now()->format('Y');//Carbon::createFromFormat('m/d/Y', $temp['date'])->format('Y');

			$studentAtt->save();
        }


		if(! $request->ajax()){
		   return redirect('/admin/attendance_students')->with('success',('Saved Sucessfully'));
        }else{
		   return response()->json(['result'=>'success','action'=>'store','message'=>('Saved Sucessfully')]);
		}


    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function student_atten_report(Request $request)
    {
        // dd($request->all());
        $student_score = [];
        // dd($student_score);
        $class_id = $request->class_id;
        $level_id = $request->level_id;
        $section_id = $request->section_id;
        $date = $request->date;
            // $student_scores=Score::where('year','=',2021)
            // ->where('section_id','=',$section_id)
            // ->where('class_id','=',$class_id)
            // ->groupBy('student_id')
            // // ->where('class_id','=',1)
            // ->get();
            if($class_id == "" || $section_id == "" || $date == ""||$level_id==""){
                return view('backend.reports.student_atten_report',compact('student_score','date','class_id','section_id','level_id'));
            }else{

                    $data['student_score']= Score::select('scores.*','student_id','students.code','students.dob','gender','sections.name as section_name',DB::raw('SUM(score) as total_score,AVG(score) as score_avg'))
                    ->join("students","students.id","=","scores.student_id")
                    ->join("sections","sections.id","=","scores.section_id")
                    // ->with('attendance')

                    ->where('month','=',date('m', strtotime($date)))
                    ->where('year','=',date('Y', strtotime($date)))

                    ->where('class_id','=',$class_id)
                    ->where('section_id','=',$section_id)
                    // ->where('class_id','=',1)
                //    ->where('attendance','=',2)
                //    ->where('gender','=','ស')
                    ->groupBy("students.id")
                    // ->orderBy("total_score")
                    ->orderBy("score_avg","DESC")
                    ->get();
                // dd($student_scores);
                // $response['data'] = $student_scores;
                // // return response()->json($response);

                // $data['student_score']=response()->json($response);


                    $data['level_id']=$level_id;
                    $data['class_id']=$class_id;
                    $data['section_id']=$section_id;
                    $data['date']=$date;

            // dd($data);

                return view('backend.reports.student_atten_report',$data);

    }
}

}
