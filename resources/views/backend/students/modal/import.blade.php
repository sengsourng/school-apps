<div class="container pt-1">
    <form action="{{ route('students.store') }}" method="post" enctype="multipart/form-data">
        @csrf

        <div class="row">
            <div class="col-md-6 bg-info pt-3">

                  <div class="form-group row">
                        <label for="lable_s_name" class="col-sm-3 col-form-label">{{trans('controls.schools.name')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="school_name"  class="form-control" id="lable_s_name" placeholder="បឋម.ស្វាយធំ">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="lable_s_phone" class="col-sm-3 col-form-label">{{trans('controls.schools.phone')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="school_phone"  class="form-control" id="lable_s_phone" placeholder="092771244">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lable_s_email" class="col-sm-3 col-form-label">{{trans('controls.schools.email')}}</label>
                        <div class="col-sm-9">
                            <input type="email" name="school_email"  class="form-control" id="lable_s_email" placeholder="yourschool@gmail.com">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lable_s_city" class="col-sm-3 col-form-label">{{trans('controls.schools.city')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="school_city"  class="form-control" id="lable_s_city" placeholder="Siem Reap">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lable_s_address" class="col-sm-3 col-form-label">{{trans('controls.schools.address')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="school_address"  class="form-control" id="lable_s_address" placeholder="ភូមិ ឃុំ ស្រុក">
                        </div>
                    </div>



            </div>


        </div>

    </form>
</div>
