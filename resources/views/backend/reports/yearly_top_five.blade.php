@extends('layouts.admin')

@push('custom_css')

<!-- DataTables -->
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

 <!-- daterange picker -->
 <link rel="stylesheet" href="{{asset('themes')}}/plugins/daterangepicker/daterangepicker.css">
@endpush


@section('content')

     <!--Section: Sales Performance KPIs-->
     <section class="content pt-2">
        <div class="card">
          <div class="card-header text-left py-3" style="height: 9vh;">
            <h5 class="mb-0 text-left">

                <strong>{{trans('controls.menus.result_by_student')}}</strong>

                <span >
                    <a href="{{route('schools.create')}}"
                    class="btn btn-primary btn-sm float-right"

                    data-toggle="modal-ajax"
                    {{-- data-toggle="modal" --}}
                    {{-- data-modal-size="modal-lg" --}}
                    data-modal-size="modal-xl"
                    {{-- modal-xl --}}
                    {{-- data-mdb-toggle="modal" --}}
                    {{-- data-target="#exampleModal" --}}
                    data-target="#openModal"
                    >
                    <i class="fas fa-plus-circle"></i> {{trans('controls.forms.Add')}}</a>
                </span>
            </h5>
          </div>


                <div class="card-body">
                    <div class="table-responsive">
                        <div class="div_tab c_42" style=" display:show">
                            <table style="width:1250px; height:843px;  background-image:url(http://weschool\/assets/uploads/academic_awards.png) !important" class="main_table">
                                <tbody><tr>
                                    <td style="padding:70px; padding-top:220px; vertical-align: top">
                                        <center>
                                            <table style="width:100%; height:100%; ">
                                                <tbody><tr>
                                                    <td style="font-size:16px; padding-left:20px ;padding-bottom:20px !important" colspan="2">លេខ : .........................</td>
                                                </tr>
                                                <tr>
                                                    <td style="width:50%; vertical-align:top !important">
                                                        <table class="khmer_side" style="width:100%; text-align:center">
                                                            <tbody><tr>
                                                                <td>
                                                                    <p>យោងតាមការប្រជុំរបស់ក្រុមប្រឹក្សាវាយតម្លៃ​នៃសាលាអន្តរជាតិអាប៊ែនឌែនឡៃ</p>
                                                                    <p><span>សាលាអន្តរជាតិអាប៊ែនឌែនឡៃ សូមសរសើរ</span></p>
                                                                    <p>
                                                                        សិស្សឈ្មោះ  <span style="font-size:16px !important">តាំង ហេងហេង</span>
                                                                        ភេទ <span>ប្រុស</span>
                                                                        ថ្ងែខែឆ្នាំកំណើត <span>០៦ មេសា ២០០៩</span></p>
                                                                    <p>
                                                                        ចំពោះការទទួលបានចំណាត់ថ្នាក់ <span>លេខ១​ ប្រចាំថ្នាក់ទី៥(ខ)</span>
                                                                    </p>
                                                                    <p>
                                                                        នៅក្នុង <span>ការប្រឡងឆមាសទី១ កម្មវិធីភាសាខ្មែរ</span>
                                                                    </p>
                                                                    <p>ឆ្នាំសិក្សា <span>២០១៩-២០២០</span></p>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                    <td style="width:50%; vertical-align:top">
                                                        <table class="english_side" style="width:100%; text-align:center">
                                                            <tbody><tr>
                                                                <td>
                                                                    <p>Upon the recommendation of the Evaluation Board of</p>
                                                                    <b><p>ABUNDANT LIFE INTERNATIONAL SCHOOL</p>
                                                                    <p>the Director</p>
                                                                    <p>CONGRATULATES AND CONFERS UPON</p></b>
                                                                    <p>
                                                                        <b style="font-size:16px !important">TANG HENGHENG</b>,
                                                                        Sex: <b>Male</b>,
                                                                        Date of birth: <b>April 06, 2009</b></p>
                                                                    <p>this Academic Award </p>
                                                                    <p>for having successfully obtained </p>
                                                                    <p><b>First Place</b> on his Fifth Grade B First&nbsp;Semester&nbsp; Examination,</p>
                                                                    <p>Cambodian Curriculum for the academic year <b>2019-2020</b></p>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size:16px !important; font-family:Khmer OS Siemreap !important;" colspan="2" class="text-center">
                                                        <p>ថ្ងៃ​ ព្រហស្បតិ៍ ១០​ កើត ខែ​ មាឃ ឆ្នាំច សំរិទ្ធិស័ក ព.ស ២៥៦២</p>
                                                        <p>រាជធានីភ្នំពេញ ថ្ងៃទី១៦ ខែមិថុនា ឆ្នាំ២០២១</p>
                                                        <span style="font-family:Times New Roman !important;">Phnom Penh, June 16, 2021</span>
                                                        <p><span style="font-family:Khmer OS Muol Light;">នាយិកា </span><span style="font-family:Times New Roman !important;">/ Director</span></p>
                                                    <!--	<p><span style="font-family:Khmer OS Muol Light;">នាយិការង  </span><span style="font-family:Times New Roman !important;">/ Deputy Director</span></p> -->
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        </center>
                                    </td>
                                </tr>
                            </tbody></table>
                        </div>
                    </div>
                </div>

            </div>
          </div>


        </div>
     </section>
      <!--Section: Sales Performance KPIs-->

{{-- @endif --}}


@endsection


@push('custom_js')

<!-- DataTables  & Plugins -->
<script src="{{asset('themes')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/jszip/jszip.min.js"></script>
<script src="{{asset('themes')}}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{asset('themes')}}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<!-- date-range-picker -->
<script src="{{asset('themes')}}/plugins/daterangepicker/daterangepicker.js"></script>

<script src="{{asset('dropzone/dist/dropzone.js')}}"></script>



<script>

    $(document).ready(function() {
           $('#DataTableList').DataTable({
            ordering:true,
            processing: false,
            // serverSide: true,
            "order": [[ 4, "asc" ]],
            language : $('html').attr('lang') != 'en' ? window.datatableI18n[$('html').attr('lang')] : null,
        });
    } );

        $(document).on('click',`[data-toggle="modal-ajax"]`,function(ev){
                    ev.preventDefault();
                    var url = $(this).attr('href');
                    var modalSize = $(this).data('modal-size');
                    var title = $(this).text();
                    // var target = $(this).data('mdb-target');
                    var target = $(this).data('target');


                    $.get(url).done((res)=>{
                        var $modal = $(target);
                        $modal.find('.modal-title').text(title);
                        $modal.find('.modal-dialog').addClass(modalSize);
                        $modal.find('.modal-body').html(res);

                      //  $.getScript(`${location.origin }/themes/js/mdb.min.js`).done();

                        $modal.modal('show');

                        $modal.find('button#btn-submit').unbind().click(function(ev){
                            ev.preventDefault();

                            var $form = $modal.find('form');
                            var formData = new FormData($form[0]);
                            $.ajax({
                                url : $form.attr('action'),
                                method : 'post',
                                data : formData,
                                processData : false,
                                contentType : false,
                                success : (res)=>{
                                    if(res.status){

                                        Swal.fire({
                                            position: 'top-end',
                                            icon: 'success',
                                            title: '{{__('controls.Your data has been saved or updated!')}}',
                                            showConfirmButton: false,
                                            timer: 1500
                                        });


                                        if($(this).data('allow-close')){
                                            $modal.find(`[data-dismiss="modal"]`).click();
                                        }
                                        if ( url.match('edit')) {
                                            $('#DataTableList').find(`tr[data-id="${res.data.id}"]`).html($(res.dom).html());
                                        }else{
                                            $('#DataTableList').find('tbody').prepend(res.dom);
                                        }


                                        $form[0].reset();
                                    }

                                },
                            })

                        });
                        $modal.find(`[data-dismiss="modal"]`).click(function(ev){
                            ev.preventDefault();
                            $modal.modal('hide');
                            $modal.find('.modal-body').html("");

                        });


                    });

                });

</script>


<script type="text/javascript">
    $(document).on('submit',`[data-toggle="formDelete"]`,function(ev){
        ev.preventDefault();
        $.ajax({
            url : $(this).attr('action'),
            method : 'post',
            data : new FormData(ev.target),
            processData : false,
            contentType : false,
            success : (res)=>{
                if(res.status){
                    Swal.fire(
                        '{{trans('controls.delete')}}!',
                        '{{trans('controls.Your file has been deleted.')}}',
                        'success'
                    );
                    $(this).parents("tr").remove();
                }else{
                    Swal.fire(
                        '{{trans('controls.delete')}}!',
                        '{{trans('controls.delete_message_can_not')}}',
                        'error'
                    )
                }
            }

        })
    });

    function deleteItem(id){
      Swal.fire({
        title: "{{trans('controls.Are you sure?')}}",
        text: "{{trans('controls.You will not be able to delete this!')}}",
        icon: 'warning',
        showCancelButton: true,

        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText :"{{trans('controls.cancel')}}",
        confirmButtonText:  "{{trans('controls.Yes, delete it!')}}"
      }).then((result) => {
        if (result.value) {
          $('#frmDeleteItem-'+id).submit();

        }
      })
    }
</script>

@endpush
