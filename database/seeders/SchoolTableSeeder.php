<?php

namespace Database\Seeders;

use App\Models\School;
use Illuminate\Database\Seeder;

class SchoolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        School::create([
        	'name' => 'បឋម.ស្វាយធំ',
            'phone' => '099887766',
            'email' => 'svaythom@gmail.com',
        	'city' => 'Siem Reap',
            'city_slug' => 'siem-reap',
            'address' => 'ស្វាយធំ ក្រុងសៀមរាប សៀមរាប',
            'director_name' => 'សេង ស៊ង់',
            'user_id' => 2,
        ]);
    }
}
