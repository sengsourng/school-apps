@extends('layouts.admin')
@push('title','Student Score Monthly')

@push('custom_css')

<!-- DataTables -->
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

 <!-- daterange picker -->
 <link rel="stylesheet" href="{{asset('themes')}}/plugins/daterangepicker/daterangepicker.css">


    <style>
            body {
                 background: rgb(204,204,204);
            }
            page[size="A4"] {
                    background: white;
                    /* width: 21cm; */
                    width: 100% !important;
                    /* height: 29.7cm; */
                    height: auto !important;
                    /* display: block; */
                    margin: 0 auto;
                    /* margin-bottom: 0.5cm; */
                    /* box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
                    border: 1.25cm solid #000; */
            }
            @media print {
                body, page[size="A4"] {
                    margin-top: 0 !important;
                    box-shadow: 0;
                }

                @page{
                    /* size:12in 8.5in  !important; */
                    size: A4 landscape;
                    /* size: A4 portrait; */
                    /* size: portrait; */
                    page-break-after: auto !important;
                }
                .resumeCanvas{
                    height: 100%;
                    width: 100%;
                    position: fixed;
                    top: -70px !important;
                    left: 0;
                    margin: 0,auto;
                    padding: 0;
                    /* line-break: 18px; */
                }
                div.portrait, div.landscape {
                    margin: 0;
                    padding: 0;
                    border: none;
                    background: none;
                }
                div.landscape {
                    transform: rotate(270deg) translate(-276mm, 0);
                    transform-origin: 0 0;
                }

                .text-color{
                    color: black !important;
                    font-size: 22px !important;
                }
                .page_break{
                    /* page-break-after: auto !important;
                     */
                     page-break-inside:avoid;
                     page-break-after:auto
                }


                .noPrint{
                    visibility: hidden;
                }
            }
    </style>

<style>
    input:focus, textarea:focus, select:focus{
        outline: none;
        border: none;
    }
    .input_outline{
        outline: none;
        border: none;
    }
</style>

@endpush

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0">{{trans('controls.student_atten_report')}}  </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('/')}}">{{trans('controls.home')}}</a></li>
            <li class="breadcrumb-item active">{{trans('controls.student_atten_report')}}</li>
        </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <form action="{{ route('admin.student_attendance.student_atten_report')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="container-fluid">
        {{-- Score List --}}
        <div class="noPrint">
            <div class="row">
                    <div class="col-md-12">
                        <div class="card card-outline card-success">
                          <div class="card-header">
                            @php
                                $level_id=$level_id;
                                $class_id=$class_id;
                                $section_id=$section_id;
                                $date=$date;
                            @endphp
                            {{-- <form id="search_form" class="params-panel validate" action="{{ route('exams.create')}}" method="post" autocomplete="off" accept-charset="utf-8"> --}}
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">{{trans('controls.levels.name')}}</label>
                                            <select name="level_id" class="form-control select2" onChange="getData(this.value);" required>
                                                <option value="">{{ trans('controls.levels.select') }}</option>
                                                {{ create_option('levels','id','name',$level_id) }}
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">{{trans('controls.classes.name')}}</label>
                                            <select name="class_id" id="class_id" class="form-control select2" required>
                                                <option value="">{{ trans('controls.classes.select') }}</option>
                                                {{ create_option('classes','id','name',$class_id) }}
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label">{{trans('controls.sections.name')}}</label>
                                            <select name="section_id" class="form-control select2" required>
                                                <option value="">{{ trans('controls.forms.Please Select') }}</option>
                                                {{ create_option('sections','id','name',$section_id) }}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="date" class="control-label">{{trans('controls.students.date')}}</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <input type="date" class="form-control datepicker" name="date" value="{{ $date }}" required>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="col-sm-2 pt-4">
                                        <div class="form-group pt-2">
                                            <button type="submit" class="btn btn-primary btn-block rect-btn">{{trans('controls.forms.filter')}}</button>
                                        </div>
                                    </div>

                                </div>

                            {{-- </form> --}}
                          </div>
                        </div>
                </div>


            </div>
        </div>

        @php
            use KhmerDateTime\KhmerDateTime;
                if ($date==null) {
                    $date = Carbon\Carbon::now();// will get you the current date, time
                }else{
                    $date=Carbon\Carbon::createFromFormat('Y-m-d', $date);
                }
                $dateTime = KhmerDateTime::parse($date->format("Y-m-d"));

                    $dateTime->day(); // ២២
                    $dateTime->fullDay(); // ពុធ
                    $dateTime->month(); // ០៥
                    $dateTime->fullMonth(); // ឧសភា
                    $dateTime->year(); // ២០១៩
                    $dateTime->minute(); // ០០
                    $dateTime->hour(); // ០០
                    $dateTime->meridiem(); // ព្រឹក
                    $dateTime->week(); // ៤
                    $dateTime->fullWeek(); // សប្តាហ៍ទី៤
                    $dateTime->weekOfYear(); // ២១
                    $dateTime->fullWeekOfYear(); // សប្តាហ៍ទី២១
                    $dateTime->quarter(); // ២
                    $dateTime->fullQuarter(); // ត្រីមាសទី២

        @endphp
    </form>
        @if (!empty($student_score))
    <form action="{{ route('admin.generateMonltyScoreReportSave')}}" method="post" enctype="multipart/form-data">
        @csrf

        {{-- @if ($student_score->count()>0) --}}
        {{-- <page size="A4"> --}}
                <div class="resumeCanvas">
                    <div class="row">
                        <div class="col-md-12">
                            <table>
                                <tbody>




                                <tr>
                                    <td class="text-center" style="width: 400px;">
                                        <br><br>
                                    <h3 style="font-size:20px; font-family:Khmer OS Muol Light !important ; font-weight: normal !important;">ក្រសួងអប់រំយុវជន និង​កីឡា</h3>
                                    <h3>{{getSchool(Auth()->user()->school_id)}}</h3>

                                    </td>
                                    {{-- <td style="width: 800px;"></td>
                                    <td style="width: 800px;"></td> --}}
                                    <td style="width: 700px;"></td>
                                    {{-- <td style="width: 800px;"></td> --}}
                                    <td class="text-center" style="width: 400px;">
                                        <h3 style="font-size:20px; font-family:Khmer OS Muol Light !important ; font-weight: normal !important;">ព្រះរាជាណាចក្រកម្ពុជា</h3>
                                        <h3 style="font-size:20px; font-family:Khmer OS Muol Light !important ; font-weight: normal !important;">ជាតិ សាសនា ព្រះមហាក្សត្រ</h3>
                                    </td>
                                    {{-- <td rowspan="4" style="width:2480px;"><img style="width:100%;" src="{{asset('images/logo_banner.png')}}"></td> --}}
                                </tr>

                                </tbody>
                            </table>
                            <div class="row page_break">
                                    <div class="col-md-12 text-center">


                                            <h3 style="color:#0e13b0 !important; font-size:20px; font-family:Khmer OS Muol Light !important ; font-weight: normal !important;">តារាងពិន្ទុ និង​វត្តមានសិស្ស​</h3>
                                            <p style="display:inline !important; color:#0e13b0 !important; font-size:18px; font-family:Khmer OS Muol Light !important; font-weight: normal !important;">ប្រចាំខែ {{ $dateTime->fullMonth()}} </p>
                                            <p style="display:inline !important; color:#f23838 !important; font-size:18px; font-family:Khmer OS Muol Light !important ; font-weight: normal !important;">{{getName('classes','name',$class_id)}}</p><p></p>


                                </div>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="card card-outline card-info">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        {{-- <div style='break-after:always'></div> --}}
                                            <table class="table table-hover table table-bordered">
                                                <tr>
                                                    <td colspan="4" class="text-center text-color bg-success">ពត៌មានសិស្ស</td>
                                                    <td colspan="4" class="text-center text-color bg-primary">ពិន្ទុសិស្ស</td>
                                                    <td colspan="4" class="text-center text-color bg-danger">វត្តមាន​សិស្ស</td>

                                                </tr>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col" style="width:300xp;">
                                                        {{trans('controls.students.name')}}
                                                    </th>
                                                    <th scope="col" style="width:300xp;">
                                                        {{trans('controls.students.code')}}
                                                    </th>
                                                    <th scope="col">
                                                        {{trans('controls.students.gender')}}
                                                    </th>
                                                    @php
                                                            $score_subject = App\Models\Score::select('scores.*','student_id')
                                                            ->join("students","students.id","=","scores.student_id")
                                                            // ->with('attendance')
                                                            // ->where('student_id','=',$item->student_id)
                                                            // ->where('month','=',$date->format("m"))
                                                            // ->where('year','=',$date->format("Y"))
                                                            //    ->where('attendance','=',2)
                                                            ->where('class_id','=',$class_id)
                                                            ->groupBy("scores.subject_id")
                                                            ->get();
                                                    @endphp
                                                        @foreach ($score_subject as $item)
                                                            {{-- <th>
                                                                {{$item->subject_name}}
                                                            </th> --}}
                                                        @endforeach
                                                    <th scope="col">ពិន្ទុសរុប</th>
                                                    <th scope="col">ម.ភាគ</th>
                                                    <th scope="col">ចំ.ថ្នាក់</th>
                                                    <th scope="col">និទ្ទេស</th>


                                                    <th scope="col">{{trans('controls.Present')}}</th>
                                                    <th scope="col">{{trans('controls.Absent')}}</th>
                                                    <th scope="col">{{trans('controls.Late')}}</th>
                                                </tr>
                                                <tbody class="page_break">
                                                    @php
                                                        $score_rank=array();
                                                    @endphp

                                                    @foreach ($student_score as $item)
                                                        @php
                                                            $score_rank[]=$item->total_score;
                                                        @endphp
                                                    @endforeach
                                                    {{-- {{$score_rank}} --}}
                                                    @php
                                                    rsort($score_rank);

                                                        $arrlength = count($score_rank);
                                                        $rank = 1;
                                                        $prev_rank = $rank;


                                                    @endphp

                                                    @php
                                                        // $rank=1;
                                                        // $prev_rank = $rank;
                                                        $total_student=0;
                                                        $total_student_f=0;
                                                    @endphp

                                                @foreach ($student_score as $key => $item)
                                                        @php
                                                            $total_student=$student_score->count();

                                                            if (getName2('students','gender',$item->student_id)==='ស') {
                                                                $total_student_f=$total_student_f+1;
                                                            }
                                                        @endphp
                                                        <tr class="peak_break">
                                                            <th scope="row">
                                                                {{-- {{$item->student_id}} --}}
                                                                {{$key+1}}
                                                                <input type="hidden" name="student_id[]" value="{{$item->student_id}}">
                                                            </th>
                                                            <td>
                                                                {{$item->student_name}}
                                                            </td>
                                                            <td>
                                                                {{$item->code}}
                                                            </td>
                                                            <td>
                                                                {{getName('students','gender',$item->student_id)}}
                                                            </td>
                                                            @php
                                                                    $score_subject = App\Models\Score::select('scores.*','student_id')
                                                                    ->join("students","students.id","=","scores.student_id")
                                                                    ->join("sections","sections.id","=","scores.section_id")
                                                                    // ->with('attendance')
                                                                    ->where('student_id','=',$item->student_id)
                                                                    ->where('month','=',$date->format("m"))
                                                                    ->where('year','=',$date->format("Y"))
                                                                    //    ->where('attendance','=',2)
                                                                    //    ->where('gender','=','ស')
                                                                    // ->groupBy("scores.class_id")
                                                                    ->get();
                                                            @endphp
                                                                @foreach ($score_subject as $sc_subj)

                                                                @endforeach
                                                                <td class="text-blue">
                                                                    {{-- {{number_format(($sc_subj->total_score), 2, '.', '')}} --}}
                                                                    <input class="text-blue input_outline" style="width: 60px;" type="number" name="total_score[]" value="{{number_format(($sc_subj->total_score), 2, '.', '')}}">
                                                                </td>
                                                                <td class="text-blue">
                                                                    {{-- {{number_format(($sc_subj->total_score/$score_subject->count()), 2, '.', '')}} --}}
                                                                    <input class="text-blue input_outline" style="width: 60px;" type="number" name="score_avg[]" value="{{number_format(($sc_subj->total_score/$score_subject->count()), 2, '.', '')}}">

                                                                </td>
                                                            <td class="text-red">
                                                                <?php
                                                                $result_rank=0;

                                                                        if ($key==0) {
                                                                            // echo $score_rank[$key]."- Rank".($rank);
                                                                            $result_rank=$rank;
                                                                            // echo $rank;
                                                                            ?>
                                                                                <input class="text-red input_outline" style="width: 60px;" type="number" name="rank[]" value="{{$rank}}">
                                                                            <?php


                                                                        }
                                                                        elseif ($score_rank[$key] != $score_rank[$key-1]) {
                                                                            $rank++;
                                                                            $prev_rank = $rank;
                                                                            // echo $score_rank[$key]."- Rank".($rank);

                                                                            // $result_rank=$rank;
                                                                            // echo $rank;
                                                                            ?>
                                                                                <input class="text-red input_outline" style="width: 60px;" type="number" name="rank[]" value="{{$rank}}">
                                                                            <?php

                                                                        }
                                                                        else{
                                                                            $rank++;
                                                                            // echo $score_rank[$key]."- Rank".($prev_rank);
                                                                            // $result_rank=$prev_rank;
                                                                            // echo $prev_rank;
                                                                            ?>
                                                                                <input class="text-red input_outline" style="width: 60px;" type="number" name="rank[]" value="{{$prev_rank}}">
                                                                            <?php
                                                                        }
                                                                        // echo "<br>";
                                                               ?>


                                                            </td>
                                                            <td>
                                                                {{-- {{checkCredit(number_format(($sc_subj->total_score/$score_subject->count()), 2, '.', ''))}} --}}
                                                                <input class="text-{{($sc_subj->total_score/$score_subject->count())<=5?'danger':'green'}} input_outline" style="width: 60px;" type="text" name="credit[]" value="{{checkCredit(number_format(($sc_subj->total_score/$score_subject->count()), 2, '.', ''))}}">
                                                            </td>
                                                            <td class="text-center text-primary">{{student_attend_count('1',$date->format("Y"),$date->format("m"),$item->student_id)}} <small>{{trans('controls.att_times')}}</small></td>
                                                            <td class="text-center text-danger">{{student_attend_count('2',$date->format("Y"),$date->format("m"),$item->student_id)}} <small>{{trans('controls.att_times')}}</small></td>
                                                            <td class="text-center text-default">{{student_attend_count('3',$date->format("Y"),$date->format("m"),$item->student_id)}} <small>{{trans('controls.att_times')}}</small></td>
                                                        </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                             {{-- Footer List --}}
                                             <table>
                                                <tbody>
                                                <tr>
                                                    <td class="text-center" style="width: 400px;">
                                                        <p>សិស្សសរុប: {{$total_student}} នាក់ <span>ស្រី : {{$total_student_f}} នាក់</span></p>
                                                    <h3 style="font-size:16px; font-family:Khmer OS Muol Light !important ; font-weight: normal !important;">បានឃើញ និង​ឯកភាព​</h3>
                                                    <h3 style="font-size:16px; font-family:Khmer OS Battambang !important ; font-weight: normal !important;"><input  class="text-center form-control border-0"type="text" value="នាយក/នាយិកា"></h3>
                                                    </td>
                                                    {{-- <td style="width: 800px;"></td>
                                                    <td style="width: 800px;"></td> --}}
                                                    <td style="width: 500px;"></td>
                                                    {{-- <td style="width: 800px;"></td> --}}
                                                    <td class="text-center" style="width: 600px;">
                                                        <h3 style="font-size:16px; font-family:Khmer OS Battambang !important ; font-weight: normal !important;"><input  class="text-center form-control border-0" type="text" value="សៀមរាប ថ្ងៃ................ខែ..........ឆ្នាំ...........ព.ស ២៥៦.."></h3>
                                                        <h3 style="font-size:16px; font-family:Khmer OS Battambang !important ; font-weight: normal !important;"><input class="text-center form-control border-0" type="text" value="ត្រូវនឹង ថ្ងៃទី.......ខែ............ឆ្នាំ​២០...."></h3>
                                                        <h3 style="font-size:16px; font-family:Khmer OS Battambang !important ; font-weight: normal !important;">ហត្ថលេខា គ្រូបន្ទុកថ្នាក់</h3>
                                                    </td>
                                                    {{-- <td rowspan="4" style="width:2480px;"><img style="width:100%;" src="{{asset('images/logo_banner.png')}}"></td> --}}
                                                </tr>

                                                </tbody>
                                            </table>

                                            {{-- <div class="col-sm-2 pt-4 noPrint">
                                                <div class="form-group pt-2">
                                                    <button type="submit" class="btn btn-success btn-block rect-btn">{{trans('controls.forms.print')}}</button>
                                                </div>
                                            </div> --}}

                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
        {{-- </page> --}}


        @endif

        </div><!--/. container-fluid -->

    </form>
    {{-- End Form --}}
</section>
<!-- /.content -->


@endsection


@push('custom_js')

<!-- DataTables  & Plugins -->
<script src="{{asset('themes')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/jszip/jszip.min.js"></script>
<script src="{{asset('themes')}}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{asset('themes')}}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<!-- date-range-picker -->
<script src="{{asset('themes')}}/plugins/daterangepicker/daterangepicker.js"></script>

<script src="{{asset('dropzone/dist/dropzone.js')}}"></script>



<script type="text/javascript">
    // getData('');

    // $('#class_id').attr('disabled', 'disabled');

	function getData(val) {
		var _token=$('input[name=_token]').val();
		var level_id=$('select[name=level_id]').val();
        if(level_id !=""){
            $('#class_id').removeAttr('disabled');
        }else{
            $('#class_id').attr('disabled', 'disabled');
        }
		$.ajax({
			type: "POST",
			url: "{{url('admin/classes/get_classes')}}",
			data:{_token:_token,level_id:level_id},
			beforeSend: function(){
				$("#preloader").css("display","block");
			},success: function(classes){
				$("#preloader").css("display","none");
				$('select[name=class_id]').html(classes);
			}
		});
	}


</script>

@endpush


