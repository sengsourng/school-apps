<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScoreYearliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('score_yearlies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->string('school_name')->nullable();
            $table->unsignedBigInteger('skill_id')->references('id')->on('skills')->onDelete('cascade');
            $table->string('skill_name')->nullable();
            $table->unsignedBigInteger('exam_id')->references('id')->on('exams')->onDelete('cascade');
            $table->string('exam_name')->nullable();
            $table->unsignedBigInteger('level_id')->references('id')->on('levels')->onDelete('cascade');
            $table->string('level_name')->nullable();
            $table->unsignedBigInteger('class_id')->references('id')->on('classes')->onDelete('cascade');
            $table->string('class_name')->nullable();
            $table->unsignedBigInteger('subject_id')->references('id')->on('subjects')->onDelete('cascade');
            $table->string('subject_name')->nullable();

            $table->integer('rank')->nullable();
            $table->unsignedDecimal('average')->nullable();
            // total_score
            $table->unsignedDecimal('total_score')->nullable();

            $table->string('study_year')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('score_yearlies');
    }
}
