<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendanceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('school_id')->references('id')->on('students')->onDelete('cascade');
            $table->unsignedBigInteger('class_id')->references('id')->on('classes')->onDelete('cascade');
            $table->unsignedBigInteger('attendance_id')->references('id')->on('students')->onDelete('cascade');
            $table->unsignedBigInteger('student_id')->references('id')->on('students')->onDelete('cascade');

            $table->integer('absent')->default('0');
            $table->integer('permission')->default('0');
            $table->integer('late')->default('0');
            $table->integer('no_homework')->default('0');
            $table->integer('breaking_rule')->default('0');

            $table->string('study_year')->nullable();
            $table->string('study_month')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_items');
    }
}
