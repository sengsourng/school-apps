<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/')}}" class="brand-link">
      <img src="{{asset('images/logo.png')}}" alt="School Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">{{ config('app.name', 'ប្រព័ន្ធគ្រប់គ្រង') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu  -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

            <li class="nav-item">
                <a href="{{url('/')}}" class="nav-link {{request()->segment(1)==''?'active':''}}">
                    <i class="nav-icon fas fa-home"></i>
                    <p>
                      {{trans('controls.home')}}
                    </p>
                  </a>

              </li>

          <li class="nav-item">
            <a href="{{url('student-top5/all')}}" class="nav-link {{request()->segment(1)=='student-top5'?'active':''}}">
            <i class="fas fa-user-graduate nav-icon"></i>
              <p>
                {{trans('controls.top five students')}}
                {{-- <span class="right badge badge-success">15 <small>នាក់</small></span> --}}
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('student.absent')}}" class="nav-link {{request()->segment(1)=='student-absent'?'active':''}}">
                <i class="fas fa-clipboard-check nav-icon"></i>
                <p>
                  {{trans('controls.student absent')}}
                {{-- <span class="right badge badge-danger">13 <small>នាក់</small></span> --}}

                </p>
              </a>
          </li>

          {{-- <li class="nav-header">{{trans('controls.teachers.all')}}</li> --}}

          <li class="nav-item">
            <a href="{{route('teachers')}}" class="nav-link {{request()->segment(1)=='teachers'?'active':''}}">

                <i class="fas fa-chalkboard-teacher nav-icon"></i>
              <p>
                {{trans('controls.teachers.all')}}
                {{-- <span class="right badge badge-info">50​ នាក់</span> --}}
              </p>
            </a>

          </li>
          <li class="nav-item">
            {{-- {{route('result.exam')}} --}}
            <a href="{{url('result-exam/all')}}" class="nav-link {{request()->segment(1)=='result-exam'?'active':''}}">
                <i class="nav-icon fas fa-th"></i>
                <p>
                  {{trans('controls.result exam')}}
                  {{-- <span class="right badge badge-danger">New</span> --}}
                </p>
              </a>
          </li>

          <li class="nav-item">
            <a href="#" class="nav-link">
              {{-- <i class="nav-icon fas fa-tachometer-alt"></i> --}}
              <i class="nav-icon fas fa-cog"></i>
              <p>
                បង្កើតគណនីសិស្ស
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/')}}" class="nav-link">
                  {{-- <i class="far fa-circle nav-icon"></i> --}}
                  <i class="fas fa-school nav-icon"></i>
                  <p>ចុះឈ្មោះសិស្ស</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('/')}}" class="nav-link">
                  {{-- <i class="far fa-circle nav-icon"></i> --}}
                  <i class="fas fa-calendar-day nav-icon"></i>
                  <p>សិស្សចូលប្រើ</p>
                </a>
              </li>

            </ul>
          </li>





        </ul>
      </nav>
      <!-- /.sidebar-menu  -->
    </div>
    <!-- /.sidebar -->
  </aside>
