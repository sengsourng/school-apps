@extends('layouts.admin')

@push('custom_css')
<style>
    * {box-sizing: border-box}
    /* body {font-family: Verdana, sans-serif; margin:0} */
    .mySlides {display: none}
    img {vertical-align: middle;}

    /* Slideshow container */
    .slideshow-container {
      max-width: 100%;
      position: relative;
      margin: auto;
    }

    /* Next & previous buttons */
    .prev, .next {
      cursor: pointer;
      position: absolute;
      top: 50%;
      width: auto;
      padding: 16px;
      margin-top: -22px;
      color: white;
      font-weight: bold;
      font-size: 18px;
      transition: 0.6s ease;
      border-radius: 0 3px 3px 0;
      user-select: none;
    }

    /* Position the "next button" to the right */
    .next {
      right: 0;
      border-radius: 3px 0 0 3px;
    }

    /* On hover, add a black background color with a little bit see-through */
    .prev:hover, .next:hover {
      background-color: rgba(0,0,0,0.8);
    }

    /* Caption text */
    .text {
      color: #f2f2f2;
      font-size: 15px;
      padding: 8px 12px;
      position: absolute;
      bottom: 8px;
      width: 100%;
      text-align: center;
    }

    /* Number text (1/3 etc) */
    .numbertext {
      color: #f2f2f2;
      font-size: 12px;
      padding: 8px 12px;
      position: absolute;
      top: 0;
    }

    /* The dots/bullets/indicators */
    .dot {
      cursor: pointer;
      height: 15px;
      width: 15px;
      margin: 0 2px;
      background-color: #bbb;
      border-radius: 50%;
      display: inline-block;
      transition: background-color 0.6s ease;
    }

    .active, .dot:hover {
      background-color: #717171;
    }

    /* Fading animation */
    .fade {
      -webkit-animation-name: fade;
      -webkit-animation-duration: 1.5s;
      animation-name: fade;
      animation-duration: 1.5s;
    }

    @-webkit-keyframes fade {
      from {opacity: .4}
      to {opacity: 1}
    }

    @keyframes fade {
      from {opacity: .4}
      to {opacity: 1}
    }

    /* On smaller screens, decrease text size */
    @media only screen and (max-width: 300px) {
      .prev, .next,.text {font-size: 11px}
    }




    marquee {
	margin-top: 5px;
	width: 100%;
}

.runtext-container {
background-color:#ddddff;
*background-color:#ccf;
background-image:-moz-linear-gradient(top,#ccf,#fff);
background-image:-webkit-gradient(linear,0 0,0 100%,from(#ccf),to(#fff));
background-image:-webkit-linear-gradient(top,#ccf,#fff);
background-image:-o-linear-gradient(top,#ccf,#fff);
background-image:linear-gradient(to bottom,#ccf,#fff);
background-repeat:repeat-x;
	border: 4px solid #000000;
	box-shadow:0 5px 20px rgba(0, 0, 0, 0.9);

/* width: 850px; */
width: 100%;
max-height: 60px;
overflow-x: hidden;
overflow-y: visible;
/* margin: 0 60px 0 30px; */
padding:0 3px 0 3px;
}

.main-runtext {margin: 0 auto;
    overflow: visible;
    position: relative;
    /* height: 40px; */
}

.runtext-container .holder {
position: relative;
overflow: visible;
display:inline;
float:left;

}

.runtext-container .holder .text-container {
	display:inline;
}

.runtext-container .holder a{
	text-decoration: none;
	font-weight: bold;
	color:#ff0000;
	text-shadow:0 -1px 0 rgba(0,0,0,0.25);
	line-height: -0.5em;
	font-size:16px;
}

.runtext-container .holder a:hover{
	text-decoration: none;
	color:#6600ff;
}
    </style>
@endpush
@section('content')
<div class="pt-2">
    {{-- <h2 class="text-center">សូមស្វាគមន៍ មកកាន់​</h2> --}}
{{-- <h3 class="text-center">សាលាបឋមសិក្សាស្វាយធំ សូមស្វាគមន៍</h3> --}}
@include('running_text')
</div>
<div class="slideshow-container">
    <div class="mySlides fade">
      <div class="numbertext">1 / 3</div>
      <img src="{{asset('images/slides/slider-bg1.jpg')}}" style="width:100%">
      {{-- <div class="text pt-5">Caption Text</div> --}}
    </div>

    <div class="mySlides fade">
      <div class="numbertext">2 / 3</div>
      <img src="{{asset('images/slides/slider-bg2.jpg')}}" style="width:100%">
      {{-- <div class="text">Caption Two</div> --}}
    </div>

    <div class="mySlides fade">
      <div class="numbertext">3 / 3</div>
      <img src="{{asset('images/slides/slider-bg3.jpg')}}" style="width:100%">
      {{-- <div class="text">Caption Three</div> --}}
    </div>

    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    </div>
    <br>

    <div style="text-align:center">
      <span class="dot" onclick="currentSlide(1)"></span>
      <span class="dot" onclick="currentSlide(2)"></span>
      <span class="dot" onclick="currentSlide(3)"></span>
    </div>

@endsection
@push('custom_js')

<script>
    var slideIndex = 0;
    showSlides();

    function showSlides() {
      var i;
      var slides = document.getElementsByClassName("mySlides");
      var dots = document.getElementsByClassName("dot");
      for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
      }
      slideIndex++;
      if (slideIndex > slides.length) {slideIndex = 1}
      for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
      }
      slides[slideIndex-1].style.display = "block";
      dots[slideIndex-1].className += " active";
      setTimeout(showSlides, 3000); // Change image every 2 seconds
    }
    </script>


@endpush
