<div class="container pt-1">
    <form action="{{ route('skills.store') }}" method="post" enctype="multipart/form-data">
        @csrf

        <div class="row">
            <div class="col-md-12  pt-3">
                    <div class="form-group row">
                        <label for="lable_s_name" class="col-sm-3 col-form-label">{{trans('controls.skills.name')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="name"  class="form-control" id="lable_s_name" placeholder="កម្មវិធីសិក្សាភាសាខ្មែរ">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lbl_description" class="col-sm-3 col-form-label">{{trans('controls.skills.description')}}</label>
                        <div class="col-sm-9">
                            <input type="text" name="description"  class="form-control" id="lbl_description" placeholder="Khmer Program">
                        </div>
                    </div>

            </div>



        </div>

    </form>
</div>
