<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_families', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('student_id')->references('id')->on('students')->onDelete('cascade');
            $table->unsignedBigInteger('relation_id')->references('id')->on('relations')->onDelete('cascade');
            $table->string('fname')->nullable();
            $table->string('lname')->nullable();
            $table->string('name_en')->nullable();
            $table->string('occupation')->nullable();
            $table->date('dob')->nullable();
            $table->string('telephone')->nullable();
            $table->text('pob')->nullable();
            $table->text('address')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_families');
    }
}
