<tr data-id="{{ $row->id}}">
    <th scope="row" style="width: 40px !important;">{{$row->id}}</th>
    <td>
        <ul class="products-list product-list-in-card pl-2 pr-2">
            <li class="item">
              <div class="product-img">

                @if ($row->profile==="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50")
                        <img src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50" alt="" class="rounded  img-size-50">

                @elseif ($row->profile==NULL)
                    <img src="{{asset('images/teachers/default_teacher.jpg')}}" alt="" class="rounded  img-size-50">
                @else
                    <img src="{{asset('images/teachers/'.$row->profile)}}" class="rounded  img-size-50" />
                @endif

            </div>
              <div class="product-info">
                <a href="javascript:void(0)" class="product-title">
                    {{$row->name_kh}}

                  <span class="badge badge-success float-right">{{trans('controls.students.dob')}}: {{$row->dob}}</span></a>
                <span class="product-description">
                    <span class="text-success">
                        {{$row->name_en}}
                    </span>
                </span>
              </div>
            </li>
        </ul>

    </td>
    <td>
        {{$row->gender==='m'?'ប្រុស':'ស្រី'}}
    </td>

    <td>
        {{$row->address}}
    </td>

    <td>
        {{-- <i class="fas fa-user me-1"></i><span> {{$row->users($row->created_by)->last_name}}</span> --}}
        <br>
        <span class="text-success">
            {{-- <i class="fas fa-clock me-1"></i><span> {{ session()->get('locale')=='kh'?KhmerDateTime\KhmerDateTime::parse($row->created_at)->fromNow():$row->created_at->diffForHumans() }}</span> --}}
        </span>

    </td>


    <td>
        <a class="btn btn-primary  edit_button btn-sm" href="{{route('teachers.show',$row)}}"
                data-toggle="modal-ajax"
                {{-- data-modal-size="modal-lg" --}}
                data-modal-size="modal-xl"
                {{-- data-mdb-toggle="modal" --}}
                data-target="#openModal"
        ><i class="fa fa-eye left"></i> {{ trans('controls.forms.Show')}}</a>

        <a class="btn btn-primary  edit_button btn-sm" href="{{route('teachers.edit',$row)}}"
                data-toggle="modal-ajax"
                {{-- data-modal-size="modal-lg" --}}
                data-modal-size="modal-xl"
                {{-- data-mdb-toggle="modal" --}}
                data-target="#openModal"
        ><i class="fa fa-edit left"></i> {{ trans('controls.forms.Edit')}}</a>


        <a href="javascript:deleteItem({{ $row->id }})"​​ class="btn btn-danger delete_button btn-sm"><i class="fa fa-trash-alt left"></i> {{ trans('controls.forms.Delete')}}</a>


        <form data-toggle="formDelete"  id="frmDeleteItem-{{ $row->id }}" style="display: none" action="{{ route('teachers.destroy',$row->id) }}" role="form" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        </form>

    </td>
</tr>
