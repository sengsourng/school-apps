@extends('layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">{{trans('controls.student absent')}}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('/')}}">{{trans('controls.home')}}</a></li>
            <li class="breadcrumb-item active">{{trans('controls.student absent')}}</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user-graduate"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">សិស្សសរុប</span>
              <span class="info-box-number">
                {{$all_students->count()}}
                <small>នាក់</small>
                ស្រី {{$all_students_female->count()}} <small>នាក់</small>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user-edit"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">សិស្សឆ្នាំថ្មី</span>
              <span class="info-box-number">
                {{$all_students_new->count()}}

                <small>នាក់</small>
                ស្រី {{$all_students_female_new->count()}} <small>នាក់</small>

              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-12 col-sm-6 col-md-4">
          <div class="info-box mb-3">
            <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-chalkboard-teacher"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">សិស្សអវត្តមាន</span>
              <span class="info-box-number">{{$student_absent->count()}} <small>នាក់</small>, ស្រី {{$class_absent_female->count()}} <small>នាក់</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->



        <div class="col-md-6">
            <!-- USERS LIST -->
            <div class="card card-outline card-info">
              <div class="card-header">
                <h3 class="card-title">បញ្ចីថ្នាក់ សិស្សអវត្តមាន</h3>

                <div class="card-tools">
                  <span class="badge badge-danger">{{$class_absent->count()}} ថ្នាក់</span>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0" style="display: block;">
                <ul class="users-list clearfix">
                  @foreach ($class_absent as $item)

                    <ul class="products-list product-list-in-card pl-2 pr-2">
                        <li class="item">
                        <div class="product-img">
                                @php
                                    $profile=getName2('teachers','profile',getName2('classes','teacher_id',$item->class_id));
                                @endphp

                                @if ($profile===null)
                                    {{-- <img width="74" src="{{asset('images/students/default_student.jpg')}}" alt="User Image" class="img-size-50"> --}}
                                    <img width="74" src="{{asset('images/teachers/default_teacher.jpg')}}" alt="User Image">

                                @elseif ($profile==="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50")
                                    {{-- <img width="74" src="{{asset('images/students/default_student.jpg')}}" alt="User Image" class="img-size-50"> --}}
                                    {{-- <img width="74" src="{{asset('images/teachers/'.$profile)}}" alt="User Image"> --}}
                                    <img width="74" src="{{asset('images/teachers/default_teacher.jpg')}}" alt="User Image">

                                @else
                                        {{-- <img width="74" src="{{asset('images/students/'.$profile)}}" alt="User Image"> --}}
                                        <img width="74" src="{{asset('images/teachers/'.$profile)}}" alt="User Image">

                                @endif
                        </div>
                        <div class="product-info">
                                <a href="javascript:void(0)" class="product-title">
                                    {{getName('classes','name',$item->class_id)}}
                                    {{-- <small> {{getName('classes','name',$item->class_id)}}</small> --}}
                                    <span class="badge badge-warning float-right">{{$item->count}} នាក់</span>
                                </a>

                                {{-- <a class="users-list-name pt-1" href="#">
                                    {{getName('classes','name',$item->class_id)}}
                                    <span class="badge badge-danger">{{$item->count}} នាក់</span>
                                </a> --}}

                                <span class="product-description">
                                    {{-- {{getName('students','address',$item->student_id)}} --}}
                                    @php
                                        $gender=getName2('teachers','gender',getName2('classes','teacher_id',$item->class_id))=='ប'?'លោកគ្រូ ':'អ្នកគ្រូ ';
                                    @endphp
                                        {{getName('teachers','name_kh',getName2('classes','teacher_id',$item->class_id))}}
                                </span>
                        </div>
                        </li>
                    </ul>

                  @endforeach
                </ul>
                <!-- /.users-list -->
              </div>
              <!-- /.card-body -->

              <!-- /.card-footer -->
            </div>
            <!--/.card -->
        </div>


        <div class="col-md-6">
            <!-- USERS LIST -->
            <div class="card card-outline card-success">
              <div class="card-header">
                <h3 class="card-title">សិស្សអវត្តមាន ថ្នាក់ទី២</h3>

                <div class="card-tools">
                  <span class="badge badge-danger">{{$student_absent->count()}} នាក់</span>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0 " style="display: block;">
                <ul class="users-list clearfix">
                  {{-- @for ($i = 1; $i <=6; $i++) --}}
                  {{-- getName('students','name_kh',$item->student_id) --}}

                  {{-- @endfor --}}

                    @foreach ($student_absent as $item)

                            <ul class="products-list product-list-in-card pl-2 pr-2">
                                    <li class="item">
                                    <div class="product-img">
                                            @php
                                                $profile=getName2('students','profile',$item->student_id);
                                            @endphp

                                            @if ($profile===null)
                                                <img width="74" src="{{asset('images/students/default_student.jpg')}}" alt="User Image" class="img-size-50">

                                            @elseif ($profile==="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50")
                                                <img width="74" src="{{asset('images/students/default_student.jpg')}}" alt="User Image" class="img-size-50">

                                            @else
                                                    <img width="74" src="{{asset('images/students/'.$profile)}}" alt="User Image">

                                            @endif
                                    </div>
                                    <div class="product-info">
                                            <a href="javascript:void(0)" class="product-title">
                                                {{getName('students','name_kh',$item->student_id)}}
                                                <small> {{getName('classes','name',$item->class_id)}}</small>
                                                <span class="badge badge-warning float-right">{{$item->reason}}</span>
                                            </a>
                                            <span class="product-description">
                                                {{getName('students','address',$item->student_id)}}
                                            </span>
                                    </div>
                                    </li>
                            </ul>
                    @endforeach
                </ul>
                <!-- /.users-list -->

              </div>
              <!-- /.card-body -->

            </div>
            <!--/.card -->
        </div>




      </div>
      <!-- /.row -->







    </div><!--/. container-fluid -->
  </section>
  <!-- /.content -->
@endsection
