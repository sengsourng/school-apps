@extends('layouts.admin')

@push('custom_css')

<!-- DataTables -->
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset('themes')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

 <!-- daterange picker -->
 <link rel="stylesheet" href="{{asset('themes')}}/plugins/daterangepicker/daterangepicker.css">

 <style>
     /* Custom checkbox */
th .c-container {
	margin-top: 12px;
    margin-bottom: 11px;
}
.c-container {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 23px;
    cursor: pointer;
    font-size: 14px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}


.c-container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}

/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 20px;
    width: 20px;
    background-color: #bdc3c7;
}

/* On mouse-over, add a grey background color */
.c-container:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.c-container input:checked ~ .checkmark {
    background-color: #007bff;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the checkmark when checked */
.c-container input:checked ~ .checkmark:after {
    display: block;
}

/* Style the checkmark/indicator */
.c-container .checkmark:after {
    left: 8px;
    top: 4px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
 </style>
@endpush


@section('content')


     <section class="content pt-2">
        <div class="card">
          <div class="card-header text-left py-3" style="height: 9vh;">
            <h5 class="mb-0 text-left">
                <strong>បញ្ចូល​ពិន្ទុសិស្ស</strong>
            </h5>
          </div>
          <div class="card-body">
            <div>
                <div class="row">
                    <div class="col-md-12">
                            <div class="card card-outline card-success">
                              <div class="card-header">
                                <form id="search_form" class="params-panel validate" action="{{ route('exams.create')}}" method="post" autocomplete="off" accept-charset="utf-8">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="control-label">{{trans('controls.levels.name')}}</label>
                                                <select name="level_id" class="form-control select2" onChange="getData(this.value);" required>
                                                    <option value="">{{ trans('controls.levels.select') }}</option>
                                                    {{ create_option('levels','id','name',$level_id) }}
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="control-label">{{trans('controls.classes.name')}}</label>
                                                <select name="class_id" id="class_id" class="form-control select2" required>
                                                    <option value="">{{ trans('controls.classes.select') }}</option>
                                                    {{ create_option('classes','id','name',$class_id) }}
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="control-label">{{trans('controls.sections.name')}}</label>
                                                <select name="section_id" class="form-control select2" required>
                                                    <option value="">{{ trans('controls.forms.Please Select') }}</option>
                                                    {{ create_option('sections','id','name',$section_id) }}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="date" class="control-label">{{trans('controls.students.date')}}</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    <input type="date" class="form-control datepicker" name="date" value="{{ $date }}" required>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-sm-2 pt-4">
                                            <div class="form-group pt-2">
                                                <button type="submit" class="btn btn-primary btn-block rect-btn">{{trans('controls.scores.manage')}}</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                              </div>
                            </div>
                    </div>
                    <?php
                    // $date=date_create("2013-03-15");
                    // echo date_format($date,"Y/m/d H:i:s");
                    $myMonth=date('m', strtotime($date));
                    $myYear=date('Y', strtotime($date));
                    $myDate=date('Y-m-d', strtotime($date));
                ?>

@php
use KhmerDateTime\KhmerDateTime;
    // $date = Carbon\Carbon::now();// will get you the current date, time

    $date=Carbon\Carbon::createFromFormat('Y-m-d', $date);
    $dateTime = KhmerDateTime::parse($date->format("Y-m-d"));

        $dateTime->day(); // ២២
        $dateTime->fullDay(); // ពុធ
        $dateTime->month(); // ០៥
        $dateTime->fullMonth(); // ឧសភា
        $dateTime->year(); // ២០១៩
        $dateTime->minute(); // ០០
        $dateTime->hour(); // ០០
        $dateTime->meridiem(); // ព្រឹក
        $dateTime->week(); // ៤
        $dateTime->fullWeek(); // សប្តាហ៍ទី៤
        $dateTime->weekOfYear(); // ២១
        $dateTime->fullWeekOfYear(); // សប្តាហ៍ទី២១
        $dateTime->quarter(); // ២
        $dateTime->fullQuarter(); // ត្រីមាសទី២

@endphp
                    <div class="col-md-12">
                            @if (!empty($attendance))

                            <div class="text-center">
                                <h3>{{trans('controls.scores.manage')}}
                                </h3>
                                <span class="text-success mb-4">






                                    {{$section}} - {{$class}} សម្រាប់ខែ : {{$dateTime->fullMonth()}} <br>
                                    {{-- {{$sectionSubject}} --}}
                                    {{-- {{$section_id}} --}}
                                    {{-- សម្រាប់ខែ : {{$myMonth}} --}}
                                    {{trans('controls.students.date')}} :
                                    {{$dateTime->fullDay()}}-{{$dateTime->day()}}-{{$dateTime->fullMonth()}}-{{$dateTime->year()}}

                                    {{-- $dateTime->day(); // ២២
                                    $dateTime->fullDay(); // ពុធ
                                    $dateTime->month(); // ០៥
                                    $dateTime->fullMonth(); // ឧសភា
                                    $dateTime->year(); // ២០១៩ --}}



                                </span>

                            </div>

                            <form action="{{ route('student_score.save') }}" class="appsvan-submit-validate" method="post" accept-charset="utf-8">
                                @csrf



                                <table id="example2" role="grid" class="table table-bordered table-hover dataTable dtr-inline collapsed table-responsive">
                                    <thead>
                                        <th style="width: 150px !important; height: 10px;">{{trans('controls.students.name')}}</th>
                                            <?php
                                                    $Subjects=explode(",",$sectionSubject);
                                                    $arrLength = count($Subjects);
                                                    for ($i=0; $i<$arrLength; $i++) {
                                                        $sID= $Subjects[$i];
                                                        // if($i==$arrLength-2){
                                                            ?>
                                                            <th style="min-width: 70px; height: 10px;">
                                                                {{getSubjectNameID($sID)}}
                                                            </th>
                                                        <?php

                                                        }
                                            ?>


                                    </thead>
                                    <tbody>
                                        @if (!empty($attendance))
                                            @foreach($attendance AS $key => $data)
                                            <tr>
                                                <td style="min-width:150px !important;">{{ $data->student_id }}-{{ $data->name_kh}}</td>

                                                <input type="hidden" name="score_id[]" value="{{$data->score_id}}">

                                                <?php
                                                        $Subjects=explode(",",$sectionSubject);
                                                        $arrLength = count($Subjects);
                                                        for ($i=0; $i<$arrLength; $i++) {
                                                            $sID= $Subjects[$i];

                                                                ?>
                                                                <input type="hidden" name="student_id[]" value="{{$data->student_id}}">
                                                                <input type="hidden" name="class_id[]" value="{{$data->class_id}}">
                                                                <input type="hidden" name="section_id[]" value="{{$section_id}}">
                                                                <input type="hidden" name="level_id" value="{{$data->level_id}}">
                                                                <input type="hidden" name="date" value="{{$date}}">

                                                                <td>
                                                                    @php
                                                                            $score_subject = App\Models\Score::select('scores.*','student_id')
                                                                                            ->join("students","students.id","=","scores.student_id")
                                                                                            ->join("subjects","scores.subject_id","=","subjects.id")
                                                                                            // ->with('attendance')
                                                                                            // ->where('student_id','=',$item->student_id)
                                                                                            ->where('scores.month','=',$myMonth)
                                                                                            // ->where('scores.month','=',date('m', strtotime($data->date)))
                                                                                            ->where('scores.year','=',$myYear)
                                                                                            // ->where('scores.date','=',date('Y-m-d', strtotime($data->date)))
                                                                                            ->where('scores.class_id','=',$class_id)
                                                                                            ->where('scores.section_id','=',$section_id)
                                                                                            //    ->where('attendance','=',2)
                                                                                            ->where('student_id','=',$data->student_id)
                                                                                            ->groupBy("scores.subject_id")
                                                                                            ->orderBy("subjects.short","ASC")
                                                                                            ->get();

                                                                                            // dd($score_subject);
                                                                        @endphp
                                                                                <input style="width: 100%;" type="number" name="score[{{$data->student_id}}][{{$sID}}]"  value="{{$score_subject[$i]->score??0}}"   placeholder="{{getSubjectNameID($sID)}}">

                                                                    {{-- <input style="width: 100%;" type="text" name="score[{{$data->student_id}}][{{$sID}}]" @if($data->date ==$date) value="8"  @endif placeholder="{{getSubjectNameID($sID)}}"> --}}

                                                                </td>
                                                            <?php
                                                        }
                                                ?>
                                            </tr>
                                            @endforeach
                                        @endif

                                        <tr>
                                          <td colspan="100"><button type="submit" class="btn btn-primary float-left">{{trans('controls.scores.save')}}</button></td>
                                        </tr>
                                    </tbody>
                                </table>



                            </form>

                            @endif
                    </div>
                </div>

            </div>
          </div>
        </div>



     </section>





@endsection


@push('custom_js')

<!-- DataTables  & Plugins -->
<script src="{{asset('themes')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{asset('themes')}}/plugins/jszip/jszip.min.js"></script>
<script src="{{asset('themes')}}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{asset('themes')}}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{asset('themes')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<!-- date-range-picker -->
<script src="{{asset('themes')}}/plugins/daterangepicker/daterangepicker.js"></script>

<script src="{{asset('dropzone/dist/dropzone.js')}}"></script>



<script type="text/javascript">
    // getData('');

    // $('#class_id').attr('disabled', 'disabled');

	function getData(val) {
		var _token=$('input[name=_token]').val();
		var level_id=$('select[name=level_id]').val();
        if(level_id !=""){
            $('#class_id').removeAttr('disabled');
        }else{
            $('#class_id').attr('disabled', 'disabled');
        }
		$.ajax({
			type: "POST",
			url: "{{url('admin/classes/get_classes')}}",
			data:{_token:_token,level_id:level_id},
			beforeSend: function(){
				$("#preloader").css("display","block");
			},success: function(classes){
				$("#preloader").css("display","none");
				$('select[name=class_id]').html(classes);
			}
		});
	}

	$("input:checkbox").on('click', function() {

		var $box = $(this);
		if ($box.is(":checked")) {
			var group = "input:checkbox[name='" + $box.attr("name") + "']";
			$(group).prop("checked", false);
			$box.prop("checked", true);
		} else {
			$box.prop("checked", false);
		}
	});

	function present(source) {
		$(".absent,.late,.present,.holiday,#late_all,#absent_all,#holiday_all").prop("checked",false);
		var checkboxes = document.querySelectorAll('.present');
		for (var i = 0; i < checkboxes.length; i++) {
			if (checkboxes[i] != source)
				checkboxes[i].checked = source.checked;
		}
	}
	function absent(source) {
		$(".absent,.late,.present,.holiday,#present_all,#late_all,#holiday_all").prop("checked",false);
		var checkboxes = document.querySelectorAll('.absent');

		for (var i = 0; i < checkboxes.length; i++) {
			if (checkboxes[i] != source)
				checkboxes[i].checked = source.checked;
		}
	}
	function late(source) {
		$(".absent,.late,.present,.holiday,#present_all,#absent_all,#holiday_all").prop("checked",false);
		var checkboxes = document.querySelectorAll('.late');
		for (var i = 0; i < checkboxes.length; i++) {
			if (checkboxes[i] != source)
				checkboxes[i].checked = source.checked;
		}
	}

	function holiday(source) {
		$(".absent,.late,.present,.holiday,#present_all,#absent_all").prop("checked",false);
		var checkboxes = document.querySelectorAll('.holiday');
		for (var i = 0; i < checkboxes.length; i++) {
			if (checkboxes[i] != source)
				checkboxes[i].checked = source.checked;
		}
	}
</script>

@endpush
