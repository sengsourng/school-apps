<?php

namespace Database\Seeders;

use App\Models\AcademicYear;
use App\Models\Skill;
use Illuminate\Database\Seeder;

class AcademicYearSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AcademicYear::create([
            'name'  =>  '២០១៨-២០១៩',
            'year'  =>  '2018',
            'description'  =>  '2018-2019',
            'is_active'  =>  '0',
        ]);

        AcademicYear::create([
            'name'  =>  '២០១៩-២០២០',
            'year'  =>  '2019',
            'description'  =>  '2019-2020',
            'is_active'  =>  '0',
        ]);

        AcademicYear::create([
            'name'  =>  '២០២០-២០២១',
            'year'  =>  '2020',
            'description'  =>  '2020-2021',
            'is_active'  =>  '0',
        ]);

        AcademicYear::create([
            'name'  =>  '២០២១-២០២២',
            'year'  =>  '2021',
            'description'  =>  '2021-2022',
            'is_active'  =>  '1',
        ]);


        // Create Skills
        Skill::create([
            'name'  =>  'កម្មវិធីភាសាខ្មែរ',
            'description'  =>  'Khmer Program',
        ]);
        Skill::create([
            'name'  =>  'កម្មវិធីភាសាអង់គ្លេស',
            'description'  =>  'English Program',
        ]);

        Skill::create([
            'name'  =>  'កម្មវិធីភាសាចិន',
            'description'  =>  'Chiness Program',
        ]);

    }
}
