<?php

namespace Database\Seeders;

use App\Models\Level;
use App\Models\Section;
use Illuminate\Database\Seeder;

class LevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Level::create([
            'code'      => 'L01',
            'name'      => 'ថ្នាក់ទី១',
            'description'      => 'Grade 1',
            'status'      => 'active',
        ]);

        Level::create([
            'code'      => 'L02',
            'name'      => 'ថ្នាក់ទី២',
            'description'      => 'Grade 2',
            'status'      => 'active',
        ]);

        Level::create([
            'code'      => 'L03',
            'name'      => 'ថ្នាក់ទី៣',
            'description'      => 'Grade 3',
            'status'      => 'active',
        ]);

        Level::create([
            'code'      => 'L04',
            'name'      => 'ថ្នាក់ទី៤',
            'description'      => 'Grade 4',
            'status'      => 'active',
        ]);

        Level::create([
            'code'      => 'L05',
            'name'      => 'ថ្នាក់ទី៥',
            'description'      => 'Grade 5',
            'status'      => 'active',
        ]);

        Level::create([
            'code'      => 'L06',
            'name'      => 'ថ្នាក់ទី៦',
            'description'      => 'Grade 6',
            'status'      => 'active',
        ]);

        Level::create([
            'code'      => 'L07',
            'name'      => 'ថ្នាក់ទី៧',
            'description'      => 'Grade 7',
            'status'      => 'active',
        ]);

        Level::create([
            'code'      => 'L08',
            'name'      => 'ថ្នាក់ទី៨',
            'description'      => 'Grade 8',
            'status'      => 'active',
        ]);
        Level::create([
            'code'      => 'L09',
            'name'      => 'ថ្នាក់ទី៩',
            'description'      => 'Grade 9',
            'status'      => 'active',
        ]);

        Level::create([
            'code'      => 'L10',
            'name'      => 'ថ្នាក់ទី១០',
            'description'      => 'Grade 10',
            'status'      => 'active',
        ]);

        Level::create([
            'code'      => 'L11',
            'name'      => 'ថ្នាក់ទី១១',
            'description'      => 'Grade 11',
            'status'      => 'active',
        ]);

        Level::create([
            'code'      => 'L12',
            'name'      => 'ថ្នាក់ទី១២',
            'description'      => 'Grade 12',
            'status'      => 'active',
        ]);



// Create Sections
        Section::create([
            'skill_id'  => 1,
            'level_id'  =>1,
            'code'      => 'S101',
            'name'      => 'ឆមាសទី១',
            'subject_id'      => '1,2,3,4,5',
            'description'      => 'Semester 1',
            'score_type'      => 'average',
            'status'      => 'active',
        ]);

        Section::create([
            'skill_id'  => 1,
            'level_id'  =>1,
            'code'      => 'S102',
            'name'      => 'ឆមាសទី២',
            'subject_id'      => '1,2,3,4,5',
            'description'      => 'Semester 2',
            'score_type'      => 'average',
            'status'      => 'active',
        ]);


    }
}
