<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Level;
use App\Models\Score;
use App\Models\Classes;
use App\Models\Section;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\Attendance;
use App\Models\StudentInfo;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Exports\StudentExport;
use App\Imports\StudentImport;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Intervention\Image\Facades\Image;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        // dd($request->all());

        try {

                $data['level_id']=$request->level_id;
                $data['class_id']=$request->class_id;
                $data['register_year']=$request->register_year ;

                $data['students']=Student::get();
                return view('backend.students.home',$data);


          } catch (\Exception $e) {

              return $e->getMessage();
          }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if( request()->ajax()){
		    return view('backend.students.modal.add');
		}
        return view('backend.students.add');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = [
            'status' => false,
            'data' => [],
        ];

        // dd($request->all());
         // Create User
            if ($files = $request->file('profile')) {
                    // $old_file=$student->profile;
                // for save original image

                    $ImageUpload = Image::make($files);
                    $ImageUpload_User = Image::make($files);
                    $originalPath = 'images/students/';
                    $originalPath_User = 'images/users/';

                    $ImageUpload->save($originalPath.time().$files->getClientOriginalName());
                    $ImageUpload_User->save($originalPath_User.time().$files->getClientOriginalName());

                    // for save thumnail image
                    $thumbnailPath = 'images/students/thumbnail/';
                    $ImageUpload->resize(250,250);
                    $ImageUpload = $ImageUpload->save($thumbnailPath.time().$files->getClientOriginalName());

                    $profile=time().$files->getClientOriginalName();
            }else{
                $profile='https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50';
            }
                        $full_name=explode(" ",$request['name_en']);

                        $user=User::create([
                            'role_id'   => 4,
                            'school_id' =>  auth()->user()->school_id,
                        //    'school_id'  => $request['school_id'],
                            'name' =>  $request['name_en'],
                            'fname' =>  $full_name[0],
                            'lname' =>  $full_name[1],
                            'username' =>  $request['code'],
                            'email' =>  $request['code']."@gmail.com",
                            'password' =>  Hash::make('123456'),
                            'usertype' =>  'student',
                            'profile' =>  $profile,

                        ]);

                        // Create Student

                        $timestamp = strtotime($request['dob']);
                        $dob=date('d-m-Y', $timestamp );
                        $student = Student::create([
                        // $insert_data[] = array(
                            'user_id'  => $user->id,
                            //    'school_id'  => $request['school_id'],
                            'school_id'  => auth()->user()->school_id,
                            'code'  => $request['code'],
                            'name_en'  => $request['name_en'],
                            'name_kh'   => $request['name_kh'],
                            'gender'   => $request['gender'],

                            'dob'    => $dob,
                            //    'dob'    => \Carbon\Carbon::parse($request['dob'])->format('d/m/Y'),

                            'pob'  => $request['pob'],
                            'address'   => $request['address'],
                            'father'   => $request['father'],
                            'father_job'   => $request['father_job'],
                            'mother'   => $request['mother'],
                            'mother_job'   => $request['mother_job'],
                            'city'   => $request['city'],
                            'city_slug'   => Str::slug($request['city']),
                            'register_year'   => $request['register_year'],
                            'is_active'   => 1,
                            'profile' =>  $profile,

                        ]);

                        $date_register = strtotime($request['date']);
                        $date_register=date('Y-m-d', $date_register );

                        $add=StudentInfo::create([
                            'school_id'  => auth()->user()->school_id,
                            'level_id'  => $request['level_id'],
                            'skill_id'  => 1,
                            'class_id'  => $request['class_id'],
                            'student_id'   => $student->id,
                            'date'   => $date_register,
                            'study_year'  => $request['register_year'],
                            // 'class_id'  => $request['class_id'],
                        ]);



        if ($add) {
            $response = [
                'status' => true,
                'data' => $add,
                'dom' => view('backend.students.rows',['row' => $add])->render(),
            ];
        }
        if(request()->ajax()){
            return $response;
        }else{
            // redirect(route('students.index'));
             return redirect()->route('students.index');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        $user=User::findOrFail($student->user_id);
        $student=Student::findOrFail($student->id);
        $student_infos=StudentInfo::where('student_id','=',$student->id)->get();
        // dd($user);
        if( request()->ajax()){
		    return view('backend.students.modal.show',compact('student','user','student_infos'));
		}
    }


    public function filter(Request $request)
    {
        $level_id = $request->level_id;
		$class_id = $request->class_id;
		// $section_id = $request->section_id;
		// $date = $request->date;
        $register_year=$request->register_year;

        // dd($request);

        $students = Student::join('student_infos', 'student_infos.student_id', '=', 'students.id')
                        ->where('student_infos.study_year','=',$register_year)
                        ->where('student_infos.class_id','=',$class_id)
                        // ->where('student_infos.level_id','=',$level_id)
                        ->get();

            // dd($students);
        return view('backend.students.home',compact('students','level_id','class_id','register_year'));

        // dd($user);
        // if( request()->ajax()){
		//     return view('backend.schools.modal.show',compact('school','user','student'));
		// }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {

        $user=User::findOrFail($student->user_id);
        $student=Student::findOrFail($student->id);
        $student_infos=StudentInfo::where('student_id','=',$student->id)->get();
        // dd($user);
        if( request()->ajax()){
		    return view('backend.students.modal.edit',compact('student','user','student_infos'));
		}else{
		    return view('backend.students.edit',compact('student','user','student_infos'));

        }


        // $user=User::findOrFail($student->user_id);
        // $student=Student::findOrFail($student->id);
        // // dd($user);
        // if( request()->ajax()){
		//     return view('backend.schools.modal.edit',compact('school','user','student'));
		// }

        // return view('backend.schools.edit',compact('school','user','student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $response = [
            'status' => false,
            'profile' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'data' => [],
        ];

        // $teacher_name=explode("-",$request->teacher_name);

        if ($files = $request->file('profile')) {
                $old_file=$student->profile;
              // for save original image

                $ImageUpload = Image::make($files);
                $originalPath = 'images/students/';

                $ImageUpload->save($originalPath.time().$files->getClientOriginalName());

                // for save thumnail image
                $thumbnailPath = 'images/students/thumbnail/';
                $ImageUpload->resize(250,250);
                $ImageUpload = $ImageUpload->save($thumbnailPath.time().$files->getClientOriginalName());

                $profile=time().$files->getClientOriginalName();

                if($old_file !="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50"){

                    if($old_file !=null){
                        if(file_exists(public_path('images/students/'.$old_file))){
                            // dd($old_file);
                            unlink(public_path('images/students/'.$old_file));
                            unlink(public_path('images/students/thumbnail/'.$old_file));
                        }
                    }
                }





            $update = $student->update([
                'name_kh'          => $request->name_kh,
                'name_en'          => $request->name_en,
                'gender'          => $request->gender,
                'dob'          => $request->dob,
                'pob'          => $request->pob,


                'city'    => $request->school_city,
                'city_slug' => $this->makeSlug($request->school_city),
                'address'    => $request->address,
                // 'director_name'    => $teacher_name[0],
                // 'user_id'    => auth()->user()->id
                'profile'          => $profile,

            ]);

        }else{
            $update = $student->update([
                'name_kh'          => $request->name_kh,
                'name_en'          => $request->name_en,
                'gender'          => $request->gender,
                'dob'          => $request->dob,
                'pob'          => $request->pob,


                'city'    => $request->school_city,
                'city_slug' => $this->makeSlug($request->school_city),
                'address'    => $request->address,
                // 'director_name'    => $teacher_name[0],
                // 'user_id'    => auth()->user()->id
                // 'profile'          => $request->name_en,

            ]);
        }





        if ($update) {
            $response = [
                'status' => true,
                'data' => $student,
                'dom' => view('backend.students.rows',['row' => $student])->render(),
            ];
        }
        // if(request()->ajax()){
        //     // return $response;
        //     return back()->with('success', 'Excel Data Imported successfully.');

        // }

        return back()->with('success', 'Excel Data Imported successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $response = [
            'status' => false,
        ];
        $student=Student::findOrFail($id);
        $user=User::findOrFail($student->user_id);

        $student_info=StudentInfo::where('student_id','=',$student->id);
        $student_att=Attendance::where('student_id','=',$student->id);
        $student_score=Score::where('student_id','=',$student->id);


        // if ($student->products->count() == 0) {
            $student_score->delete();
            $student_att->delete();
            $student_info->delete();
            $student->delete();
            $user->delete();
            $response = [
                'status' => true,
            ];
        // }
        if (request()->ajax()) {
            return $response;
        }

        return redirect()->route('students.index');
    }


    public function ImportView()
    {
        $level_id=Level::get();

    return view('backend.students.import',compact(
                                        'level_id'
                                    ));

    }

    public function export()
    {
        return Excel::download(new StudentExport, 'students.xlsx');
    }


    public function import(Request $request)
    {

        try {
            //  dd($request->study_year);
                if(request()->file('file')){
                    // Excel::import(new StudentImport,request()->file('file'));
                    Excel::import(new StudentImport,request()->file('file'));
                }
                // return back();
                return back()->with('success', 'Excel Data Imported successfully.');

        } catch (\Exception $e) {
            // return back()->withError($e->getMessage());
            return back()->with('error', 'You can not Import Data');

        }




    }

    // function import(Request $request)
    // {
    //     // $this->validate($request,[
    //     // 'select_file'  => 'required|mimes:xls,xlsx'
    //     // ]);

    //     $path = $request->file('select_file')->getRealPath();

    //     $data = Excel::load($path)->get();

    //         if($data->count() > 0)
    //         {
    //             foreach($data->toArray() as $key => $value)
    //             {
    //                 foreach($value as $row)
    //                 {

    //                     // Create User
    //                     $full_name=explode(" ",$row['name_en']);

    //                     $user=User::create([
    //                         'role_id'   => 4,
    //                         'school_id' =>  auth()->user()->school_id,
    //                     //    'school_id'  => $row['school_id'],
    //                         'name' =>  $row['name_en'],
    //                         'fname' =>  $full_name[0],
    //                         'lname' =>  $full_name[1],
    //                         'username' =>  $row['code'],
    //                         'email' =>  $row['code']."@gmail.com",
    //                         'password' =>  Hash::make('123456'),
    //                         'usertype' =>  'student',
    //                         'profile' =>  'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50',

    //                     ]);

    //                     // Create Student

    //                     $timestamp = strtotime($row['dob']);
    //                     $dob=date('d-m-Y', $timestamp );

    //                     $insert_data[] = array(
    //                         'user_id'  => $user->id,
    //                         //    'school_id'  => $row['school_id'],
    //                         'school_id'  => auth()->user()->school_id,
    //                         'code'  => $row['code'],
    //                         'name_en'  => $row['name_en'],
    //                         'name_kh'   => $row['name_kh'],
    //                         'gender'   => $row['gender'],

    //                         'dob'    => $dob,
    //                         //    'dob'    => \Carbon\Carbon::parse($row['dob'])->format('d/m/Y'),

    //                         'pob'  => $row['pob'],
    //                         'address'   => $row['address'],
    //                         'father'   => $row['father'],
    //                         'father_job'   => $row['father_job'],
    //                         'mother'   => $row['mother'],
    //                         'mother_job'   => $row['mother_job'],
    //                         'city'   => $row['city'],
    //                         'city_slug'   => Str::slug($row['city']),
    //                         'register_year'   => $row['register_year'],
    //                         'is_active'   => 1,
    //                     );
    //                 }
    //             }

    //             if(!empty($insert_data))
    //             {
    //                 DB::table('students')->insert($insert_data);
    //             }
    //         }
    //     }
    //     return back()->with('success', 'Excel Data Imported successfully.');

    // }



}
