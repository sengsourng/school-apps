<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Student extends Model
{
    use HasFactory;

    protected $fillable = [
        'role_id',
        'user_id',
        'school_id',
        'code',
        'name_en',
        'name_kh',
        'gender',
        'dob',
        'pob',
        'address',
        'father',
        'father_job',
        'mother',
        'mother_job',
        'city',
        'city_slug',
        'register_year',
        'is_active',
        'profile',
        'bio',
        'status',
    ];



    public function classes()
    {
        return $this->hasOne(Classes::class);
    }

    public function student_infos()
    {
        return $this->hasOne(StudentInfo::class);
    }

    public function levels()
    {
        // return $this->hasOne(Level::class);
        return $this->hasOne(StudentInfo::class);

    }


    public function attendance()
    {
        return $this->hasOne(Attendance::class);
    }



}
