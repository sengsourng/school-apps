<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_infos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->unsignedBigInteger('level_id')->references('id')->on('levels')->onDelete('cascade');
            $table->unsignedBigInteger('skill_id')->references('id')->on('skills')->onDelete('cascade');
            $table->unsignedBigInteger('class_id')->references('id')->on('classes')->onDelete('cascade');
            $table->unsignedBigInteger('student_id')->references('id')->on('students')->onDelete('cascade');

            $table->date('date')->nullable();
            $table->text('description')->nullable();
            $table->string('study_year')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_infos');
    }
}
