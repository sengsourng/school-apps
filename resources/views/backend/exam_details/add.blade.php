
@extends('layouts.admin')

@section('content')

    <section class="content pt-2">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title text-center">{{trans('controls.forms.Add New')}}</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form>
                  <div class="card-body">

                    <div class="form-group">
                      <label for="name">{{trans('controls.schools.name')}}</label>
                      <input type="text" class="form-control" id="name" placeholder="បឋម.ស្វាយធំ">
                    </div>

                    <div class="form-group">
                        <label for="name">{{trans('controls.schools.phone')}}</label>
                        <input type="text" class="form-control" id="name" placeholder="092771244">
                    </div>

                    <div class="form-group">
                        <label for="name">{{trans('controls.schools.email')}}</label>
                        <input type="email" class="form-control" id="name" placeholder="yourschool@gmail.com">
                    </div>

                    <div class="form-group">
                        <label for="name">{{trans('controls.schools.city')}}</label>
                        <input type="text" class="form-control" id="name" placeholder="Siem Reap">
                    </div>
                    <div class="form-group">
                        <label for="name">{{trans('controls.schools.address')}}</label>
                        <input type="text" class="form-control" id="name" placeholder="ភូមិ ឃុំ ស្រុក">
                    </div>

                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer float-right">
                    <button type="submit" class="btn btn-primary mr-3">{{trans('controls.save')}}</button>
                    <button type="submit" class="btn btn-danger">{{trans('controls.cancel')}}</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->


            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
              <!-- Form Element sizes -->
              <div class="card card-success">
                <div class="card-header">
                  <h3 class="card-title">ពត៌មាន​នាយក</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">{{trans('controls.schools.director_name')}}</label>
                        <input type="text" class="form-control" id="name" placeholder="សេង ស៊ង់">
                    </div>

                    <div class="form-group">
                        <label for="name">{{trans('controls.schools.email')}}</label>
                        <input type="text" class="form-control" id="name" placeholder="សេង ស៊ង់">
                    </div>

                    <div class="form-group">
                        <label for="name">{{trans('controls.schools.phone')}}</label>
                        <input type="text" class="form-control" id="name" placeholder="សេង ស៊ង់">
                    </div>
                    <div class="form-group">
                        <label for="name">{{trans('controls.schools.gender')}}</label>
                        <input type="text" class="form-control" id="name" placeholder="M or F">
                    </div>
                    <div class="form-group">
                        <label for="name">{{trans('controls.schools.bod')}}</label>
                        <input type="text" class="form-control" id="name" placeholder="1983-04-18">
                    </div>

                    <div class="form-group">
                        <label for="name">{{trans('controls.schools.pob')}}</label>
                        <input type="text" class="form-control" id="name" placeholder="គោករុន​ មុខប៉ែន ពួក សៀមរាប">
                    </div>

                    <div class="form-group">
                        <label for="name">រូបថតកាត</label>
                        <input type="text" class="form-control" id="name" placeholder="សេង ស៊ង់">
                    </div>


                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->



            </div>
            <!--/.col (right) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>

@endsection
